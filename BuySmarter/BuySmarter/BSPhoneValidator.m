//
//  BSPhoneValidator.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSPhoneValidator.h"

@implementation BSPhoneValidator

- (BOOL)execute
{
    if(!self.inputString){
        return NO;
    }
    NSString *expression = @"^(07\\d\\d|0(2|3)\\d\\d)\\d{6}$";
    NSError *error = NULL;
    BOOL result;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:self.inputString options:0 range:NSMakeRange(0, [self.inputString length])];
    if (match){
        result = YES;
    }else{
        result = NO;
    }
    return result;
}

@end
