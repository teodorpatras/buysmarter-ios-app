//
//  BSIndicatorView.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSIndicatorView.h"
#import "BSAccessoryView.h"

static BSIndicatorView *indicatorView;

@implementation BSIndicatorView


+ (void)showWithText:(NSString *)text
{
    if(indicatorView){
        if([indicatorView isVisible]){
            [indicatorView dismissWithClickedButtonIndex:0 animated:YES];
            indicatorView = nil;
        }
    }
    BSAccessoryView * accessoryView = [BSAccessoryView accessoryViewWithText:text];
    indicatorView = [BSIndicatorView new];
    [indicatorView setValue:accessoryView forKeyPath:@"accessoryView"];
    [indicatorView setBackgroundColor:[UIColor redColor]];
    [indicatorView show];
}

+ (void) hide
{
    if(indicatorView){
        [indicatorView dismissWithClickedButtonIndex:0 animated:YES];
        indicatorView = nil;
    }
}

@end
