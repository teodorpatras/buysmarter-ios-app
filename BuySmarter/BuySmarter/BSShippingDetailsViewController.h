//
//  BSShippingDetailsViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"
#import "BSBaseInputViewController.h"

@interface BSShippingDetailsViewController : BSBaseInputViewController<UITextFieldDelegate>

/**
 *  Enables or disables the editing mode
 *
 *  @param editable     wheter or not the user should be able to edit the info
 */
- (void) setEditable:(BOOL)editable;
/**
 *  Gets the validate inputs
 *
 *  @return     dictionary representation of the validated inputs
 */
- (NSDictionary *)getValidInputs;
/**
 *  Configures itself with the user info object
 *
 *  @param user     user info object for the view to be configured with
 */
- (void) populateFieldsWithUser:(UserInfo *)user;

@end
