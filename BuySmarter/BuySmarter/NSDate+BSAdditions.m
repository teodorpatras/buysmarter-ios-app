//
//  NSDate+BSAdditions.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "NSDate+BSAdditions.h"

@implementation NSDate (BSAdditions)


- (NSDate *)midnight
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:self];
    
    // Set the time components manually
    [dateComps setHour:24];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    
    return beginningOfDay;
}


@end
