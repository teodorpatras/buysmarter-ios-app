//
//  BSAnimatedTransition.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSAnimatedTransitioning.h"


static float BSTransitionDuration = 0.25f;

@implementation BSAnimatedTransitioning


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return BSTransitionDuration;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    
    UIView *inView = [transitionContext containerView];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [inView addSubview:toVC.view];
    
    toVC.view.frame = fromVC.view.frame;
    [toVC.view setAlpha:0];
    
    [UIView animateWithDuration:BSTransitionDuration
                     animations:^{
                         [toVC.view setAlpha:1];
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}


@end
