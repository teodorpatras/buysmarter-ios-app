//
//  BSTitleViewsFactory.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductDetailViewsFactory.h"

@implementation BSProductDetailViewsFactory

+ (id) productDetailViewOfType:(BSProductDetailViewType)type withProduct:(Product *)p
{
    switch (type) {
        case BSProductDetailViewTypeDescriptionView:
            return [BSProductDescriptionView descriptionViewForProduct:p];
            break;
        case BSProductDetailViewTypeStockPriceView:
            return [BSProductStockPriceView stockPriceViewForProduct:p];
            break;
        case BSProductDetailViewTypeTitleView:
            return [BSProductTitleView titleViewForProduct:p];
            break;
        default: NSAssert(nil, @"Invalid data type!");
            return nil;
            break;
    }
}

@end
