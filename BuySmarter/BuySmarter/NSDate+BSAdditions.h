//
//  NSDate+BSAdditions.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (BSAdditions)
/**
 *  Returns the begining of the next day (00:00)
 *
 *  @return the begining of the next day
 */
- (NSDate *)midnight;

@end
