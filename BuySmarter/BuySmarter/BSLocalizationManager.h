//
//  BSLocalizationManager.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, BSSupportedLanguageType) {
    BSSupportedLanguageTypeEN,
    BSSupportedLanguageTypeRO
};

#define LOCALIZED_STRING(key) [BSLocalizationManager localizedStringForKey:key comment:nil]

FOUNDATION_EXPORT NSString * const SupportedLanguages[];

@interface BSLocalizationManager : NSObject
/**
 *  Gets the localized string for the specified key
 *
 *  @param key      the key of the localized string
 *  @param comment  comment for the localized string
 *
 *  @return         localized string
 */
+ (NSString *)localizedStringForKey:(NSString *)key comment:(NSString *)comment;
/**
 *  Gets the language for the specified type
 *
 *  @param type     language type
 *
 *  @return         language name
 */
+ (NSString *)getLanguageForType:(BSSupportedLanguageType)type;
/**
 *  Gets the type for the specified language
 *
 *  @param language     the language name
 *
 *  @return             the type of the language
 */
+ (BSSupportedLanguageType) getTypeForLanguage:(NSString *)language;
/**
 *  Sets the language
 *
 *  @param language     language to be set
 */
+ (void) setLocalizedLanguage:(NSString *)language;
/**
 *  Deallocates the bundle where the localized files are stored
 */
+ (void) deallocate;

@end
