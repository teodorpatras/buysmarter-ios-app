//
//  BSRegisterViewController.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSAuthDelegate.h"
#import "BSBaseInputViewController.h"

@interface BSRegisterViewController : BSBaseInputViewController
@property (nonatomic, weak) id<BSAuthDelegate> delegate;

/**
 *   Resets the input fields
 */
- (void) resetFields;
/**
 *  Re configures for the settings screen
 */
- (void) configureForSettingsScreen;
/**
 *  Gets the new and old passwords
 *
 *  @param newPass  new password
 *  @param oldPass  old password
 */
- (void) getNewPassword:(NSString **)newPass andOldPassword:(NSString **)oldPass;

@end
