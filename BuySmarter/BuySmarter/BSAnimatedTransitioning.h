//
//  BSAnimatedTransition.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@end
