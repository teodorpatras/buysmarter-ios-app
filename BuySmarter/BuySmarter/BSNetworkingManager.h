//
//  BSNetworkManager.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSConstants.h"

@interface BSNetworkingManager : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
/**
 *  Creates a new GET request
 *
 *  @param url              the url to which the request should me made
 *  @param headers          the headers of the request
 *  @param completionBlock  the block to be called after the request finishes
 */
- (void) newGETRequestForURL:(NSString *)url headers:(NSDictionary *)headers completionBlock:(BSStandardCompletionBlock) completionBlock;
/**
 *  Creates a new POST request
 *
 *  @param url              the url to which the request should me made
 *  @param headers          the headers of the request
 *  @param data             the data to be sent within the body of the request
 *  @param completionBlock  the block to be called after the request finishes
 */
- (void) newPOSTRequestForURL:(NSString *)url headers:(NSDictionary *)headers postData:(NSData *)data completionBlock:(BSStandardCompletionBlock)completionBlock;
/**
 *  Cancels all the active connections for the specified url
 *
 *  @param url              the url for which all the connections should cancel
 */
- (void) cancelConnectionForURL:(NSString *)url;
/**
 *  Cancels all active connections
 */
- (void) cancelAllConnections;
@end
