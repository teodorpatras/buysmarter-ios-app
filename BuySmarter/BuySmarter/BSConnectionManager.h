//
//  BSConnectionManager.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

#define INTERNET_REACHABLE [[BSConnectionManager sharedInstance] internetConnectionAvailable]

@interface BSConnectionManager : NSObject
/**
 *  Method for accesssing the singleton instance
 *
 *  @return Singleton instance
 */
+ (BSConnectionManager *)sharedInstance;
/**
 *  Begins listening for network connectivity changes
 */
- (void) monitorConectivityStatus;
/**
 *  Indicates whether or not the internet connection is available
 *
 *  @return whether or not the internet connection is available
 */
- (BOOL) internetConnectionAvailable;
@end
