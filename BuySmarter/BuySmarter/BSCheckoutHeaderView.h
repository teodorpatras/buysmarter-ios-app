//
//  BSCheckoutHeaderView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat BSCheckoutHeaderViewHeight = 30.0f;

@interface BSCheckoutHeaderView : UIView
/**
 *  Factory method
 *
 *  @param success      boolean parameter indicating the type of the display view
 *
 *  @return             instance of the checkout header view
 */
+ (BSCheckoutHeaderView *) headerViewForSuccess:(BOOL)success;

@end
