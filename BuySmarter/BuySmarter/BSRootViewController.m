//
//  BSRootViewController.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSRootViewController.h"
#import "BSCoreDataAdapter.h"
#import "BSNetworkingAdapter.h"

@interface BSRootViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation BSRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.backgroundImageView setImage:[UIImage imageNamed:@"LaunchImage"]];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /**
     *  Either perform a login or go directly to the home screen
     */
    if(!USER_INFO.accessToken){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"showLogin" sender:self];
        });
    }else{
        [self performSegueWithIdentifier:@"showHomeScreen" sender:self];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


@end
