//
//  BSMenuViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSBaseViewController.h"

@interface BSMenuViewController : BSBaseViewController<UITableViewDataSource, UITableViewDelegate>

@end
