//
//  BSAbstractValidator.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSAbstractValidator : NSObject
// the error message to be registered
@property (nonatomic) NSString *errorMessage;
// the input to be registered
@property (nonatomic) NSString *inputString;
/**
 *  Validates the registered input
 *
 *  @return whether or not the input is valid
 */
- (BOOL) execute;

@end
