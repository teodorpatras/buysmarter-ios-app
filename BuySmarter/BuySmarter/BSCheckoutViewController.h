//
//  BSCheckoutViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "Vendor.h"
#import "PayPalMobile.h"
#import "BSBaseViewController.h"

static NSString * const kBSCheckoutErrorKey = @"error";
static NSString * const kBSCheckoutReservedProductsKey = @"reserved_products";
static NSString * const kBSCheckoutConfirmationKey = @"confirmation-dict";

@protocol BSCheckoutViewControllerDelegate <NSObject>
/**
 *  Informs the delegate that the checkout controller finished
 *
 *  @param dictionary   the dictionary containing the payment confirmation
 */
- (void) checkoutViewControllerDidFinishWithDictionary:(NSDictionary *)dictionary;
/**
 *  Informs the delegate that it should reload its datasource
 */
- (void) checkoutViewControllerDidAlterDataSource;
@end

@interface BSCheckoutViewController : BSBaseViewController<UITableViewDataSource, UITableViewDelegate, PayPalPaymentDelegate>

@property (nonatomic) Vendor *vendor;
@property (nonatomic) NSArray *productsArray;
@property (nonatomic, weak) id<BSCheckoutViewControllerDelegate> delegate;

@end
