//
//  BSOrderProductCell.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "Vendor.h"

static const CGFloat BSOrderLooseCellHeight = 344.f;
static const CGFloat BSOrderCompactCellHeight = 172.f;

@interface BSOrderProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
/**
 *  Configures the cell
 *
 *  @param product the product for which the cell should be configured
 *  @param vendor  the vendor for which the cell should be configured
 */
- (void) configureWithProduct:(Product *)product andVendor:(Vendor *)vendor;

@end
