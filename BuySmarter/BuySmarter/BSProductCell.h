//
//  BSProductViewCell.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "Vendor.h"

@interface BSProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

/**
 *  Configuration method
 *
 *  @param product  the product for the cell to be configured with
 *  @param vendor   the vendor for the cell to be configured with
 */
- (void) configureWithProduct:(Product *)product andVendor:(Vendor *)vendor;
/**
 *  Makes the cell to display the out of stock info
 */
- (void) forceOutOfStock;

@end
