//
//  BSOrderView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSOrderView.h"
#import "BSOrderProductCell.h"
#import "BSConstants.h"
#import "UIImageView+BSAsync.h"
#import "Vendor.h"
#import "Product.h"
#import "BSCoreDataAdapter.h"
#import "BSSessionManager.h"
#import "BSImageLoader.h"

static const CGFloat kBSCornerRadius = 5.0f;
static const CGFloat kBSShadowOpacity = 0.7f;
static const CGFloat kBSShadowOffset = 1.0f;
static const CGFloat kBSShadowRadius = 5.0f;

static NSString * const kBSCompactCellIdentifier = @"compactCell";
static NSString * const kBSLooseCellIdentifier = @"looseCell";

@interface BSOrderView(){
    NSDictionary *_latestReloadedDictionary;
}

@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) id<BSOrderViewDelegate> delegate;
@property (nonatomic) NSDictionary *orderDict;
@end
@implementation BSOrderView

#pragma mark - Configuration

- (void) configureAppearance
{
    for (UIView* subview in self.subviews)
        subview.layer.cornerRadius = kBSCornerRadius;
    [self layer].cornerRadius = kBSCornerRadius;
    [self layer].shadowColor = [UIColor blackColor].CGColor;
    [self layer].shadowOpacity = kBSShadowOpacity;
    [self layer].shadowOffset = CGSizeMake(kBSShadowOffset, kBSShadowOffset);
    [self layer].shadowRadius = kBSShadowRadius;
    [self layer].masksToBounds = NO;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:[self bounds]];
    [self layer].shadowPath = path.CGPath;
}

- (void) configureLocalization
{
    self.totalLabel.text = LOCALIZED_STRING(@"order_view_total_text");
}


- (void)shouldReloadInfo
{
    if(![_latestReloadedDictionary isEqual:self.orderDict]){
        _latestReloadedDictionary = self.orderDict;
        [self.logoView setImageFromURL:[self.orderDict[BSOrderVendorKey] vendorLogoURL] placeHolderImage:nil];
        [self.tableView reloadData];
    }
}
- (void)configureWithDictionary:(NSDictionary *)orderDict
{
    self.orderDict = orderDict;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM YYYY HH:mm"];
    
    //Optionally for time zone converstions
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSString *stringFromDate = [formatter stringFromDate:[self.orderDict[BSOrderKey] date]];
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@", stringFromDate];
    [self.logoView setImageFromURL:[self.orderDict[BSOrderVendorKey] vendorLogoURL] placeHolderImage:nil];
    CGFloat total = 0;
    for(Product *p in self.orderDict[BSOrderProductsKey]){
        total += p.productPrice.floatValue;
    }
    if([USER_INFO.preferredCurrency isEqualToString:[self.orderDict[BSOrderVendorKey] vendorCurrency]]){
        self.totalAmountLabel.text = [NSString stringWithFormat:@"%.2f", total];
        self.currencyLabel.text = [self.orderDict[BSOrderVendorKey] vendorCurrency];
    }else{
        self.totalAmountLabel.text = [NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:total withCurrency:[self.orderDict[BSOrderVendorKey] vendorCurrency]]];
        self.currencyLabel.text = USER_INFO.preferredCurrency;
    }
    if([self.orderDict[BSOrderProductsKey] count] <= 2){
        [self.tableView setScrollEnabled:NO];
    }else{
        [self.tableView setScrollEnabled:YES];
    }
    [self.tableView reloadData];
}
#pragma mark - UITableViewDataSource + Delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.orderDict[BSOrderProductsKey] count] == 1){
        return BSOrderLooseCellHeight;
    }else{
        return BSOrderCompactCellHeight;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BSOrderProductCell *cell = [tableView dequeueReusableCellWithIdentifier:[self.orderDict[BSOrderProductsKey] count] < 2? kBSLooseCellIdentifier: kBSCompactCellIdentifier];
    [cell configureWithProduct:self.orderDict[BSOrderProductsKey][indexPath.row] andVendor:self.orderDict[BSOrderVendorKey]];
    UIImage *image = [[BSImageLoader sharedInstance] cachedImageFromURL:[self.orderDict[BSOrderProductsKey][indexPath.row] imageURLsArray][0]];
    if(image){
        [cell.productImageView setImage:image];
    }else{
        [cell.productImageView setImage:[UIImage imageNamed:@"Placeholder"]];
        [self startIconDownloadForIndexPath:indexPath withOrderViewIndex:self.viewIndex];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.orderDict[BSOrderProductsKey] count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate orderViewDidSelectProduct:self.orderDict[BSOrderProductsKey][indexPath.row]];
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

#pragma mark - helper methods


- (void)loadImagesForOnscreenRows
{
    if ([(NSArray *)self.orderDict[BSOrderProductsKey] count] > 0)
    {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if(indexPath.row < [self.orderDict[BSOrderProductsKey] count]){
                
                if (![[BSImageLoader sharedInstance] cachedImageFromURL:[self.orderDict[BSProductStockKey][indexPath.row] imageURLsArray][0]]){
                    [self startIconDownloadForIndexPath:indexPath withOrderViewIndex:self.viewIndex];
                }
            }
        }
    }
}

- (void) startIconDownloadForIndexPath:(NSIndexPath *)indexPath withOrderViewIndex:(NSInteger)index
{
    Product *product = self.orderDict[BSOrderProductsKey][indexPath.row];
    if([(NSArray *)product.imageURLsArray count] > 0){
        [[BSImageLoader sharedInstance] getImageWithURL:product.imageURLsArray[0] withCompletionBlock:^(UIImage *image, NSString *error){
            if(!error && self.viewIndex == index){
                BSOrderProductCell *productCell = (BSOrderProductCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                [productCell.productImageView setImage:image animated:YES];
            }else{
                // NSLog(@"ERROR: %@", error);
            }
        }];
    }
}

#pragma mark - Class Method

+ (BSOrderView *)orderViewWithDelegate:(id<BSOrderViewDelegate>)delegate
{
    BSOrderView *orderView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
    [orderView setDelegate:delegate];
    [orderView configureAppearance];
    [orderView configureLocalization];
    [orderView.tableView registerNib:[UINib nibWithNibName:@"BSOrderProductLooseCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kBSLooseCellIdentifier];
    [orderView.tableView registerNib:[UINib nibWithNibName:@"BSOrderProductCompactCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kBSCompactCellIdentifier];
    return orderView;
}

@end
