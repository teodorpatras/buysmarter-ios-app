//
//  BSCoreDataAdapter.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/19/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSCoreDataAdapter.h"
#import "BSAppDelegate.h"
#import "Product.h"
#import "NSDate+BSAdditions.h"

static NSString * const BSIdentifierKey = @"identifier";

@interface BSCoreDataAdapter ()
@property (nonatomic) NSDictionary *entitiesDictionary;
@property (nonatomic) NSArray *vendorsArray;
@property (nonatomic) UserInfo *userInfo;
@end
@implementation BSCoreDataAdapter

#pragma mark - Singleton

+ (BSCoreDataAdapter *)sharedInstance
{
    static BSCoreDataAdapter *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[BSCoreDataAdapter alloc] initWithManagedObjectContext:[(BSAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext]];
    });
    return manager;
}

#pragma mark - accessor methods

- (NSDictionary *)entitiesDictionary
{
    if(!_entitiesDictionary){
        _entitiesDictionary = @{@(BSEntityTypeVendor): @"Vendor",
                                @(BSEntityTypeProduct) : @"Product",
                                @(BSEntityTypeUser) : @"UserInfo",
                                @(BSEntityTypeOrder) : @"Order"};
    }
    return _entitiesDictionary;
}

- (NSArray *)vendorsArray
{
    if(!_vendorsArray){
        _vendorsArray = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeVendor)] withPredicate:nil];
    }
    return _vendorsArray;
}

- (UserInfo *)userInfo
{
    if(!_userInfo){
        [self reloadUserInfo];
    }
    return _userInfo;
}

- (void) reloadUserInfo
{
    NSArray *users = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeUser)] withPredicate:nil];
    if(users.count){
        _userInfo = users[0];
    }else{
        _userInfo = nil;
    }
}

#pragma mark - Public methods

- (NSArray *)fetchedOrders
{
    NSArray *orders = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeOrder)] withPredicate:nil];
    return orders;
}

- (UserInfo *)storedUserInfo
{
    return self.userInfo;
}

- (void) insertOrderWithProducts:(NSArray *)productsArray andCreationDateString:(NSString *)creationString
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [formatter dateFromString:creationString];
    
    NSMutableArray *idsArray = [@[] mutableCopy];
    
    NSArray *arrayWithExistingObjectIds = [[self fetchProductsForCategory:BSProductCategoryNone] valueForKey:BSProductDatabaseIdKey];
    
    
    for(int i = 0; i < productsArray.count; i++){
        [idsArray addObject:[productsArray[i] databaseId]];
        if([arrayWithExistingObjectIds containsObject:[productsArray[i] databaseId]]){
            [self deleteObject:productsArray[i]];
        }else{
            [self updateObject:productsArray[i] withValuesAndKeys:@{BSProductCategoryKey : @(BSProductCategoryNone)}];
        }
    }
    
    [self insertObjects:@[@{kBSNDateKey : date, kBSNProductsKey : idsArray}] ofEntity:self.entitiesDictionary[@(BSEntityTypeOrder)]];
    [self updateObject:self.storedUserInfo withValuesAndKeys:@{BSRequestedHistoryCount : @(self.userInfo.requestedHistoryCount.integerValue + 1), BSOrdersCountKey : @(self.userInfo.ordersCount.integerValue + 1)}];
}

- (void)parseServerResponse:(NSDictionary *)response forOperationType:(BSNetworkOperationType)operationType{
    switch (operationType) {
        case BSNetworkOperationTypeGETShoppingCart: case BSNetworkOperationTypeGETWishlist:
            [self insertProducts:response inCategory:operationType == BSNetworkOperationTypeGETShoppingCart?BSProductCategoryShoppingCart:BSProductCategoryWishList];
            break;
        case BSNetworkOperationTypeAddProduct:
            [self addProduct:response];
            break;
        case BSNetworkOperationTypeGETVendorsList:
            [self storeVendors:response];
            break;
        case BSNetworkOperationTypeCheckProductsAvailability:
            [self updateProductsStocks:response];
            break;
        case BSNetworkOperationTypeGETShippingDetails:
            [self storeUserShippingDetails:response];
            break;
        case BSNetworkOperationTypeGetCurrencyRates:
            [self storeUserCurrencyRate:response];
            break;
        case BSNetworkOperationTypeGETOrdersHistory:
            [self storeUserOrdersHistory:response];
            break;
        case BSNetworkOperationTypeGETOrdersHistoryPaginated:
            [self storeUserOrdersHistoryPaginated:response];
            break;
        default: NSAssert(nil, @"Invalid operation type!");
            break;
    }
}


- (void) storeUserOrdersHistoryPaginated:(NSDictionary *)orders
{
    int beginIndex = [orders[kBSNBeginIndex] integerValue];
    int endIndex = [orders[kBSNEndIndex] integerValue];
    NSArray *ordersArray = orders[kBSNOrdersKey];
    NSAssert(ordersArray != nil, @"Invalid response from server!!!");
    
    if(beginIndex == 0){
        [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeOrder)] matchingPredicate:nil];
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@",BSProductCategoryKey, @(BSProductCategoryNone)]];
        [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] matchingPredicate:filterPredicate];
    }
    if(ordersArray.count){
        NSDictionary *dict = [self productsAndOrdersFromOrdersDictionary:orders];
        
        [self insertObjects:dict[kBSNProductsKey] ofEntity:self.entitiesDictionary[@(BSEntityTypeProduct)]];
        [self insertObjects:dict[kBSNOrdersKey] ofEntity:self.entitiesDictionary[@(BSEntityTypeOrder)]];
        NSLog(@"SAVED PRODUCTS : %@", dict[kBSNProductsKey]);
    }
    [self updateObject:self.storedUserInfo withValuesAndKeys:@{BSOrdersRequestedKey : @(YES), BSRequestedHistoryCount : @(ordersArray.count == 0? 0 : endIndex + 1), BSOrdersCountKey : orders[kBSNTotalCount]}];
    NSLog(@"Saved the user info : %@", self.userInfo);
    
}

- (NSDictionary *) productsAndOrdersFromOrdersDictionary:(NSDictionary *)dictionary
{
    
    NSArray *arrayWithExistingObjectIds = [[self fetchProductsForCategory:BSProductCategoryNone] valueForKey:BSProductDatabaseIdKey];
    
    NSArray *productsArray;
    NSMutableArray *arrayWithIds, *productRepresentations;
    NSMutableArray *orderRepresentations;
    NSDictionary *orderRepresentation;
    NSString *dateString;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSz"];
    
    productRepresentations = [@[] mutableCopy];
    arrayWithIds = [@[] mutableCopy];
    orderRepresentations = [@[] mutableCopy];
    
    for(NSDictionary *dict in dictionary[kBSNOrdersKey]){
        productsArray = dict[kBSNProductsKey];
        NSAssert(productsArray != nil, @"Invalid response!!!");
        for(NSDictionary *dict in productsArray){
            [productRepresentations addObject:[self dictionaryForProductEntityWithServerInfo:dict andCategory:BSProductCategoryNone]];
            [arrayWithIds addObject:dict[kBSNIDKey]];
        }
        dateString = dict[kBSNDateKey];
        orderRepresentation = @{kBSNDateKey : [formatter dateFromString:dateString], kBSNProductsKey : arrayWithIds};
        [orderRepresentations addObject:orderRepresentation];
        arrayWithIds = [@[] mutableCopy];
    }
    NSMutableArray *distincProductRepresentations = [@[] mutableCopy];
    
    BOOL found;
    for(NSDictionary *rep in productRepresentations){
        found = NO;
        for(NSDictionary *r in distincProductRepresentations){
            if([r[BSProductDatabaseIdKey] isEqualToString:rep[BSProductDatabaseIdKey]]){
                found = YES;
                break;
            }
        }
        if(!found && ![arrayWithExistingObjectIds containsObject:rep[BSProductDatabaseIdKey]]){
            [distincProductRepresentations addObject:rep];
        }
    }
    return @{kBSNProductsKey : distincProductRepresentations, kBSNOrdersKey : orderRepresentations};
}

- (void) storeUserOrdersHistory:(NSDictionary *)orders{
    
    [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeOrder)] matchingPredicate:nil];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@",BSProductCategoryKey, @(BSProductCategoryNone)]];
    [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] matchingPredicate:filterPredicate];
    
    NSDictionary *dict = [self productsAndOrdersFromOrdersDictionary:orders];
    
    if([orders[kBSNOrdersKey] count]){
        [self insertObjects:dict[kBSNProductsKey] ofEntity:self.entitiesDictionary[@(BSEntityTypeProduct)]];
        [self insertObjects:dict[kBSNOrdersKey] ofEntity:self.entitiesDictionary[@(BSEntityTypeOrder)]];
    }
    [self updateObject:self.storedUserInfo withValuesAndKeys:@{BSOrdersRequestedKey : @(YES), BSRequestedHistoryCount : @([orders[kBSNOrdersKey] count]), BSOrdersCountKey : @([orders[kBSNOrdersKey] count])}];
}

- (void) storeUserCurrencyRate:(NSDictionary *)currencies
{
    NSNumber *currencyRate = currencies[BSEURCurrency];
    if(currencyRate){
        NSDictionary *currencyDict = @{BSCurrencyRatesKey : currencies, BSCurrencyExpirationKey : [[NSDate date] midnight]};
        NSAssert([[self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeUser)] withPredicate:nil] count] == 1, @"Invalid number of users!");
        [self updateObject:self.userInfo withValuesAndKeys:@{BSCurrencyRatesDictionaryKey : currencyDict}];
    }else{
        NSAssert(nil, @"No currency here!!!");
    }
}

- (void) storeUserEmail:(NSString *)email andAccessTokenFromResponse:(NSDictionary *)serverResponse
{
    NSAssert(serverResponse[kBSNAccessTokenKey] != nil, @"FATAL ERROR!!!");
    [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeUser)] matchingPredicate: nil];
    [self insertObjects:@[@{kBSNEmail : email, BSAccessTokenPropertyKey : serverResponse[kBSNAccessTokenKey], BSPreferredCurrencyKey : BSRONCurrency}] ofEntity:self.entitiesDictionary[@(BSEntityTypeUser)]];
    NSLog(@"Successfully stored: %@", email);
    [self reloadUserInfo];
}

- (void) storeUserShippingDetails:(NSDictionary *)shipping
{
    NSAssert([[self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeUser)] withPredicate:nil] count] == 1, @"Invalid number of users!");
    NSMutableDictionary *propertiesDict;
    if(shipping[kBSNShippingKey]){
        propertiesDict = [shipping[kBSNShippingKey] mutableCopy];
    }else{
        if(shipping[kBSNAddressLine]){
            propertiesDict = [shipping mutableCopy];
        }
    }
    [propertiesDict setValue:@(YES) forKey:BSShippingRequestedKey];
    [self updateObject:self.userInfo withValuesAndKeys:propertiesDict];
    NSLog(@"Successfully stored: %@", propertiesDict);
}

- (NSArray *)fetchedVendorsArray
{
    return self.vendorsArray;
}

- (NSArray *) fetchProductsForCategory:(BSProductCategory)category{
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@",BSProductCategoryKey, @(category)]];
    NSArray *array = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] withPredicate:filterPredicate];
    return array;
}

- (Vendor *) vendorForIdentifier:(NSString *)identifier
{
    NSString *format = [NSString stringWithFormat:@"%@ matches[c] %%@", BSVendorIdentifierKey];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:format, identifier];
    NSArray *vendors = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeVendor)] withPredicate:filterPredicate];
    NSAssert(vendors.count == 1, @"Invalid number of objects for identifier!!!");
    return vendors.lastObject;
}

- (void) deleteAll
{
    for(NSNumber *key in self.entitiesDictionary){
        [self deleteObjectsOfEntity:self.entitiesDictionary[key] matchingPredicate:nil];
    }
    _vendorsArray = nil;
    _userInfo = nil;
}
#pragma mark - Validation

- (BOOL) isProductDictionaryValid:(NSDictionary *)dict
{
    if(!dict[kBSNProductDescription] || !dict[kBSNProductIdentifier] || !dict[kBSNProductImageURLs] || !dict[kBSNProductPageURL] || !dict[kBSNProductPrice] || !dict[kBSNProductStock] || !dict[kBSNProductTitle] || !dict[kBSNVendorIdKey] || !dict[kBSNIDKey]){
        NSAssert(nil, @"Invalid dictionary to initialize object");
        return NO;
    }else{
        return YES;
    }
}

#pragma mark - Private methods

- (void) updateProductsStocks:(NSDictionary *)responseDict
{
    NSArray *products = responseDict[kBSNProductsKey];
    NSMutableArray *identifiersArray = [@[] mutableCopy];
    
    for(NSDictionary *dict in products){
        [identifiersArray addObject:dict[BSIdentifierKey]];
    }
    
    NSString *vendorId = responseDict[kBSNVendorIdKey];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY %K IN %@", BSProductIdentifierKey, identifiersArray];
    
    NSArray * array = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] withPredicate:predicate];
    for (Product *p in array){
        if([p.vendorIdentifier isEqualToString:vendorId]){
            for(NSDictionary *dict in products){
                if([p.productIdentifier isEqualToString:dict[BSIdentifierKey]]){
                    [self updateObject:p withValuesAndKeys: @{BSProductStockKey : dict[kBSNProductStock]}];
                    break;
                }
            }
        }
    }
}

- (void) storeVendors:(NSDictionary *)responseDict
{
    [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeVendor)] matchingPredicate: nil];
    _vendorsArray = nil;
    NSArray *vendorsArray = responseDict[kBSNVendorsKey];
    NSAssert(vendorsArray != nil, @"Vendors array cannot be nil.");
    NSMutableArray *vendorsInfoArray = [@[] mutableCopy];
    for(NSDictionary *dict in vendorsArray){
        [vendorsInfoArray addObject:[self dictionaryRepresentationForEntityType:BSEntityTypeVendor withServerInfo:dict]];
    }
    [self insertObjects:vendorsInfoArray ofEntity:self.entitiesDictionary[@(BSEntityTypeVendor)]];
}

- (void) addProduct:(NSDictionary *)responseDict
{
    NSString *categoryString = responseDict[kBSNProductCategory];
    NSDictionary *productDict = responseDict[kBSNproductKey];
    NSAssert([self isProductDictionaryValid:productDict] && categoryString != nil, @"Invalid response from server.");
    NSDictionary *entityRep = [self dictionaryForProductEntityWithServerInfo:productDict andCategory:[categoryString isEqualToString:kBSNProductCategoryShoppingCart]?BSProductCategoryShoppingCart:BSProductCategoryWishList];
    
    BSProductCategory category = [categoryString isEqualToString:kBSNProductCategoryShoppingCart]?BSProductCategoryShoppingCart:BSProductCategoryWishList;
    BSProductCategory otherCategory = category == BSProductCategoryWishList?BSProductCategoryShoppingCart:BSProductCategoryWishList;
    NSArray *otherCategoryProducts = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@ OR %@ = %@",BSProductCategoryKey, @(otherCategory), BSProductCategoryKey, @(category)]]];
    
    for(Product *p in otherCategoryProducts){
        if([p.databaseId isEqualToString:productDict[kBSNIDKey]]){
            // this should never ever ever ever happen
            NSAssert(nil, @"Product already stored in the wishlist or shopping cart!!!");
        }
    }
    
    [self insertObjects:@[entityRep] ofEntity:self.entitiesDictionary[@(BSEntityTypeProduct)]];
}

- (void) insertProducts:(NSDictionary *)products inCategory:(BSProductCategory)category
{
    [self deleteObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] matchingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@",BSProductCategoryKey, @(category)]]];
    NSArray *productsArray = products[(category == BSProductCategoryShoppingCart)?kBSNProductCategoryShoppingCart:kBSNProductCategoryWishList];
    NSAssert(productsArray != nil, @"Products array cannot be nil!");
    NSMutableArray *productsInfosArray = [@[] mutableCopy];
    
    BSProductCategory otherCategory = category == BSProductCategoryWishList?BSProductCategoryShoppingCart:BSProductCategoryWishList;
    NSArray *otherCategoryProducts = [self fetchObjectsOfEntity:self.entitiesDictionary[@(BSEntityTypeProduct)] withPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@",BSProductCategoryKey, @(otherCategory)]]];
    
    for(NSDictionary *dict in productsArray){
        for(Product *p in otherCategoryProducts){
            if([p.databaseId isEqualToString:dict[kBSNIDKey]]){
                // this should never ever ever ever happen
                NSAssert(nil, @"Product already stored in the other category!!!!");
            }
        }
        [productsInfosArray addObject:[self dictionaryForProductEntityWithServerInfo:dict andCategory:category]];
    }
    [self insertObjects:productsInfosArray ofEntity:self.entitiesDictionary[@(BSEntityTypeProduct)]];
    [self updateObject:self.storedUserInfo withValuesAndKeys:@{BSProductsListsRequested : @(YES)}];
}

- (NSDictionary *) dictionaryRepresentationForEntityType:(BSEntityType)type withServerInfo:(NSDictionary *)dict{
    NSAssert(type != BSEntityTypeProduct, @"Use dictionaryForProductEntityWithServerInfo:andCategory: instead!");
    NSDictionary *resultedDictionary;
    if(type == BSEntityTypeVendor){
        resultedDictionary = @{BSVendorNameKey : dict[kBSNVendorTitle],
                               BSVendorIdentifierKey : dict[kBSNVendorIdentifier],
                               BSVendorCurrencyKey : dict[kBSNVendorCurrency],
                               BSVendorLogoURLKey : dict[kBSNVendorLogoURL],
                               BSVendorApiKey : dict[kBSNVendorApiKey],
                               BSVendorEmailKey : dict[kBSNVendorEmail]
                               };
    }
    return resultedDictionary;
}

- (NSDictionary *) dictionaryForProductEntityWithServerInfo:(NSDictionary *)dict andCategory:(BSProductCategory)category
{
    NSAssert((dict != nil && [self isProductDictionaryValid:dict]), @"Fatal error.");
    
    return @{ BSProductTitleKey : dict[kBSNProductTitle],
              BSProductDescriptionKey : dict[kBSNProductDescription],
              BSProductStockKey : dict[kBSNProductStock],
              BSProductPriceKey : dict[kBSNProductPrice],
              BSProductVendorIdentifierKey : dict[kBSNVendorIdKey],
              BSProductImageURLsArray : dict[kBSNProductImageURLs],
              BSProductURLKey : dict[kBSNProductPageURL],
              BSProductIdentifierKey : dict[kBSNProductIdentifier],
              BSProductDatabaseIdKey : dict[kBSNIDKey],
              BSProductCategoryKey  : @(category)
              };
}

@end