//
//  BSXMLParser.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSXMLParser : NSObject <NSXMLParserDelegate>
/**
 *  Converts the xml data to a NSDictionary object
 *
 *  @param data             the xml data
 *  @param errorPointer     the error object
 *
 *  @return                 dictionary representation of the xml data
 */
+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)errorPointer;
/**
 *  Converts the xml string to a NSDictionary object
 *
 *  @param string               the xml string
 *  @param errorPointer         the error object
 *
 *  @return                     dictionary representation of the xml string
 */
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)errorPointer;

@end
