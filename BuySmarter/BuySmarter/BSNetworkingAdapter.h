//
//  BSNetworkingManager.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSNetworkingManager.h"
#import "BSConstants.h"

@interface BSNetworkingAdapter : BSNetworkingManager

/**
 *  Method for accesssing the singleton instance
 *
 *  @return the instance of the networking adapter
 */
+ (BSNetworkingAdapter *)sharedInstance;
/**
 *  Perform a specific operation
 *
 *  @param operationType    the type of the operation
 *  @param data             the payload data to be sent (nil when the operation makes a GET request)
 *  @param completionBlock  the completion block to be called after the connection receives a response
 */
- (void) performOperation:(BSNetworkOperationType)operationType  withAdditionalData:(NSDictionary *)data completionBlock:(BSOperationBlock)completionBlock;
/**
 *  Cancel any active connection for the specified operation type
 *
 *  @param operation        the operation for which the connection should be canceled
 */
- (void) cancelOperation:(BSNetworkOperationType)operation;
@end
