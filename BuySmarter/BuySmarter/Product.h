//
//  Product.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/12/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

static NSString * const BSProductTitleKey = @"productTitle";
static NSString * const BSProductDescriptionKey = @"productDescription";
static NSString * const BSProductStockKey = @"productStock";
static NSString * const BSProductPriceKey = @"productPrice";
static NSString * const BSProductVendorIdentifierKey = @"vendorIdentifier";
static NSString * const BSProductImageURLsArray = @"imageURLsArray";
static NSString * const BSProductURLKey = @"productURL";
static NSString * const BSProductIdentifierKey = @"productIdentifier";
static NSString * const BSProductCategoryKey = @"productCategory";
static NSString * const BSProductDatabaseIdKey = @"databaseId";

@interface Product : NSManagedObject

@property (nonatomic, retain) id imageURLsArray;
@property (nonatomic, retain) NSString * productDescription;
@property (nonatomic, retain) NSString * productIdentifier;
@property (nonatomic, retain) NSNumber * productPrice;
@property (nonatomic, retain) NSNumber * productStock;
@property (nonatomic, retain) NSString * productTitle;
@property (nonatomic, retain) NSString * productURL;
@property (nonatomic, retain) NSString * vendorIdentifier;
@property (nonatomic, retain) NSNumber * productCategory;
@property (nonatomic, retain) NSString * databaseId;

@end
