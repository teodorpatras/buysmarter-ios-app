//
//  BSPhotoCollectionView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/3/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSPhotoGalleryView : UIView <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

/**
 *  Factory method
 *
 *  @param frame        the frame of the gallery view
 *  @param imageUrls    the urls for the images
 *
 *  @return instance of the photo gallery view
 */
+ (BSPhotoGalleryView *) photoGalleryViewForFrame:(CGRect)frame withDataSourceArray:(NSArray *) imageUrls;
/**
 *  Reloads the information
 */
- (void) reloadData;

@end
