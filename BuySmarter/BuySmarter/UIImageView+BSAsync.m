//
//  UIImageView+BSAsync.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "UIImageView+BSAsync.h"
#import "BSImageLoader.h"

static const float BSFadeAnimationDuration = 0.55;
static NSString * const BSChangeImageAnimation = @"change_image_animation";

@implementation UIImageView (BSAsync)

- (void) setImageFromURL:(NSString *)URL placeHolderImage:(UIImage *)placeholder
{
    [self setImage:placeholder];
    if([[BSImageLoader sharedInstance] cachedImageFromURL:URL]){
        [self setImage:[[BSImageLoader sharedInstance] cachedImageFromURL:URL]];
    }else{
        [[BSImageLoader sharedInstance] getImageWithURL:URL withCompletionBlock:^(UIImage *img, NSString *error){
            if(error){
                NSLog(@"ERROR WHILE DOWNLOADING THE IMAGE FROM %@! [%@]", URL, error);
            }else{
                [self setImage:img animated:YES];
            }
        }];
    }
}

- (void) setImage:(UIImage *)image animated:(BOOL)animated
{
    if(!animated){
        [self setImage:image];
    }else{
        CATransition *transition = [CATransition animation];
        transition.duration = BSFadeAnimationDuration;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        [self.layer addAnimation:transition forKey:BSChangeImageAnimation];
        [self setImage:image];
        [self.layer removeAnimationForKey:BSChangeImageAnimation];
    }
}

@end
