//
//  UIViewController+BSAuth.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "UIViewController+BSAuth.h"
#import "BSSlidingMenuViewController.h"
#import "BSAuthViewController.h"
#import "BSSessionManager.h"

@implementation UIViewController (BSAuth)

- (void)logOut
{
    if([[self.slidingMenuViewController presentingViewController] isKindOfClass:[BSAuthViewController class]]){
        [(BSAuthViewController *)[self.slidingMenuViewController presentingViewController] resetToInitialState];
    }
    [[BSSessionManager sharedInstance] logOut];
    [[self.slidingMenuViewController presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

@end
