//
//  BSAuthDelegate.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BSAuthDelegate <NSObject>
/**
 *  Informs the delegate to commence the login
 *
 *  @param email        input email
 *  @param password     input password
 */
- (void) commenceLoginWithEmail:(NSString *)email andPasword:(NSString *)password;
/**
 *  Informs the delegate to commence the registration
 *
 *  @param email        input email
 *  @param password     input password
 */
- (void) commenceRegistrationWithEmail:(NSString *)email andPasword:(NSString *)password;

@end
