//
//  BSPayPalCell.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vendor.h"

@interface BSPayPalCell : UITableViewCell
/**
 *  Configuration method
 *
 *  @param mainAmount           the main amount
 *  @param mainCurrency         the currency for the main amount
 *  @param secondaryAmount      the secondary amount
 *  @param secondaryCurrency    the currency for the secondary amount
 */
- (void) configureWithMainAmount:(CGFloat)mainAmount currency:(NSString *)mainCurrency secondaryAmount:(CGFloat)secondaryAmount currency:(NSString *)secondaryCurrency;

@end
