//
//  BSSettingsViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/19/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSSettingsViewController.h"
#import "BSShippingDetailsViewController.h"
#import "BSConstants.h"
#import "BSCoreDataAdapter.h"
#import "BSNetworkingAdapter.h"
#import "BSSessionManager.h"
#import "BSUIManager.h"
#import "BSIndicatorView.h"
#import "BSConnectionManager.h"
#import "BSRegisterViewController.h"
#import "BSConstants.h"
#import "BSLocalizationManager.h"

typedef NS_ENUM(NSUInteger, BSEditingType) {
    BSEditingTypeShippingDetails,
    BSEditingTypePassword
};

@interface BSSettingsViewController ()
{
    BSEditingType _editingType;
    BOOL _finishedSyncingShippingInfo;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *currencyControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *languageControl;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *informationalView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *informationalLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (nonatomic) BSShippingDetailsViewController *shippingDetailsViewController;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *refreshingLabel;
@property (nonatomic) BSRegisterViewController *registerViewController;
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@end

@implementation BSSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    [self verifyAndDownloadInfoIfNeeded];
    [self configureLocalization];
}

- (void) configureLocalization
{
    [self.actionButton setTitle:_editingType == BSEditingTypeShippingDetails? LOCALIZED_STRING(@"settings_change_password_button_title"):LOCALIZED_STRING(@"settings_change_shipping_details_title")forState:UIControlStateNormal];
    self.languageLabel.text = LOCALIZED_STRING(@"settings_language_text");
    self.currencyLabel.text = LOCALIZED_STRING(@"settings_currency_text");
    [self.saveButton setTitle:LOCALIZED_STRING(@"save_button_title")];
    [_refreshingLabel setText:LOCALIZED_STRING(@"indicator_loading")];
    self.title = LOCALIZED_STRING(@"settings_screen_title");
}

- (void) configureView
{
    _currencyControl.selectedSegmentIndex = [[BSSessionManager sharedInstance] preferredUserCurrencyAsEnum];
    [self.saveButton setEnabled:NO];
    [self.indicatorView setColor:BSCOLOR];
    [BSUIManager roundCornersForView:self.actionButton];
    if(USER_INFO.preferredLanguage){
        self.languageControl.selectedSegmentIndex = [BSLocalizationManager getTypeForLanguage:USER_INFO.preferredLanguage];
    }
    [self.registerViewController configureForSettingsScreen];
}

- (void) verifyAndDownloadInfoIfNeeded
{
    if(!USER_INFO.shippingRequested.boolValue){
        if(INTERNET_REACHABLE){
            [self setInformationalViewHidden:NO];
            [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGETShippingDetails withAdditionalData:nil completionBlock:^(NSNumber *statusCode, NSDictionary *responseDictionary, NSString *error){
                if(error){
                    [self setInformationalViewHidden:YES];
                }else{
                    if(statusCode.intValue == 200){
                        [self setInformationalViewHidden:YES];
                        [self.saveButton setEnabled:YES];
                        [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDictionary forOperationType:BSNetworkOperationTypeGETShippingDetails];
                        [self.shippingDetailsViewController populateFieldsWithUser:USER_INFO];
                        [self.shippingDetailsViewController setEditable:YES];
                        [self.saveButton setEnabled:YES];
                        _finishedSyncingShippingInfo = YES;
                    }else if(statusCode.intValue == 405){
                        [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                    }
                }
            }];
        }else{
            [self setInformationalViewHidden:YES];
            [BSUIManager showInternetNotReachableAlert];
        }
    }else{
        [self.shippingDetailsViewController populateFieldsWithUser:USER_INFO];
        [self.shippingDetailsViewController setEditable:YES];
        _finishedSyncingShippingInfo = YES;
        [self.saveButton setEnabled:YES];
    }
}
- (void) setInformationalViewHidden:(BOOL) hidden
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.informationalView setAlpha:hidden ? 0 : 1];
    }completion:nil];
}
#pragma mark - Accessor Methods

- (BSRegisterViewController *)registerViewController
{
    if(!_registerViewController){
        _registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSRegisterViewController"];
        _registerViewController.view.frame = (CGRect){.size = self.shippingDetailsViewController.view.frame.size};
    }
    return _registerViewController;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embedShipping"]){
        self.shippingDetailsViewController = segue.destinationViewController;
        [self.shippingDetailsViewController setEditable:NO];
    }
}

#pragma mark - Callbacks

- (IBAction)languageChanged:(UISegmentedControl *)sender {
    [[BSCoreDataAdapter sharedInstance] updateObject:USER_INFO withValuesAndKeys:@{BSPreferredLanguageKey : SupportedLanguages[sender.selectedSegmentIndex]}];
    [BSLocalizationManager setLocalizedLanguage:SupportedLanguages[sender.selectedSegmentIndex]];
    [[BSSessionManager sharedInstance] postLanguageChangedNotification];
    [self configureLocalization];
}
- (IBAction)currencyChanged:(UISegmentedControl *)sender {
    [[BSSessionManager sharedInstance] preferredCurrencyChanged:sender.selectedSegmentIndex];
}

- (IBAction)hideKeyboard:(id)sender {
    [[BSSessionManager sharedInstance] postHideKeyboardNotification];
}

- (IBAction)changeAction {
    [self.actionButton setUserInteractionEnabled:NO];
    if(_editingType == BSEditingTypePassword){
        _saveButton.enabled = _finishedSyncingShippingInfo;
        _editingType = BSEditingTypeShippingDetails;
    }else{
        _saveButton.enabled = YES;
        _editingType = BSEditingTypePassword;
    }
    [UIView animateWithDuration:0.8 animations:^{
        [UIView setAnimationTransition:_editingType == BSEditingTypeShippingDetails ?
        UIViewAnimationTransitionFlipFromLeft : UIViewAnimationTransitionFlipFromRight
                               forView:self.containerView cache:YES];
        [self.containerView addSubview:_editingType == BSEditingTypePassword ? self.registerViewController.view : self.shippingDetailsViewController.view];
        [self configureActionButtonForType:_editingType];
    }completion:^(BOOL completed){
        if(completed){
            [self.actionButton setUserInteractionEnabled:YES];
        }
    }];
}

- (void) configureActionButtonForType:(BSEditingType)type
{
    if(type == BSEditingTypePassword){
        [_actionButton setBackgroundColor:BSCOLOR];
        [_actionButton setTitle:LOCALIZED_STRING(@"settings_change_shipping_details_title") forState:UIControlStateNormal];
    }else{
        [_actionButton setBackgroundColor:[UIColor redColor]];
        [_actionButton setTitle:LOCALIZED_STRING(@"settings_change_password_button_title") forState:UIControlStateNormal];
    }
}

- (IBAction)saveAction{
    
    BOOL shouldSaveShippingInfo = YES;
    BOOL validationErrorOccured = NO;
    NSDictionary *payloadDictionary;
    [[BSSessionManager sharedInstance] postHideKeyboardNotification];
    if(_editingType == BSEditingTypeShippingDetails){
        NSDictionary *dict = self.shippingDetailsViewController.getValidInputs;
        
        if(!dict){
            validationErrorOccured = YES;
        }
        
        shouldSaveShippingInfo = NO;
        
        for(NSString *key in dict){
            if(![dict[key] isEqual:[USER_INFO valueForKey:key]]){
                shouldSaveShippingInfo = YES;
                break;
            }
        }
        
        if(shouldSaveShippingInfo){
            payloadDictionary = [self.shippingDetailsViewController getValidInputs];
            if(!payloadDictionary){
                validationErrorOccured = YES;
            }
        }
    }else{
        NSString *newPass;
        NSString *oldPass;
        [self.registerViewController getNewPassword:&newPass andOldPassword:&oldPass];
        if(newPass && oldPass){
            payloadDictionary = @{kBSNNewPassword : newPass, kBSNOldPassword : oldPass};
        }

    }
    BSNetworkOperationType operation = _editingType == BSEditingTypePassword ? BSNetworkOperationTypeUpdatePassword : BSNetworkOperationTypeUpdateShippingDetails;
    
    if(INTERNET_REACHABLE){
        if(payloadDictionary){
            [[BSSessionManager sharedInstance] postHideKeyboardNotification];
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_saving")];
            [[BSNetworkingAdapter sharedInstance] performOperation:operation withAdditionalData:payloadDictionary completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
                [BSIndicatorView hide];
                
                if(error){
                    [BSUIManager showServerErrorAlert];
//                    if(_editingType == BSEditingTypeShippingDetails){
//                        NSLog(@"Error while saving shipping details: %@", error);
//                    }else{
//                        NSLog(@"Error while saving password: %@", error);
//                    }
                }else{
                    if(statusCode.integerValue == 200){
                        if(_editingType == BSEditingTypeShippingDetails){
                            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_successfully_saved")];
                            [[BSCoreDataAdapter sharedInstance] storeUserShippingDetails:@{kBSNShippingKey : payloadDictionary}];
                        }else{
                            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_successfully_changed_password")];
                            [self.registerViewController resetFields];
                            [self changeAction];
                        }
                    }else if(statusCode.integerValue == 405){
                        [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                    }else{
                        if(_editingType == BSEditingTypeShippingDetails){
                            [BSUIManager showServerErrorAlert];
                        }else{
                            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(@"alert_invalid_current_password")];
                        }
                    }
                }
            }];
        }else{
            if(!shouldSaveShippingInfo && !validationErrorOccured)
            {
                [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_info_title") andText:LOCALIZED_STRING(@"alert_nothing_to_save")];
            }
        }
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}


@end
