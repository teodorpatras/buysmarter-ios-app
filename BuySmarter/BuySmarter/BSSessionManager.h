//
//  BSSessionManager.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, BSSupportedCurrency) {
    BSSupportedCurrencyRON,
    BSSupportedCurrencyEUR
};

@interface BSSessionManager : NSObject
/**
 *  Method for accesssing the singleton instance
 *
 *  @return     singleton instance
 */
+ (BSSessionManager *)sharedInstance;
/**
 *  Empties the images cache and deletes everything from Core Data
 */
- (void) logOut;
/**
 *  Post a notification to hide the keyboard
 */
- (void) postHideKeyboardNotification;
/**
 *  Post a notification for the specified event
 */
- (void) postWillEnterForeground;
/**
 * Post a notification for the specified event
 */
- (void) postLanguageChangedNotification;
/**
 *  Saves the currency to core data
 */
- (void) preferredCurrencyChanged:(BSSupportedCurrency)currency;
/**
 *  Returns the preferred currency
 */
- (BSSupportedCurrency)preferredUserCurrencyAsEnum;
/**
 *  Converts the amount to the prefered currency
 *
 *  @param amount       the amount
 *  @param currency     the currency of the amount
 *
 *  @return new amount value for the prefered currency
 */
- (CGFloat) convertedAmountFromTotalAmount:(CGFloat) amount withCurrency:(NSString *)currency;
@end
