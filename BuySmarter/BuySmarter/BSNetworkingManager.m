
//
//  BSNetworkManager.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSNetworkingManager.h"
#import "BSConnectionPacket.h"

@interface BSNetworkingManager ()
@property (nonatomic) NSMutableDictionary *activeConnectionsPackets;
@end

@implementation BSNetworkingManager

#pragma mark - accessor methods

- (NSMutableDictionary *)activeConnectionsPackets
{
    if(!_activeConnectionsPackets){
        _activeConnectionsPackets =[@{} mutableCopy];
    }
    return _activeConnectionsPackets;
}

#pragma mark - public methods

- (void)cancelAllConnections
{
    for(NSString *key in self.activeConnectionsPackets){
        [[self.activeConnectionsPackets[key] connection] cancel];
    }
    [self.activeConnectionsPackets removeAllObjects];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void) newPOSTRequestForURL:(NSString *)url headers:(NSDictionary *)headers postData:(NSData *)data completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if(!self.activeConnectionsPackets[url]){
        NSMutableURLRequest *request = [self requestWithURLAddress:url andHeaders:headers];
        request.HTTPMethod = @"POST";
        request.HTTPBody = data;
        BSConnectionPacket *packet = [[BSConnectionPacket alloc] initWithCompletionBlock:completionBlock requestedURL:url andOperationType:BSOperationTypePOST];
        [self newConnectionForRequest:request withPacket:packet];
    }else{
        if([self.activeConnectionsPackets[url] operationType] == BSOperationTypePOST){
            [self.activeConnectionsPackets[url] registerNewCompletionBlock:completionBlock];
        }else{
            completionBlock(nil, nil, nil, [NSString stringWithFormat:@"A connection for [%@] is already in progress!", url]);
        }
    }
}

- (void) newGETRequestForURL:(NSString *)url headers:(NSDictionary *)headers completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if(!self.activeConnectionsPackets[url]){
        NSMutableURLRequest *request = [self requestWithURLAddress:url andHeaders:headers];
        request.HTTPMethod = @"GET";
        BSConnectionPacket *packet = [[BSConnectionPacket alloc] initWithCompletionBlock:completionBlock requestedURL:url andOperationType:BSOperationTypeGET];
        [self newConnectionForRequest:request withPacket:packet];
    }else{
        if([self.activeConnectionsPackets[url] operationType] == BSOperationTypeGET){
            [self.activeConnectionsPackets[url] registerNewCompletionBlock:completionBlock];
        }else{
            completionBlock(nil, nil, nil, [NSString stringWithFormat:@"A connection for [%@] is already in progress!", url]);
        }
    }
}

- (void)cancelConnectionForURL:(NSString *)url
{
    BSConnectionPacket *packet = self.activeConnectionsPackets[url];
    if(packet){
        [packet.connection cancel];
        packet.connection = nil;
        [self.activeConnectionsPackets removeObjectForKey:url];
    }
}
#pragma mark - private methods

- (void) newConnectionForRequest:(NSMutableURLRequest *)request withPacket:(BSConnectionPacket *)packet
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    // Create url connection and fire request
    packet.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [self.activeConnectionsPackets setValue:packet forKey:packet.requestedURL];
}

- (NSMutableURLRequest *) requestWithURLAddress:(NSString *)address andHeaders:(NSDictionary *)headers
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:address]];
    for(NSString *key in headers){
        [request setValue:headers[key] forHTTPHeaderField:key];
    }
    [request setTimeoutInterval:15.0];
    return request;
}

- (BSConnectionPacket *) packetForConnection:(NSURLConnection *)connection
{
    BSConnectionPacket *result;
    for(NSString *key in self.activeConnectionsPackets){
        if([[self.activeConnectionsPackets[key] connection] isEqual:connection]){
            result = self.activeConnectionsPackets[key];
            break;
        }
    }
    return result;
}

- (void) cleanupConnection:(NSURLConnection *)connection
{
    if(!connection){
        return;
    }
    BSConnectionPacket *packet = [self packetForConnection:connection];
    NSString *key =  [packet requestedURL];
    [self.activeConnectionsPackets removeObjectForKey:key];
    if(self.activeConnectionsPackets.count == 0){
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    //NSLog(@"Succesfully cleaned up the connection! Remaining connections for: %@ -> [%@].",key, self.activeConnectionsPackets[key]);
}

#pragma mark - NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(!connection || !data){
        return;
    }
    [[self packetForConnection:connection] appendData:data];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    if(!self.activeConnectionsPackets || self.activeConnectionsPackets.count == 0){
        return;
    }
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    [[self packetForConnection:connection] setStatusCode:@([httpResponse statusCode])];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {

    if(!connection || !self.activeConnectionsPackets || !self.activeConnectionsPackets.count){
        return;
    }
    // get a hold of the packet before cleaning up the connection
    BSConnectionPacket *packet = [self packetForConnection:connection];
    // clean up the connection BEFORE performing the completion block
    [self cleanupConnection:connection];
    // perform the completion block, safe knowing that a new request for the same url can now be made
    [packet performCompletionWithErrorMessage:nil];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

    // get a hold of the packet before cleaning up the connection
    BSConnectionPacket *packet = [self packetForConnection:connection];
    // clean up the connection BEFORE performing the completion block
    [self cleanupConnection:connection];
    // perform the completion block, safe knowing that a new request for the same url can now be made
    [packet performCompletionWithErrorMessage:error.description];
}

@end
