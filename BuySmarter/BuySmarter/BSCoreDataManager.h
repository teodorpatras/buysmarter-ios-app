//
//  BSCoreDataManager.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/11/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSCoreDataManager : NSObject
/**
 *  Instantiates a new BSCoreDataManager object assigning to it the specified context
 *
 *  @param context      the context object
 *
 *  @return             instance of the core data manager
 */
- (id) initWithManagedObjectContext:(NSManagedObjectContext *)context;

/**
 *  Inserts the object of the entity name
 *
 *  @param objects      objects array
 *  @param entityName   the name of the entity
 */
- (void) insertObjects:(NSArray*)objects ofEntity:(NSString *)entityName;

/**
 *  Fetches objects of the entity name
 *
 *  @param entityName   the name of the entity
 *  @param predicate    the predicate for filtering options
 *
 *  @return array of fetched objects
 */
- (NSArray *) fetchObjectsOfEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate;

/**
 *  Deletes objects of the specified entity
 *
 *  @param entityName   the name of the entity
 *  @param predicate    the predicate for filtering options
 */
- (void) deleteObjectsOfEntity:(NSString *)entityName matchingPredicate:(NSPredicate *)predicate;

/**
 *  Deletes the specified object
 *
 *  @param object       object to be deleted
 */
- (void) deleteObject:(NSManagedObject *)object;

/**
 *  Gets an array of the values of the objects for the specified key
 *
 *  @param key          the key for the values
 *  @param entityName   the name of the entity
 *
 *  @return             an array with the values for the specified key
 */
- (NSArray *) objectValueForKey:(NSString *)key withEntityName:(NSString *)entityName;
/**
 *  Updates the object with the specified values
 *
 *  @param object         the object to be updated
 *  @param valuesWithKeys dictionary containing keys and values for the object
 */
- (void) updateObject:(NSManagedObject *) object withValuesAndKeys:(NSDictionary *)valuesWithKeys;
@end
