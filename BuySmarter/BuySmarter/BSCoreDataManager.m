//
//  BSCoreDataManager.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/11/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSCoreDataManager.h"
#import "BSAppDelegate.h"

@interface BSCoreDataManager (){
    NSManagedObjectContext *_managedObjectContext;
}
@end
@implementation BSCoreDataManager

#pragma mark - Initializer

- (id) initWithManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super init];
    if(self){
        _managedObjectContext = context;
    }
    return self;
}

#pragma mark - object manipulation

- (void) insertObjects:(NSArray*)objects ofEntity:(NSString *)entityName
{
    NSString *key;
    NSManagedObject *managedObject;
    NSError *error;
    for(NSDictionary *dict in objects){
        managedObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:_managedObjectContext];

        for(key in dict){
            [managedObject setValue:dict[key] forKey:key];
        }
    }
    [_managedObjectContext save:&error];
    NSAssert(error == nil, error.description);
}

- (NSArray *) fetchObjectsOfEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate{
    NSArray *array;
    NSError *error;
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    [fetchRequest setEntity:entityDescription];
    if(predicate){
        [fetchRequest setPredicate:predicate];
    }
    array = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSAssert(error == nil, error.description);
    return array;
}

- (void) deleteObjectsOfEntity:(NSString *)entityName matchingPredicate:(NSPredicate *)predicate
{
    NSArray *objects = [self fetchObjectsOfEntity:entityName withPredicate:predicate];
    for(NSManagedObject *object in objects){
        [self deleteObject:object];
    }
}

- (NSArray *) objectValueForKey:(NSString *)key withEntityName:(NSString *)entityName
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entity];
    NSError *error;
    NSArray *array = [_managedObjectContext executeFetchRequest:request error:&error];
    NSAssert(error == nil, error.description);
    return [array valueForKey:key];
}

- (void)updateObject:(NSManagedObject *)object withValuesAndKeys:(NSDictionary *)valuesWithKeys
{
    NSError *error;
    for(NSString * key in valuesWithKeys){
        [object setValue:valuesWithKeys[key] forKeyPath:key];
    }
    [_managedObjectContext save:&error];
    if(error){
        NSLog(@"ERROR WHILE SAVING OBJECT: %@", error.description);
    }
}

- (void) deleteObject:(NSManagedObject *)object
{
    [_managedObjectContext deleteObject:object];
    [_managedObjectContext save:nil];
}

@end
