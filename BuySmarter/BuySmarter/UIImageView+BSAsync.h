//
//  UIImageView+BSAsync.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (BSAsync)
/**
 *  Sets a placeholder image, downloads an image from the specified URL and then sets it
 *
 *  @param URL          url from where the image should be download
 *  @param placeholder  placeholder image to be displayed until the download finishes
 */
- (void) setImageFromURL:(NSString *)URL placeHolderImage:(UIImage *)placeholder;
/**
 *  Sets the image animated or not
 *
 *  @param image        image to be set
 *  @param animated     boolean parameter indicating if the animation should occur or not
 */
- (void) setImage:(UIImage *)image animated:(BOOL)animated;

@end
