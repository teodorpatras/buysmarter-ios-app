//
//  BSImageLoader.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSNetworkingManager.h"

@interface BSImageLoader : BSNetworkingManager
/**
 *  Method for accesssing the singleton instance
 *
 *  @return singleton instance
 */
+ (BSImageLoader *) sharedInstance;
/**
 *  Downloads the image from the specified URL
 *
 *  @param url              url from where the image should be downloaded
 *  @param completionBlock  block to be called after the download finishes
 */
- (void)getImageWithURL:(NSString *)url withCompletionBlock:(void (^)(UIImage *, NSString *))completionBlock;
/**
 *  Gets a cached image by an URL
 *
 *  @param URL  url for the cached image
 *
 *  @return     cached image
 */
- (UIImage *) cachedImageFromURL:(NSString *)URL;
/**
 *  Empties the images cache
 */
- (void) emptyCache;

@end
