//
//  BSValidationManager.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSValidationManager : NSObject
/**
 *  Registers a validator class with an identifier and error message
 *
 *  @param validatorClass   the class of the validator
 *  @param identifier       the identifier of the validator
 *  @param errorMessage     the error message
 */
- (void) registerClass:(Class)validatorClass withIdentifier:(NSString *)identifier andErrorMessage:(NSString *)errorMessage;
/**
 *  Registers an input with an identifier
 *
 *  @param input        input string to be registered
 *  @param identifier   identifier of the validator
 */
- (void) registerInput:(NSString *)input withValidatorIdentifier:(NSString *)identifier;
/**
 *  Attempts to validate the input for the specified validator identifier
 *
 *  @param input        input string
 *  @param identifier   the identifier of the validator
 *  @param error        the error message to be returned it the validation fails
 */
- (void) validateInput:(NSString *)input forIdentifier:(NSString *)identifier error:(NSString **)error;
/**
 *  Attempts to validate all the registered inputs
 *
 *  @param errorMessage     the error message to be returned it the validation fails
 */
- (void) validateInputs:(NSString **)errorMessage;

@end
