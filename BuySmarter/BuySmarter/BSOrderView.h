//
//  BSOrderView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Product.h"

@protocol BSOrderViewDelegate<NSObject>

- (void) orderViewDidSelectProduct:(Product *)product;

@end

@interface BSOrderView : UIView <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
@property (nonatomic) NSInteger viewIndex;

/**
 *  Factory method
 *
 *  @param delegate     the delegate of the view
 *
 *  @return             instance of the order view
 */
+ (BSOrderView *) orderViewWithDelegate:(id<BSOrderViewDelegate>)delegate;
/**
 *  Configures the cell with a dictionary
 *
 *  @param orderDict     configuration dictionary
 */
- (void)configureWithDictionary:(NSDictionary *)orderDict;
/**
 *  Attempts to reload the information within the view
 */
- (void) shouldReloadInfo;

@end
