//
//  BSDefaultBackgroundedView.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSDefaultBackgroundedView.h"

@implementation BSDefaultBackgroundedView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Bkg"]]];
    }
    return self;
}

@end
