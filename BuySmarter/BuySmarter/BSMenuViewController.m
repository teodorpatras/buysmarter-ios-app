//
//  BSMenuViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSMenuViewController.h"
#import "BSSlidingMenuViewController.h"
#import "UIViewController+BSAuth.h"
#import "BSSessionManager.h"
#import "BSConstants.h"

typedef NS_ENUM(NSInteger, BSSelectionType) {
    BSSelectionTypeHomeScreen,
    BSSelectionTypeOrdersHistory,
    BSSelectionTypeSettings,
    BSSelectionTypeAboutScreen,
    BSSelectionTypeLogout
};

static NSString * const kBSText = @"text";
static NSString * const kBSIcon = @"icon";
static NSString * const kBSSelectionType = @"index";

@interface BSMenuViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic)NSArray *dataSourceArray;
@end

@implementation BSMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.navigationItem.titleView = [[NSBundle mainBundle] loadNibNamed:@"MenuTitleView" owner:self options:nil][0];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MenuBackground"]]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configureLocalization) name:BSLanguageChangedNotification object:nil];
    [self configureLocalization];
    [self slidingMenuViewController];
}

- (void) configureLocalization
{
    _dataSourceArray = @[@{kBSText : LOCALIZED_STRING(@"home_screen_title"), kBSIcon: @"HomeIcon", kBSSelectionType : @(BSSelectionTypeHomeScreen)},
                         @{kBSText : LOCALIZED_STRING(@"orders_history_screen_title"), kBSIcon : @"OrdersHistory", kBSSelectionType : @(BSSelectionTypeOrdersHistory)},
                         @{kBSText : LOCALIZED_STRING(@"settings_screen_title"), kBSIcon: @"SettingsIcon", kBSSelectionType : @(BSSelectionTypeSettings)},
                         @{kBSText : LOCALIZED_STRING(@"about_screen_title"), kBSIcon: @"AboutIcon", kBSSelectionType : @(BSSelectionTypeAboutScreen)},
                         @{kBSText : LOCALIZED_STRING(@"logout_title"), kBSIcon: @"LogOutIcon", kBSSelectionType : @(BSSelectionTypeLogout)}];
    self.title = LOCALIZED_STRING(@"menu_screen_title");
    [self.tableView reloadData];
}

#pragma mark - UITableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"menuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:15.0]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    cell.textLabel.text = self.dataSourceArray[indexPath.row][kBSText];
    [cell.imageView setImage:[UIImage imageNamed:self.dataSourceArray[indexPath.row][kBSIcon]]];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([self.dataSourceArray[indexPath.row][kBSSelectionType] integerValue] == BSSelectionTypeLogout){
        [[[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_confirmation_needed") message:LOCALIZED_STRING(@"alert_are_you_sure_logout") delegate:self cancelButtonTitle:LOCALIZED_STRING(@"no_answer") otherButtonTitles:LOCALIZED_STRING(@"yes_answer"), nil] show];
    }else if([self.dataSourceArray[indexPath.row][kBSSelectionType] integerValue] == BSSelectionTypeSettings){
        [self.slidingMenuViewController performSegueWithIdentifier:@"showSettings" sender:self];
    }else if([self.dataSourceArray[indexPath.row][kBSSelectionType] integerValue] == BSSelectionTypeHomeScreen){
        [self.slidingMenuViewController performSegueWithIdentifier:@"init_front_segue" sender:self];
    }else if([self.dataSourceArray[indexPath.row][kBSSelectionType] integerValue] == BSSelectionTypeOrdersHistory){
        [self.slidingMenuViewController performSegueWithIdentifier:@"showOrdersHistory" sender:self];
    }else{
        [self.slidingMenuViewController performSegueWithIdentifier:@"embedAbout" sender:self];
    }
}

#pragma mark - UIAlerViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        // ok button
        [self.slidingMenuViewController hideMenuWithCompletionBlock:^{
            [self logOut];
        }];
    }
}


@end
