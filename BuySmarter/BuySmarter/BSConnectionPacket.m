//
//  BSConnectionPacket.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSConnectionPacket.h"

@interface BSConnectionPacket ()
@property (nonatomic) NSMutableArray *completionBlocks;
@property (nonatomic) NSString *requestedURL;
@property (nonatomic) NSMutableData *data;
@end

@implementation BSConnectionPacket

- (id) initWithCompletionBlock:(BSStandardCompletionBlock) completionBlock requestedURL:(NSString *)requestedURL andOperationType:(BSOperationType)type
{
    self = [super init];
    if(self){
        _completionBlocks = [@[completionBlock] mutableCopy];
        _requestedURL = requestedURL;
        _operationType = type;
    }
    return self;
}

- (void) registerNewCompletionBlock:(BSStandardCompletionBlock)compBlock
{
    [_completionBlocks addObject:compBlock];
}

#pragma mark - accessor methods 

-(NSMutableData *)data
{
    if(!_data){
        _data = [[NSMutableData alloc] init];
    }
    return _data;
}

- (NSString *)requestedURL
{
    return _requestedURL;
}

#pragma mark - public methods

- (void)appendData:(NSData *)data
{
    [self.data appendData:data];
}

- (void) performCompletionWithErrorMessage:(NSString *)errorMessage
{
    if(errorMessage){
        for(BSStandardCompletionBlock completionBlock in self.completionBlocks){
            completionBlock(self.requestedURL, nil, nil, errorMessage);
        }
    }else{
        for(BSStandardCompletionBlock completionBlock in self.completionBlocks){
            completionBlock(self.requestedURL, self.statusCode, self.data, nil);
        }
    }
}


@end
