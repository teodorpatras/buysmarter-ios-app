//
//  BSImageLoader.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSImageLoader.h"

static const int BSCacheLimit = 50;
static const float BSImageDimensionMax = 150;

@interface BSImageLoader ()
@property (nonatomic) NSCache *cache;
@end;

@implementation BSImageLoader

#pragma mark - Singleton

+ (BSImageLoader *)sharedInstance
{
    static BSImageLoader *loader = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loader = [BSImageLoader new];
    });
    return loader;
}

#pragma mark - accessor methods

- (NSCache *)cache
{
    if(!_cache){
        _cache = [[NSCache alloc] init];
        [_cache setCountLimit:BSCacheLimit];
    }
    return _cache;
}

#pragma mark - Public methods

- (void) emptyCache
{
    [self.cache removeAllObjects];
}

- (UIImage *) cachedImageFromURL:(NSString *)URL
{
    return [self.cache objectForKey:URL];
}

- (void)getImageWithURL:(NSString *)url withCompletionBlock:(void (^)(UIImage *, NSString *))completionBlock
{
    [self newGETRequestForURL:url headers:nil completionBlock:[self adapterBlockForBlock:completionBlock]];
}

#pragma mark - Utility methods

-  (void(^) (NSString *, NSNumber* number, NSData *, NSString*)) adapterBlockForBlock:(void(^) (UIImage *, NSString *))block
{
    return ^(NSString *requestedURL, NSNumber *statusCode, NSData *receivedData, NSString *errorMessage){
        
        if(errorMessage){
            block(nil, errorMessage);
        }else{
            if(statusCode.integerValue != 200){
                // !200 == !OK
                block(nil, @"Server error.");
            }else{
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(queue, ^{
                    UIImage *img = [self resizedImageFromData:receivedData];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(img){
                            [self.cache setObject:img forKey:requestedURL];
                            block(img, nil);
                        }else{
                            block(nil, [NSString stringWithFormat:@"Invalid image from server[%@]", requestedURL]);
                        }
                    });
                });

            }
        }
    };
}

- (UIImage *) resizedImageFromData:(NSData *)data
{
    UIImage *image = [[UIImage alloc] initWithData:data];
    
    float width = image.size.width;
    float height = image.size.height;
    
    // resize the image if it's too big
    if (width > BSImageDimensionMax || height > BSImageDimensionMax)
    {
        float scaleFactor = 0;
        
        if(width > height){
            scaleFactor = BSImageDimensionMax / width;
        }else{
            scaleFactor = BSImageDimensionMax / height;
        }
       // NSLog(@"Drawing w: %f h: %f to scale factor: %f", width, height, scaleFactor);
        CGSize itemSize = CGSizeMake(width * scaleFactor, height * scaleFactor);
        UIGraphicsBeginImageContextWithOptions(itemSize, NO, 0.0f);
        CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
        [image drawInRect:imageRect];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return image;
}


@end
