//
//  BSConnectionPacket.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSConstants.h"

typedef NS_ENUM(NSInteger, BSOperationType) {
    BSOperationTypeGET,
    BSOperationTypePOST
};

@interface BSConnectionPacket : NSObject

@property (nonatomic) NSNumber *statusCode;
@property (nonatomic) NSURLConnection *connection;
@property (nonatomic, readonly) BSOperationType operationType;
/**
 *  Initializing method
 *
 *  @param completionBlock  completion block to be called when the operation finishes
 *  @param requestedURL     url where the request should be made
 *  @param type             the type of the operation
 *
 *  @return                 instance of the initialized object
 */
- (id) initWithCompletionBlock:(BSStandardCompletionBlock) completionBlock requestedURL:(NSString *)requestedURL andOperationType:(BSOperationType) type;
/**
 *  Accessor method for the requested URL
 *
 *  @return requested URL
 */
- (NSString *)requestedURL;
/**
 *  Appends response data
 *
 *  @param data     new response data
 */
- (void) appendData:(NSData *)data;
/**
 *  Performs all the registered completion blocks
 *
 *  @param errorMessage     optional error message
 */
- (void) performCompletionWithErrorMessage:(NSString *)errorMessage;
/**
 *  Registers a new completion block
 *
 *  @param compBlock    new completion block
 */
- (void) registerNewCompletionBlock:(BSStandardCompletionBlock)compBlock;

@end
