//
//  BSProductDetailsViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "BSConstants.h"
#import "BSBaseViewController.h"

typedef NS_ENUM(NSUInteger, BSProductActionType) {
    BSProductActionTypeSwitch,
    BSProductActionTypeDiscard
};

@protocol BSProductDetailsViewControllerDelegate <NSObject>
/**
 *  Informs its delegate to reload the data source
 */
- (void) refreshDataSource;
/**
 *  Informs the delegate to move the product to a new category
 *
 *  @param category     the new category for the product
 */
- (void) changeToCategory:(BSProductCategory)category;

@end

@interface BSProductDetailsViewController : BSBaseViewController <UIActionSheetDelegate>
@property (nonatomic) Product *product;
@property (nonatomic) BOOL shouldConfigureForOrderProduct;
@property (nonatomic, weak) id<BSProductDetailsViewControllerDelegate> delegate;
@end
