//
//  BSNotEmptyValidator.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSAbstractValidator.h"

@interface BSNotEmptyValidator : BSAbstractValidator

@end
