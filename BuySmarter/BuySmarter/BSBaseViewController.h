//
//  BSBaseViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSBaseViewController : UIViewController<UIAlertViewDelegate>
/**
 *  Logs out the user while displaying an alert message
 *
 *  @param message  the message to be displayed to the user
 */
- (void) logOutUserWithMessage:(NSString *)message;

@end
