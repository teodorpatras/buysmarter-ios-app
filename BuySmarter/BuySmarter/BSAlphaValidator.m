//
//  BSAlphaValidator.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSAlphaValidator.h"

@implementation BSAlphaValidator

- (BOOL)execute
{
    if(!self.inputString){
        return NO;
    }
    // convert from ăâîșț to aaist
    NSData *asciiEncoded = [self.inputString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *newString = [[NSString alloc] initWithData:asciiEncoded encoding:NSASCIIStringEncoding];
    
    NSString *expression = @"^[a-zA-Z]+$";
    NSError *error = NULL;
    BOOL result;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:newString options:0 range:NSMakeRange(0, [self.inputString length])];
    if (match){
        result = YES;
    }else{
        result = NO;
    }
    return result;
}

@end
