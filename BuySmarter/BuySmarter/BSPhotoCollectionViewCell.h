//
//  BSPhotoCollectionViewCell.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/3/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSPhotoCollectionViewCell : UICollectionViewCell
/**
 *  Sets the image
 *
 *  @param image        the image to be set
 *  @param animated     whether or not the image should animate when being set
 */
- (void) setImage:(UIImage *)image animated:(BOOL)animated;
/**
 *  Removes the image from the image view
 */
- (void) clearImageView;

@end
