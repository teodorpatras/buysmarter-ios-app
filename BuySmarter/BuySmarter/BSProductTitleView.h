//
//  BSProductTitleView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface BSProductTitleView : UIView

/**
 *  Factory method
 *
 *  @param product      the product for the view to be configured with
 *
 *  @return             the instace of the product title view
 */
+ (BSProductTitleView *)titleViewForProduct:(Product *)product;

@end
