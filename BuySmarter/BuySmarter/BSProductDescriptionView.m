//
//  BSProductDescriptionView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductDescriptionView.h"
#import "BSConstants.h"

static const CGFloat BSFontSize = 15;

@interface BSProductDescriptionView()
@property (nonatomic) UILabel *descriptionLabel;
@end
@implementation BSProductDescriptionView

+ (BSProductDescriptionView *)descriptionViewForProduct:(Product *)product
{
    BSProductDescriptionView *view = [BSProductDescriptionView new];
    CGFloat height = [product.productDescription boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 2 * BSSidePadding, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:BSFontSize]} context:nil].size.height;
    [view setFrame:(CGRect){.size = {CGRectGetWidth([[UIScreen mainScreen] bounds]), height + BSSidePadding}}];
    view.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(BSSidePadding, 0, [UIScreen mainScreen].bounds.size.width - 2 * BSSidePadding, height)];
    view.descriptionLabel.numberOfLines = 15;
    view.descriptionLabel.font = [UIFont italicSystemFontOfSize:BSFontSize];
    [view addSubview: view.descriptionLabel];
    view.descriptionLabel.text = product.productDescription;
    return view;
}

@end
