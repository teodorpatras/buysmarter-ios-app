//
//  BSAboutViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/29/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSAboutViewController.h"

@interface BSAboutViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation BSAboutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:LOCALIZED_STRING(@"about_title")];
    [_webView loadHTMLString:LOCALIZED_STRING(@"about_html") baseURL:nil];
}

@end
