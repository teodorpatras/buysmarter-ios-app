//
//  BSScanViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/1/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSScanViewController.h"
#import "BSTransitionDelegate.h"
#import "BSConnectionManager.h"
#import "BSUIManager.h"
#import "BSConstants.h"

@interface BSScanViewController (){
    BSProductLoaderViewController *dialogViewController;
    BSTransitionDelegate *transitionDelegate;
    BOOL isAbleToPresent;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *dismissButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet ZBarReaderView *readerView;
@property (weak, nonatomic) IBOutlet UIView *scannerContainerView;
@end

@implementation BSScanViewController


#pragma mark - viewcontroller lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _readerView.readerDelegate = self;
    transitionDelegate = [BSTransitionDelegate new];
    _scannerContainerView.layer.contents = (id) [UIImage imageNamed:@"CameraPlaceholder"].CGImage;
    [BSUIManager configureViewWithBorderShadowAndRoundedCorners:_readerView];
     [_readerView start];
    [self configureLocalization];
}

- (void) configureLocalization
{
    self.title = LOCALIZED_STRING(@"scan_screen_title");
    self.infoLabel.text = LOCALIZED_STRING(@"scan_info_label_text");
    [self.dismissButton setTitle:LOCALIZED_STRING(@"scan_dismiss_button_title")];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    isAbleToPresent = YES;
}

- (void) viewDidDisappear: (BOOL) animated
{
    [super viewDidDisappear:animated];
    isAbleToPresent = NO;
    [_readerView stop];
}

#pragma mark - callbacks

- (IBAction)cancelAction:(id)sender {
    isAbleToPresent = NO;
    [self.delegate scannerViewControllerDidFinishWithDictionary:nil authenticationError:NO];
}

#pragma mark - ZBarReaderView delegate methods


- (void) readerView: (ZBarReaderView*) view
     didReadSymbols: (ZBarSymbolSet*) syms
          fromImage: (UIImage*) img
{
    for(ZBarSymbol *sym in syms) {
        
        if(sym.data.length){
            NSData *data = [sym.data dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if([self productJSONIsValid:json]){
                if([[BSConnectionManager sharedInstance] internetConnectionAvailable]){
                    [self showProductDialogWithDataDictionary:json];
                }else{
                    [BSUIManager showInternetNotReachableAlert];
                }
            }else{
                [BSUIManager showInvalidProductAlert];
            }
        }
        break;
    }
}

- (BOOL) productJSONIsValid:(NSDictionary *)json
{
    return json[kBSNProductIdentifier] && json[kBSNVendorIdKey];
}

- (void) showProductDialogWithDataDictionary:(NSDictionary *)dict
{
    [_readerView stop];
    dialogViewController =  [[BSProductLoaderViewController alloc] initWithNibName:@"BSProductLoaderViewController" bundle:nil];
    dialogViewController.delegate = self;
    dialogViewController.view.backgroundColor = [UIColor clearColor];
    dialogViewController.modalPresentationStyle = UIModalPresentationCustom;
    dialogViewController.transitioningDelegate = transitionDelegate;
    if(isAbleToPresent){
        [self presentViewController:dialogViewController animated:YES completion:^{
            [dialogViewController requestForProductWithDictionary:dict];
            [_scannerContainerView setHidden:YES];
        }];
    }
}

#pragma mark - BSProductLoaderDelegate method

- (void)productLoaderDidFinishWithDictionary:(NSDictionary *)dictionary authenticationError:(BOOL)authError
{
    if(authError){
        [self dismissViewControllerAnimated:NO completion:^{
            [self.delegate scannerViewControllerDidFinishWithDictionary:nil authenticationError:YES];
        }];
    }else{
        if(dictionary){
            [self dismissViewControllerAnimated:NO completion:^{
                [self.delegate scannerViewControllerDidFinishWithDictionary:dictionary authenticationError:authError];
            }];
        }else{
            [_scannerContainerView setHidden:NO];
            [UIView animateWithDuration:0.2 animations:^{
                dialogViewController.view.alpha = 0;
            }completion:^(BOOL completed){
                [self dismissViewControllerAnimated:NO completion:nil];
                [_readerView start];
            }];
        }
    }
}

@end