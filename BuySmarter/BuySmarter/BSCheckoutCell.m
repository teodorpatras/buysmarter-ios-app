//
//  BSCheckoutCell.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSCheckoutCell.h"
#import "Product.h"
#import "BSConstants.h"
#import "BSSessionManager.h"
#import "BSCoreDataAdapter.h"

@interface BSCheckoutCell()
@property (weak, nonatomic) IBOutlet UILabel *checkoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@end

@implementation BSCheckoutCell

- (void) configureWithVendor:(Vendor *)vendor andProductsArray:(NSArray *)productsArray
{
    CGFloat amount = 0;
    if(![self.backgroundColor isEqual:BSCOLOR]){
        [self.backgroundView setBackgroundColor:BSCOLOR];
        self.backgroundColor = BSCOLOR;
    }
    for(Product *p in productsArray){
        if(p.productStock.integerValue > 0){
            amount += p.productPrice.floatValue;
        }
    }
    if([USER_INFO.preferredCurrency isEqualToString:vendor.vendorCurrency]){
        self.amountLabel.text = [NSString stringWithFormat:@"%.2f", amount];
        self.currencyLabel.text = vendor.vendorCurrency;
    }else{
        self.amountLabel.text = [NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:amount withCurrency:vendor.vendorCurrency]];
        self.currencyLabel.text = USER_INFO.preferredCurrency;
    }
    self.checkoutLabel.text = LOCALIZED_STRING(@"checkout_label_text");
}

@end
