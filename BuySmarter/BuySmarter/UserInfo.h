//
//  User.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

static NSString * const BSAccessTokenPropertyKey = @"accessToken";
static NSString * const BSShippingRequestedKey = @"shippingRequested";
static NSString * const BSOrdersRequestedKey = @"ordersRequested";
static NSString * const BSProductsListsRequested = @"productsListsRequested";
static NSString * const BSCurrencyRatesDictionaryKey = @"currencyRatesDictionary";
static NSString * const BSPreferredCurrencyKey = @"preferredCurrency";
static NSString * const BSOrdersCountKey = @"ordersCount";
static NSString * const BSRequestedHistoryCount = @"requestedHistoryCount";
static NSString * const BSPreferredLanguageKey = @"preferredLanguage";

@interface UserInfo : NSManagedObject

@property (nonatomic, retain) NSString * accessToken;
@property (nonatomic, retain) NSString * addressLine;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) id currencyRatesDictionary;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * ordersCount;
@property (nonatomic, retain) NSNumber * ordersRequested;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) NSString * postalCode;
@property (nonatomic, retain) NSString * preferredCurrency;
@property (nonatomic, retain) NSNumber * productsListsRequested;
@property (nonatomic, retain) NSNumber * requestedHistoryCount;
@property (nonatomic, retain) NSNumber * shippingRequested;
@property (nonatomic, retain) NSString * preferredLanguage;

@end
