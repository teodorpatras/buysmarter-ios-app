//
//  BSAlphaValidator.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSAbstractValidator.h"

@interface BSAlphaValidator : BSAbstractValidator

@end
