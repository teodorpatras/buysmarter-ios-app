//
//  BSOrderProductCell.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSOrderProductCell.h"
#import "UIImageView+BSAsync.h"
#import "BSCoreDataAdapter.h"
#import "BSSessionManager.h"

@interface BSOrderProductCell()
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCurrencyLabel;
@end
@implementation BSOrderProductCell


- (void)configureWithProduct:(Product *)product andVendor:(Vendor *)vendor
{
    self.productTitleLabel.text = product.productTitle;
    self.productDescriptionLabel.text = product.productDescription;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    if([USER_INFO.preferredCurrency isEqualToString:vendor.vendorCurrency]){
        self.productPriceLabel.text = [NSString stringWithFormat:@"%@", product.productPrice];
        self.productCurrencyLabel.text = vendor.vendorCurrency;
    }else{
        self.productPriceLabel.text = [NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:[product.productPrice floatValue] withCurrency:vendor.vendorCurrency]];
        self.productCurrencyLabel.text = USER_INFO.preferredCurrency;
    }
}

@end
