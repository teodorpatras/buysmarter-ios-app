//
//  User.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "UserInfo.h"


@implementation UserInfo

@dynamic accessToken;
@dynamic addressLine;
@dynamic city;
@dynamic currencyRatesDictionary;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic ordersCount;
@dynamic ordersRequested;
@dynamic phoneNumber;
@dynamic postalCode;
@dynamic preferredCurrency;
@dynamic productsListsRequested;
@dynamic requestedHistoryCount;
@dynamic shippingRequested;
@dynamic preferredLanguage;

@end
