//
//  BSRegisterViewController.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSRegisterViewController.h"
#import "BSUIManager.h"
#import "BSTextField.h"
#import "BSConstants.h"
#import "BSValidationManager.h"
#import "BSEmailValidator.h"
#import "BSPasswordValidator.h"
#import "BSRoundedCornersImageView.h"


@interface BSRegisterViewController ()
{
    BOOL _configuredForSettingsScreen;
}
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIView *buttonsSectionView;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet BSTextField *emailOrCurrentPasswordTextField;
@property (weak, nonatomic) IBOutlet BSTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet BSRoundedCornersImageView *backgroundView;
@property (weak, nonatomic) IBOutlet BSTextField *repeatPasswordTextField;
@property (nonatomic) BSValidationManager *validationManager;
@end

@implementation BSRegisterViewController

#pragma mark - ViewController lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configureLocalization) name:BSLanguageChangedNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureView];
    [self configureLocalization];
}

- (void) configureLocalization
{
    [self.registerButton setTitle:LOCALIZED_STRING(@"register_button_title") forState:UIControlStateNormal];
    [self.loginButton setTitle:LOCALIZED_STRING(@"login_button_title") forState:UIControlStateNormal];

    if(_configuredForSettingsScreen){
        self.emailOrCurrentPasswordTextField.placeholder = LOCALIZED_STRING(@"current_password_field_placeholder");
        self.passwordTextField.placeholder = LOCALIZED_STRING(@"new_password_placeholder");
        self.repeatPasswordTextField.placeholder = LOCALIZED_STRING(@"repeat_new_password_placeholder");
    }else{
        self.emailOrCurrentPasswordTextField.placeholder = LOCALIZED_STRING(@"email_field_placeholder");
        self.passwordTextField.placeholder = LOCALIZED_STRING(@"password_field_placeholder");
        self.repeatPasswordTextField.placeholder = LOCALIZED_STRING(@"repeat_password_placeholder");
    }
}

#pragma mark - Configurations

- (void) configureView
{
    [BSUIManager customizeButton:self.registerButton withResizableImageNamed:@"GreenButton-Normal"];
    
    if(!_configuredForSettingsScreen){
        [_emailOrCurrentPasswordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_emailOrCurrentPasswordTextField setValidationIdentifier:kvBSEmailIdentifier];
    }else{
        [_emailOrCurrentPasswordTextField setValidationIdentifier:kvBSCurrentPasswordIdentifier];
    }
    [_passwordTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_repeatPasswordTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_emailOrCurrentPasswordTextField setReturnKeyType:UIReturnKeyNext];
    [_passwordTextField setReturnKeyType:UIReturnKeyNext];
    
    [_passwordTextField setValidationIdentifier:kvBSPasswordIdentifier];
    [_repeatPasswordTextField setValidationIdentifier:kvBSRepeatPasswordIdentifier];
    
    if(_configuredForSettingsScreen){
        _emailOrCurrentPasswordTextField.text = _passwordTextField.text = _repeatPasswordTextField.text = @"";
    }
}

- (void) configureForSettingsScreen
{
    [self.buttonsSectionView removeFromSuperview];
    NSArray *fieldsArray = @[_emailOrCurrentPasswordTextField, _passwordTextField, _repeatPasswordTextField];
    CGFloat padding = (CGRectGetHeight(self.view.frame) - fieldsArray.count * CGRectGetHeight([fieldsArray[0] frame])) / (fieldsArray.count + 1);
    CGRect frame;
    for(int i = 0; i < fieldsArray.count; i++){
        frame = [fieldsArray[i] frame];
        frame.origin.y = (i + 1) * padding + i * CGRectGetHeight(frame);
        [fieldsArray[i] setFrame:frame];
    }
    
    _emailOrCurrentPasswordTextField.secureTextEntry = YES;
    _configuredForSettingsScreen = YES;
}

- (void) resetFields
{
    _emailOrCurrentPasswordTextField.text = _repeatPasswordTextField.text = _passwordTextField.text = @"";
}

#pragma mark - accessor methods

- (BSValidationManager *)validationManager
{
    if(!_validationManager){
        _validationManager = [BSValidationManager new];
        
        if(!_configuredForSettingsScreen){
            [_validationManager registerClass:[BSEmailValidator class] withIdentifier:kvBSEmailIdentifier andErrorMessage:@"alert_invalid_email"];
            [_validationManager registerClass:[BSPasswordValidator class] withIdentifier:kvBSPasswordIdentifier andErrorMessage:@"alert_password_invalid"];
        }else{
            [_validationManager registerClass:[BSPasswordValidator class] withIdentifier:kvBSCurrentPasswordIdentifier andErrorMessage:@"alert_invalid_current_password"];
            [_validationManager registerClass:[BSPasswordValidator class] withIdentifier:kvBSPasswordIdentifier andErrorMessage:@"alert_new_password_invalid"];
        }
        [_validationManager registerClass:[BSPasswordValidator class] withIdentifier:kvBSRepeatPasswordIdentifier andErrorMessage:@"alert_repeat_password_invalid"];
    }
    return _validationManager;
}

#pragma mark - Callbacks

- (void) hideKeyboard
{
    [_emailOrCurrentPasswordTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_repeatPasswordTextField resignFirstResponder];
}

- (void) getNewPassword:(NSString **)newPass andOldPassword:(NSString **)oldPass
{
    NSString *error;
    [self hideKeyboard];
    [self.validationManager registerInput:_emailOrCurrentPasswordTextField.text withValidatorIdentifier:_emailOrCurrentPasswordTextField.validationIdentifier];
    [self.validationManager registerInput:_passwordTextField.text withValidatorIdentifier:_passwordTextField.validationIdentifier];
    [self.validationManager registerInput:_repeatPasswordTextField.text withValidatorIdentifier:_repeatPasswordTextField.validationIdentifier];
    [self.validationManager validateInputs:&error];
    if(error){
        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(error)];
    }else{
        if([_repeatPasswordTextField.text isEqualToString:_passwordTextField.text]){
            *newPass = _repeatPasswordTextField.text;
            *oldPass = _emailOrCurrentPasswordTextField.text;
        }else{
            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(@"alert_password_missmatch")];
        }
    }
}

- (IBAction)registerAction {
    
    NSString *error;
    [self.validationManager registerInput:_emailOrCurrentPasswordTextField.text withValidatorIdentifier:_emailOrCurrentPasswordTextField.validationIdentifier];
    [self.validationManager registerInput:_passwordTextField.text withValidatorIdentifier:_passwordTextField.validationIdentifier];
    [self.validationManager registerInput:_repeatPasswordTextField.text withValidatorIdentifier:_repeatPasswordTextField.validationIdentifier];
    [self.validationManager validateInputs:&error];
    if(error){
        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(error)];
    }else{
        [self hideKeyboard];
        if([_repeatPasswordTextField.text isEqualToString:_passwordTextField.text]){
            [self.delegate commenceRegistrationWithEmail:_emailOrCurrentPasswordTextField.text andPasword:_passwordTextField.text];
        }else{
             [BSUIManager showInformativeAlertViewWithTitle:@"Error" andText:LOCALIZED_STRING(@"alert_password_missmatch")];
        }
    }
}

- (IBAction)logInAction {
    [self hideKeyboard];
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                           forView:self.view.superview cache:YES];
    [self.view removeFromSuperview];
    
    [UIView commitAnimations];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_emailOrCurrentPasswordTextField]){
        [_passwordTextField becomeFirstResponder];
    }else if([textField isEqual:_passwordTextField]){
        [_repeatPasswordTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(BSTextField *)textField
{
    NSString *error;
    if(textField.text.length > 0){
        [self.validationManager validateInput:textField.text forIdentifier:textField.validationIdentifier error:&error];
        if(error){
            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_warning_title") andText:LOCALIZED_STRING(error)];
        }
    }
    return YES;
}

@end
