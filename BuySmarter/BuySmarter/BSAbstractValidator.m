//
//  BSAbstractValidator.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSAbstractValidator.h"

@implementation BSAbstractValidator

- (BOOL)execute
{
    // to be overriden in subclasses
    return NO;
}

@end
