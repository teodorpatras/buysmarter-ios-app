//
//  BSProductDescriptionView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface BSProductDescriptionView : UIView
/**
 *  Factory method
 *
 *  @param product      the product for the view to be configured with
 *
 *  @return             the instance of the product description view
 */
+ (BSProductDescriptionView *)descriptionViewForProduct:(Product *)product;

@end
