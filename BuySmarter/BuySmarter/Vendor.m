//
//  Vendor.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "Vendor.h"


@implementation Vendor

@dynamic vendorCurrency;
@dynamic vendorIdentifier;
@dynamic vendorName;
@dynamic vendorLogoURL;
@dynamic vendorApiKey;
@dynamic vendorEmail;

@end
