//
//  BSAddressValidator.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSAddressValidator.h"

@implementation BSAddressValidator

- (BOOL)execute
{
    if(!self.inputString){
        return NO;
    }
    // convert from ăâîșț to aaist
    NSData *asciiEncoded = [self.inputString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *newString = [[NSString alloc] initWithData:asciiEncoded encoding:NSASCIIStringEncoding];
    
    NSString *expression = @"^[0-9a-zA-Z. ]+$";
    NSError *error = NULL;
    BOOL result;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:newString options:0 range:NSMakeRange(0, [self.inputString length])];
    if (match){
        result = YES;
    }else{
        result = NO;
    }
    return result;
}
@end
