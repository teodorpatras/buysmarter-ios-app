//
//  BSProductTitleView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductTitleView.h"
#import "Vendor.h"
#import "BSCoreDataAdapter.h"
#import "BSImageLoader.h"
#import "UIImageView+BSAsync.h"
#import "BSConstants.h"

@interface BSProductTitleView()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@end
@implementation BSProductTitleView

+ (BSProductTitleView *)titleViewForProduct:(Product *)product
{
    BSProductTitleView* titleView = [[NSBundle mainBundle] loadNibNamed:@"BSProductTitleView" owner:self options:nil][0];
    titleView.titleLabel.text = product.productTitle;
    NSString *vendorLogoURL;
    for(Vendor *v in [[BSCoreDataAdapter sharedInstance] fetchedVendorsArray]){
        if([v.vendorIdentifier isEqualToString:product.vendorIdentifier]){
            vendorLogoURL = v.vendorLogoURL;
            break;
        }
    }
    UIImage *image = [[BSImageLoader sharedInstance] cachedImageFromURL:vendorLogoURL];
    if(image){
        [titleView.logoImageView setImage:image];
    }else{
        [titleView.logoImageView setImageFromURL:vendorLogoURL placeHolderImage:nil];
    }
    titleView.titleLabel.textColor = BSCOLOR;
    return titleView;
}

@end
