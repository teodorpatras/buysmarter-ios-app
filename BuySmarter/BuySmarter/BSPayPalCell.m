//
//  BSPayPalCell.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSPayPalCell.h"
#import "Product.h"
#import "BSConstants.h"

@interface BSPayPalCell()
@property (weak, nonatomic) IBOutlet UILabel *payWithLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *convertedAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end

@implementation BSPayPalCell

- (void) configureWithMainAmount:(CGFloat)mainAmount currency:(NSString *)mainCurrency secondaryAmount:(CGFloat)secondaryAmount currency:(NSString *)secondaryCurrency
{
    [self configureLocalization];
    self.amountLabel.text = [NSString stringWithFormat:@"%.2f %@ ", mainAmount, mainCurrency];
    self.convertedAmountLabel.text = [NSString stringWithFormat:@"(%.2f %@)", secondaryAmount, secondaryCurrency];
    self.backgroundColor = BSCOLOR;
}

- (void) configureLocalization
{
    self.payWithLabel.text = LOCALIZED_STRING(@"paypal_cell_pay_with_text");
    self.totalLabel.text = LOCALIZED_STRING(@"paypal_cell_total_text");
}

@end
