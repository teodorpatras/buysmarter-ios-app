//
//  BSProductHeaderView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSVendorHeaderView.h"
#import "UIImageView+BSAsync.h"
#import "BSConstants.h"

@interface BSVendorHeaderView()

@property (weak, nonatomic) IBOutlet UIImageView *vendorImageView;
@property (weak, nonatomic) IBOutlet UILabel *vendorLabel;

@end
@implementation BSVendorHeaderView

+ (BSVendorHeaderView *)headerViewForVendor:(Vendor *)vendor{
    BSVendorHeaderView* headerView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
    [headerView.vendorImageView setImageFromURL:vendor.vendorLogoURL placeHolderImage:[UIImage imageNamed:@"Placeholder"]];
    headerView.vendorLabel.text = vendor.vendorName;
    headerView.vendorLabel.textColor = BSCOLOR;
    return headerView;
}

@end
