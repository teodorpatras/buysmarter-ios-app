//
//  BSValidationManager.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSValidationManager.h"
#import "BSAbstractValidator.h"

static NSString * const BSValidatorKey = @"validator";
static NSString * const BSIdentifierKey = @"identifier";

@interface BSValidationManager()
@property (nonatomic) NSMutableArray *validatorsArray;
@property (nonatomic) NSString *classAssertionMessage;
@end

@implementation BSValidationManager

#pragma mark - Accessor methods

- (NSMutableArray *)validatorsArray
{
    if(!_validatorsArray){
        _validatorsArray = [@[] mutableCopy];
    }
    return _validatorsArray;
}
- (NSString *)classAssertionMessage
{
    if(!_classAssertionMessage){
        _classAssertionMessage = [NSString stringWithFormat:@"Provided class is not a subclass of %@.", NSStringFromClass([BSAbstractValidator class])];
    }
    return _classAssertionMessage;
}

#pragma mark - Public methods

- (void)registerClass:(Class)validatorClass withIdentifier:(NSString *)identifier andErrorMessage:(NSString *)errorMessage
{
    id validatorObject = [validatorClass new];
    NSAssert([validatorObject isKindOfClass:[BSAbstractValidator class]], self.classAssertionMessage);
    [validatorObject setErrorMessage:errorMessage];
    NSAssert([self validatorForIdetifier:identifier] == nil, @"Identifier already used. Cannot register two validators with the same identifier.");
    [self.validatorsArray addObject:@{BSValidatorKey : validatorObject, BSIdentifierKey : identifier}];
}

- (void) registerInput:(NSString *)input withValidatorIdentifier:(NSString *)identifier
{
    BSAbstractValidator *validator = [self validatorForIdetifier:identifier];
    NSAssert(validator != nil, @"Invalid validator identifier.");
    [validator setInputString:input];
}

- (void) validateInputs:(NSString **)errorMessage;
{
    for(int i = 0; i < self.validatorsArray.count; i ++){
        if(![self.validatorsArray[i][BSValidatorKey] execute]){
            *errorMessage = [self.validatorsArray[i][BSValidatorKey] errorMessage];
            break;
        }
    }
}

- (void) validateInput:(NSString *)input forIdentifier:(NSString *)identifier error:(NSString **)error
{
    BSAbstractValidator *validator = [self validatorForIdetifier:identifier];
    NSAssert(validator != nil, @"Invalid validation identifier!");
    [validator setInputString:input];
    BOOL valid = [validator execute];
    if(!valid){
        *error = [validator errorMessage];
    }
}
#pragma mark - Private methods

- (BSAbstractValidator *) validatorForIdetifier:(NSString *)identifier
{
    id result = nil;
    
    for(NSDictionary * dict in self.validatorsArray){
        if([dict[BSIdentifierKey] isEqualToString:identifier]){
            result = dict[BSValidatorKey];
            break;
        }
    }
    return result;
}


@end
