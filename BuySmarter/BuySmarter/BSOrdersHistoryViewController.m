//
//  BSOrderViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSOrdersHistoryViewController.h"
#import "BSNetworkingAdapter.h"
#import "BSCoreDataAdapter.h"
#import "BSIndicatorView.h"
#import "BSConnectionManager.h"
#import "BSUIManager.h"
#import "BSProductDetailsViewController.h"

static const CGFloat kBSFullScale = 1.f;
static const CGFloat kMinScale = .8f;
static const CGFloat sideOffset = 20;

static const NSInteger kReusableViewsCount = 3;

@interface BSOrdersHistoryViewController ()
{
    int _viewIndex;
    Product *_lastSelectedProduct;
    BOOL _requestViewShown;
}
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

@property (weak, nonatomic) IBOutlet UIView *requestView;
@property (weak, nonatomic) IBOutlet UIImageView *leftArrowView;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) NSArray *dataSource;
@property (nonatomic) NSArray *orderViews;
@end

@implementation BSOrdersHistoryViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureLocalization];
    if(!USER_INFO.ordersRequested.boolValue){
        if(INTERNET_REACHABLE){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
            [self requestPaginatedWithCompletionBlock:^(NSString *error, BOOL authError){
                [BSIndicatorView hide];
                if(error){
                    [BSUIManager showServerErrorAlert];
                }else if(authError){
                    [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                }else{
                    // 200 OK
                    _dataSource = [@[] mutableCopy];
                    [self configureDataSource];
                    [self configureOrderViews];
                    [self configureView];
                }
            }];
        }else{
            [BSUIManager showInternetNotReachableAlert];
        }
    }else{
        [self configureDataSource];
        [self configureOrderViews];
        [self configureView];
    }
}

- (void) configureLocalization
{
    self.title = LOCALIZED_STRING(@"orders_history_title");
    self.questionLabel.text = LOCALIZED_STRING(@"request_more_question");
    [self.noButton setTitle:LOCALIZED_STRING(@"no_answer") forState:UIControlStateNormal];
    [self.yesButton setTitle:LOCALIZED_STRING(@"yes_answer") forState:UIControlStateNormal];
}

- (void) requestPaginatedWithCompletionBlock:(void(^) (NSString *, BOOL))completionBlock{
    [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGETOrdersHistoryPaginated withAdditionalData:nil completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
        [BSIndicatorView hide];
        if(error){
            completionBlock(error, NO);
        }else{
            if(statusCode.integerValue == 200){
                [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDict forOperationType:BSNetworkOperationTypeGETOrdersHistoryPaginated];
                completionBlock(nil, NO);
            }else if(statusCode.integerValue == 405){
                // not allowed
                completionBlock(nil, YES);
            }else{
                completionBlock(@"Server error", NO);
            }
        }
    }];
    
}

- (void) configureDataSource
{
    NSArray *orders = [[BSCoreDataAdapter sharedInstance] fetchedOrders];
    NSArray *products = [[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:BSProductCategoryNone];
    
    NSDictionary *orderDictionary;
    NSMutableArray *orderProducts;
    NSMutableArray *array = [@[] mutableCopy];
    Vendor *v;
    
    for(Order *o in orders){
        orderProducts = [@[] mutableCopy];
        for(NSString *idString in o.products){
            for(Product *p in products){
                if([p.databaseId isEqualToString:idString]){
                    [orderProducts addObject:p];
                    break;
                }
            }
        }
        v = [[BSCoreDataAdapter sharedInstance] vendorForIdentifier:[orderProducts[0] vendorIdentifier]];
        orderDictionary = @{BSOrderKey : o, BSOrderProductsKey : orderProducts, BSOrderVendorKey : v};
        [array addObject:orderDictionary];
    }
    _dataSource = [array sortedArrayUsingComparator:^NSComparisonResult(id firstObject, id secondObject){
        NSDate *firstDate = [firstObject[BSOrderKey] date];
        NSDate *secondDate = [secondObject[BSOrderKey] date];
        return [secondDate compare:firstDate];
    }];
}
- (void) configureOrderViews
{
    if(self.dataSource.count > kReusableViewsCount){
        _orderViews = @[[BSOrderView orderViewWithDelegate:self], [BSOrderView orderViewWithDelegate:self], [BSOrderView orderViewWithDelegate:self]];
        
        for(int i = 0; i < _orderViews.count; i ++){
            [_orderViews[i] setTag:i];
            [_orderViews[i] setViewIndex:i];
            [_orderViews[i] configureWithDictionary:_dataSource[i]];
        }
    }else{
        NSMutableArray *array = [@[] mutableCopy];
        for(int i = 0; i < _dataSource.count; i ++){
            [array addObject:[BSOrderView orderViewWithDelegate:self]];
            [array[i] configureWithDictionary:_dataSource[i]];
        }
        _orderViews = array;
    }
}
- (void) configureView
{
    [_requestView setBackgroundColor:BSCOLOR];
    
    [self setLeftArrow:NO rigtArrow:self.dataSource.count > 1 hidden:NO];
    CGFloat height = CGRectGetHeight(self.scrollView.bounds) - BSTopBarsHeight;
    
    self.scrollView.contentSize = CGSizeMake(_dataSource.count * CGRectGetWidth(self.view.bounds), height);
    
    CGRect frame;
    
    for(int i = 0; i < _orderViews.count; i ++){
        frame = [_orderViews[i] frame];
        frame.origin.x = i * CGRectGetWidth(self.view.bounds) + sideOffset;
        frame.origin.y = sideOffset;
        [_orderViews[i] setFrame:frame];
        [self.scrollView addSubview:_orderViews[i]];
    }
}

- (void) setLeftArrow:(BOOL)leftArrow rigtArrow:(BOOL)rightArrow hidden:(BOOL) hidden{
    [UIView animateWithDuration:0.2 animations:^{
        if(leftArrow){
            [_leftArrowView setAlpha:hidden?0:1];
        }
        if(rightArrow){
            [_rightArrowView setAlpha:hidden?0:1];
        }
    }];
}

- (void) repositionViews
{
    
    if(_orderViews.count != 3){
        return;
    }
    
    CGFloat nextViewOrigin = (_viewIndex  + 1) * CGRectGetWidth(self.view.frame) + sideOffset;
    CGFloat previousViewOrigin = (_viewIndex - 1)* CGRectGetWidth(self.view.frame) + sideOffset;
    CGPoint nextViewCenter, previousViewCenter;
    
    nextViewCenter.y = previousViewCenter.y = self.view.center.y - BSTopBarsHeight/2;
    nextViewCenter.x = nextViewOrigin + CGRectGetWidth(self.view.frame) / 2 - sideOffset;
    previousViewCenter.x = previousViewOrigin + CGRectGetWidth(self.view.frame) / 2 - sideOffset;
    
    id previousView, nextView;
    
    
    if(_viewIndex % 3  == 0){
        if(_viewIndex < _dataSource.count - 1){
            // [1] on the right side
            nextView = _orderViews[1];
        }
        if(_viewIndex > 0){
            // [2] on the left side
            previousView = _orderViews[2];
        }
    }else if(_viewIndex % 3 == 1){
        // [0] on the left side
        previousView = _orderViews[0];
        
        if(_viewIndex < _dataSource.count - 1){
            // [2] on the right side
            nextView = _orderViews[2];
        }
    }else{
        // [1] on the left side
        previousView = _orderViews[1];
        if(_viewIndex < _dataSource.count - 1){
            //[0] on the right side
            nextView = _orderViews[0];

        }
    }
    if(nextView){
        if([nextView center].x != nextViewCenter.x){
            [nextView setViewIndex:_viewIndex + 1];
            [nextView setCenter:nextViewCenter];
            [nextView configureWithDictionary:_dataSource[_viewIndex + 1]];
        }
    }
    if(previousView){
        if([previousView center].x != previousViewCenter.x){
            [previousView setCenter:previousViewCenter];
            [previousView setViewIndex:_viewIndex - 1];
            [previousView configureWithDictionary:_dataSource[_viewIndex - 1]];
        }
    }
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self setLeftArrow:YES rigtArrow:YES hidden:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger index;
    CGFloat width, x, value, scale, offset = scrollView.contentOffset.x;
    _viewIndex = (scrollView.contentOffset.x / CGRectGetWidth(self.scrollView.frame));
    
    for (BSOrderView * view in self.orderViews) {
        index = (CGRectGetMinX(view.frame) - sideOffset) / CGRectGetMaxX(self.scrollView.frame);
        width = self.scrollView.frame.size.width;
        x = index * width;
        value = (offset - x)/width;
        scale = kBSFullScale - fabs(value);
        if (scale > kBSFullScale) scale = kBSFullScale;
        if (scale < kMinScale) scale = kMinScale;
        [view setTransform:CGAffineTransformMakeScale(scale, scale)];
    }
    if(offset > (_dataSource.count - 1) * CGRectGetWidth(self.view.frame)){
        [self shouldShowRequestView];
    }else if(offset < (_dataSource.count - 1) * CGRectGetWidth(self.view.frame) && _requestViewShown){
        [self setRequestViewHidden:YES];
    }
    [self repositionViews];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self setLeftArrow:(_viewIndex > 0) rigtArrow:(_viewIndex < _dataSource.count - 1) hidden:NO];
    for(BSOrderView *orderView in self.orderViews){
        [orderView shouldReloadInfo];
    }
}

#pragma mark - BSOrderViewDelegate method

- (void)orderViewDidSelectProduct:(Product *)product
{
    _lastSelectedProduct = product;
    [self performSegueWithIdentifier:@"showProductDetails" sender:self];
}

#pragma mark - Callbacks

- (IBAction)answerAction:(id)sender {
    [self setRequestViewHidden:YES];
    if(INTERNET_REACHABLE)
    {
        if([sender isEqual:self.yesButton]){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
            [self requestPaginatedWithCompletionBlock:^(NSString *error, BOOL authError){
                [BSIndicatorView hide];
                if(error){
                    [BSUIManager showServerErrorAlert];
                }else if(authError){
                    [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                }else{
                    // 200 OK
                    for(id subview in self.scrollView.subviews){
                        [subview removeFromSuperview];
                    }
                    _orderViews = @[];
                    _dataSource = [@[] mutableCopy];
                    [self configureDataSource];
                    [self configureOrderViews];
                    [self configureView];
                    _viewIndex --;
                    [self repositionViews];
                    _viewIndex ++;
                    [self repositionViews];
                }
            }];
        }
    }else{
        if([sender isEqual:self.yesButton]){
            [BSUIManager showInternetNotReachableAlert];
        }
    }
}

- (void) shouldShowRequestView
{
    if(!_requestViewShown){
        _requestViewShown = YES;
        if(USER_INFO.requestedHistoryCount.integerValue < USER_INFO.ordersCount.integerValue){
            if(INTERNET_REACHABLE){
                [self setRequestViewHidden:NO];
            }
        }
    }
}

- (void) setRequestViewHidden:(BOOL) hidden
{
    CGFloat height = CGRectGetHeight(self.requestView.frame);
    CGRect frame = self.requestView.frame;
    CGFloat mainViewHeight = CGRectGetHeight(self.view.frame);
    [UIView animateWithDuration:0.2 animations:^{
        [_requestView setFrame:CGRectMake(frame.origin.x, mainViewHeight + (hidden? 1 : -1) * height , frame.size.width, frame.size.height)];
    } completion:^(BOOL completed){
        if(completed && hidden){
            _requestViewShown = NO;
        }
        NSLog(@"%@", NSStringFromCGRect(_requestView.frame));
    }];
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showProductDetails"]){
        BSProductDetailsViewController *dvc = segue.destinationViewController;
        [dvc setProduct:_lastSelectedProduct];
        dvc.shouldConfigureForOrderProduct = YES;
    }
}

#pragma mark - Debug

- (UIColor *)randomColor
{
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    return color;
}
@end
