//
//  BSCheckoutViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSCheckoutViewController.h"
#import "BSIndicatorView.h"
#import "BSNetworkingAdapter.h"
#import "BSConnectionManager.h"
#import "BSUIManager.h"
#import "BSProductCell.h"
#import "BSPayPalCell.h"
#import "BSImageLoader.h"
#import "UIImageView+BSAsync.h"
#import "BSCheckoutHeaderView.h"
#import "BSCoreDataAdapter.h"
#import "UIViewController+BSAuth.h"
#import "BSSessionManager.h"

static NSString * kBSBookedProducts = @"booked-products";
static NSString * kBSUnbookedProducts = @"unbooked-products";

@interface BSCheckoutViewController ()
{
    NSDate *_reservationDate;
    NSTimer *_checkTimer;
    BOOL _productsReserved;
    PayPalConfiguration *_configuration;
}
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSDictionary *payloadData;
@property (nonatomic) NSDictionary *dataSourceDictionary;
@property (nonatomic) CGFloat totalPrice;
@property (nonatomic) PayPalPaymentViewController *paymentViewController;
@end

@implementation BSCheckoutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_tableView setAlpha:0];
    self.title = LOCALIZED_STRING(@"checkout_label_text");
    NSMutableArray *productIds = [@[] mutableCopy];
    for(Product *p in self.productsArray){
        [productIds addObject:p.productIdentifier];
    }
    [_headerImageView setImageFromURL:_vendor.vendorLogoURL placeHolderImage:nil];
    _payloadData = @{ kBSNVendorIdKey : _vendor.vendorIdentifier, kBSNProductsKey : productIds};
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifyReservationTime) name:BSWillEnterForegroundNotification object:nil];
    /*
     *  sand: AfqvQxBi5XpUJa5D-B_AXS0xdlwbJ_0yguCPZtZMRDGKZK5x6wveowVWGkFs
     *  live: ASlAKxA203Zu7hao9rWDXhawPT_ISDN4LMwfkfsSnaYm8spnopJkQsrtC2oK
     */
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox : _vendor.vendorApiKey}];
}

- (void) verifyReservationTime
{
    NSTimeInterval interval = [_reservationDate timeIntervalSinceNow];
    if(interval < -4.5 * 60){
        [_checkTimer invalidate];
        _checkTimer = nil;
        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_info_title") andText:LOCALIZED_STRING(@"alert_reservetion_expired")];
        if(_paymentViewController){
            [self dismissViewControllerAnimated:YES completion:^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }else{
        [self startCheckTimer];
    }
}

- (void)invalidateTimer
{
    [_checkTimer invalidate];
    _checkTimer = nil;
}

- (void) startCheckTimer
{
    _checkTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(verifyReservationTime) userInfo:nil repeats:NO];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _configuration = [[PayPalConfiguration alloc] init];
    _configuration.acceptCreditCards = NO;
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
    
    if(!_productsReserved){
        _productsReserved = YES;
        if(INTERNET_REACHABLE){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
            [self bookProductsWithCompletionBlock:^(NSDictionary *bookedProducts, NSString *error, BOOL authError){
                [BSIndicatorView hide];
                if(authError){
                    [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                }else if(error){
                    NSLog(@"[BOOK ERROR: %@]", error);
                    [BSUIManager showServerErrorAlert];
                }else{
                    // everything is ok
                    if([bookedProducts[kBSNBookedProductsKey] count] != self.productsArray.count){
                        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_warning_title") andText:LOCALIZED_STRING(@"alert_some_products_reserved")];
                        for(Product *p in self.productsArray){
                            if(![bookedProducts[kBSNBookedProductsKey] containsObject:p.productIdentifier]){
                                [[BSCoreDataAdapter sharedInstance] updateObject:p withValuesAndKeys:@{BSProductStockKey : @0}];
                            }
                            [self.delegate checkoutViewControllerDidAlterDataSource];
                        }
                    }
                    _reservationDate = [NSDate date];
                    [self startCheckTimer];
                    [self configureDataSourceWithDictionary:bookedProducts];
                    [self configureTotalPrice];
                    [UIView animateWithDuration:0.5 animations:^{
                        [_tableView setAlpha:1];
                    }];
                    [_tableView reloadData];
                }
            }];
        }else{
            [BSIndicatorView hide];
            [BSUIManager showInternetNotReachableAlert];
        }
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_checkTimer invalidate];
    _checkTimer = nil;
    if(!_paymentViewController){
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeCancelBooking withAdditionalData:@{kBSNVendorIdKey : _vendor.vendorIdentifier} completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *err){
        }];
    }
}

#pragma mark - Booking

- (void) bookProductsWithCompletionBlock:(void(^)(NSDictionary *, NSString *, BOOL))completionBlock
{
    if(INTERNET_REACHABLE){
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeBookProducts withAdditionalData:_payloadData completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
            if(!error){
                if(statusCode.integerValue == 200){
                    completionBlock(responseDict, nil, NO);
                }else{
                    if(statusCode.integerValue == 405){
                        completionBlock(nil, nil, YES);
                    }else{
                        completionBlock(nil, @"Server error", NO);
                    }
                }
            }else{
                completionBlock(nil, error, NO);
            }
            
        }];
    }else{
        completionBlock(nil, @"No internet connection!", NO);
    }
}

#pragma mark - Configurations

- (void) configureTotalPrice
{
    for(Product *p in self.dataSourceDictionary[kBSBookedProducts]){
        _totalPrice += [p.productPrice floatValue];
    }
}
- (void) configureDataSourceWithDictionary:(NSDictionary *)responseDict{
    for(int i = 0; i < self.productsArray.count; i ++){
        if([responseDict[kBSNBookedProductsKey] containsObject:[self.productsArray[i] productIdentifier]]){
            [self.dataSourceDictionary[kBSBookedProducts] addObject:self.productsArray[i]];
        }else{
            [self.dataSourceDictionary[kBSUnbookedProducts] addObject:self.productsArray[i]];
        }
    }
}

#pragma mark - UITableView methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return BSCheckoutHeaderViewHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([self.dataSourceDictionary[kBSUnbookedProducts] count] && [self.dataSourceDictionary[kBSBookedProducts] count]){
        if(section == 0){
            return [BSCheckoutHeaderView headerViewForSuccess:YES];
        }else{
            return [BSCheckoutHeaderView headerViewForSuccess:NO];
        }
    }else if([self.dataSourceDictionary[kBSBookedProducts] count]){
        return [BSCheckoutHeaderView headerViewForSuccess:YES];
    }else if([self.dataSourceDictionary[kBSUnbookedProducts] count]){
        return [BSCheckoutHeaderView headerViewForSuccess:NO];
    }else{
        NSAssert(nil, @"Fatal error!");
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.dataSourceDictionary[kBSUnbookedProducts] count] && [self.dataSourceDictionary[kBSBookedProducts] count]){
        if(indexPath.section == 0){
            if(indexPath.row < [self.dataSourceDictionary[kBSBookedProducts] count]){
                return kBSProductCellHeight;
            }else{
                return kBSPayPalCellHeight;
            }
        }else{
            return kBSProductCellHeight;
        }
    }else if([self.dataSourceDictionary[kBSBookedProducts] count]){
        if(indexPath.section == 0){
            if(indexPath.row < [self.dataSourceDictionary[kBSBookedProducts] count]){
                return kBSProductCellHeight;
            }else{
                return kBSPayPalCellHeight;
            }
        }else{
            return kBSProductCellHeight;
        }
    }else if([self.dataSourceDictionary[kBSUnbookedProducts] count]){
        return kBSProductCellHeight;
    }else{
        return kBSProductCellHeight;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([self.dataSourceDictionary[kBSUnbookedProducts] count] && [self.dataSourceDictionary[kBSBookedProducts] count]){
        return self.dataSourceDictionary.count;
    }else if([self.dataSourceDictionary[kBSBookedProducts] count]){
        return 1;
    }else if([self.dataSourceDictionary[kBSUnbookedProducts] count]){
        return 1;
    }else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.dataSourceDictionary[kBSUnbookedProducts] count] && [self.dataSourceDictionary[kBSBookedProducts] count]){
        return [self.dataSourceDictionary[section == 0? kBSBookedProducts : kBSUnbookedProducts] count] + (section == 0 ? 1 : 0);
    }else if([self.dataSourceDictionary[kBSBookedProducts] count]){
        return [self.dataSourceDictionary[kBSBookedProducts] count] + 1;
    }else if([self.dataSourceDictionary[kBSUnbookedProducts] count]){
       return [self.dataSourceDictionary[kBSUnbookedProducts] count];
    }else{
        return 0;
    }
}

- (BOOL) shouldDisplayProductCellsForIndexPath:(NSIndexPath *)indexPath
{
    BOOL shouldDisplayProductCell = NO;
    
    if([self.dataSourceDictionary[kBSUnbookedProducts] count] && [self.dataSourceDictionary[kBSBookedProducts] count]){
        if(indexPath.section == 0){
            shouldDisplayProductCell = [self.dataSourceDictionary[kBSBookedProducts] count] > indexPath.row;
        }else{
            shouldDisplayProductCell = YES;
        }
    }else if([self.dataSourceDictionary[kBSBookedProducts] count]){
        shouldDisplayProductCell = [self.dataSourceDictionary[kBSBookedProducts] count] > indexPath.row;
    }else if([self.dataSourceDictionary[kBSUnbookedProducts] count]){
        shouldDisplayProductCell = YES;
    }
    return shouldDisplayProductCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self shouldDisplayProductCellsForIndexPath:indexPath]){
        BSProductCell *cell = [tableView dequeueReusableCellWithIdentifier:BSProductCellIdentifier];
        
        Product *product;
        BOOL forceOutOfStock;
        
        if([self.dataSourceDictionary[kBSBookedProducts] count] && indexPath.section == 0 && indexPath.row < [self.dataSourceDictionary[kBSBookedProducts] count])
        {
            forceOutOfStock = NO;
            product = self.dataSourceDictionary[kBSBookedProducts][indexPath.row];
        }else{
            product = self.dataSourceDictionary[kBSUnbookedProducts][indexPath.row];
            forceOutOfStock = YES;
        }
        
        if(![[BSImageLoader sharedInstance] cachedImageFromURL:product.imageURLsArray[0]]){
            cell.productImageView.image = [UIImage imageNamed:@"Placeholder"];
            if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
            {
                [self startIconDownloadForIndexPath:indexPath];
            }
        }else{
            cell.productImageView.image = [[BSImageLoader sharedInstance] cachedImageFromURL:product.imageURLsArray[0]];
        }
        [cell configureWithProduct:product andVendor:self.vendor];
        if(forceOutOfStock){
            [cell forceOutOfStock];
        }
        return cell;
    }else{
        NSAssert([self.vendor.vendorCurrency isEqualToString:BSRONCurrency] || [self.vendor.vendorCurrency isEqualToString:BSEURCurrency], @"Unsupported currency!!!");
        BSPayPalCell *payPalCell = [tableView dequeueReusableCellWithIdentifier:BSPayPalCellIdentifier];

        CGFloat mainAmount = 0;
        CGFloat secondaryAmount = 0;
        NSString *secondaryCurrency;
        
        if([USER_INFO.preferredCurrency isEqualToString:_vendor.vendorCurrency]){
            mainAmount = _totalPrice;
            if([_vendor.vendorCurrency isEqualToString:BSRONCurrency]){
                secondaryCurrency = BSEURCurrency;
                secondaryAmount = [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:_totalPrice withCurrency:BSRONCurrency];
                
            }else{
                secondaryAmount = [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:_totalPrice withCurrency:BSEURCurrency];
                secondaryCurrency = BSRONCurrency;
            }
        }else{
            mainAmount = [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:_totalPrice withCurrency:_vendor.vendorCurrency];
            secondaryAmount = _totalPrice;
            secondaryCurrency = _vendor.vendorCurrency;
        }
        [payPalCell configureWithMainAmount:mainAmount currency:USER_INFO.preferredCurrency secondaryAmount:secondaryAmount currency:secondaryCurrency];
        return payPalCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(![self shouldDisplayProductCellsForIndexPath:indexPath]){
    
        // Create a PayPalPayment
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        

        // Amount, currency, and description
        if([_vendor.vendorCurrency isEqualToString:BSRONCurrency]){
            payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:_totalPrice withCurrency:self.vendor.vendorCurrency]]];
        }else{
            payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%.2f", _totalPrice]];
        }
        payment.currencyCode = BSEURCurrency;
        payment.shortDescription = [NSString stringWithFormat:LOCALIZED_STRING(@"products_from_vendor"), _vendor.vendorName];
        
        payment.intent = PayPalPaymentIntentSale;
        
        _paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                       configuration:_configuration
                                                                            delegate:self];
        // Present the PayPalPaymentViewController.
        [self presentViewController:_paymentViewController animated:YES completion:^{
            [[_paymentViewController parentViewController].view setTintColor:BSCOLOR];
        }];
    }
}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    _paymentViewController = nil;
    [self invalidateTimer];
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    _paymentViewController = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
    [_checkTimer invalidate];
    _checkTimer = nil;
    if(INTERNET_REACHABLE){
        NSDictionary *payloadDict = @{kBSNVendorIdKey : _vendor.vendorIdentifier, kBSNConfirmationKey : completedPayment.confirmation};
        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_veryfing_proof")];
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeProcessOrder withAdditionalData:payloadDict completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
            [self invalidateTimer];
            [BSIndicatorView hide];
            if(!error){
                if(statusCode.integerValue == 200){
                    [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_order_successfully_placed")];
                    [self.delegate checkoutViewControllerDidFinishWithDictionary:@{kBSCheckoutErrorKey : @(NO), kBSCheckoutReservedProductsKey : self.dataSourceDictionary[kBSBookedProducts], kBSCheckoutConfirmationKey : completedPayment.confirmation}];
                }else if(statusCode.integerValue == 405){
                    [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                }else{
                    //500 server error...
                    [BSUIManager showServerErrorAlert];
                    [self.delegate checkoutViewControllerDidFinishWithDictionary:@{kBSCheckoutErrorKey : @(YES), kBSCheckoutConfirmationKey : completedPayment.confirmation, kBSCheckoutReservedProductsKey : self.dataSourceDictionary[kBSBookedProducts]}];
                }
            }else{
                NSLog(@"%@", error);
                [BSIndicatorView hide];
                [BSUIManager showServerErrorAlert];
                [self.delegate checkoutViewControllerDidFinishWithDictionary:@{kBSCheckoutErrorKey : @(YES), kBSCheckoutConfirmationKey : completedPayment.confirmation, kBSCheckoutReservedProductsKey : self.dataSourceDictionary[kBSBookedProducts]}];
            }
        }];
    }else{
        [self invalidateTimer];
        [BSUIManager showInternetNotReachableAlert];
        [self.delegate checkoutViewControllerDidFinishWithDictionary:@{kBSCheckoutErrorKey : @(YES), kBSCheckoutConfirmationKey : completedPayment.confirmation, kBSCheckoutReservedProductsKey : self.dataSourceDictionary[kBSBookedProducts]}];
    }
}


#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

#pragma mark - Helper Methods

- (void)loadImagesForOnscreenRows
{
    if ([(NSArray *)self.dataSourceDictionary[kBSNProductsKey] count] > 0)
    {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if(indexPath.row < [self.dataSourceDictionary[kBSNProductsKey][indexPath.section] count]){
                if (![[BSImageLoader sharedInstance] cachedImageFromURL:[self.dataSourceDictionary[kBSNProductsKey][indexPath.section][indexPath.row] imageURLsArray][0]]){
                    [self startIconDownloadForIndexPath:indexPath];
                }
            }
        }
    }
}

- (void) startIconDownloadForIndexPath:(NSIndexPath *)indexPath
{
    Product *product = self.dataSourceDictionary[kBSNProductsKey][indexPath.section][indexPath.row];
    if([(NSArray *)product.imageURLsArray count] > 0){
        [[BSImageLoader sharedInstance] getImageWithURL:product.imageURLsArray[0] withCompletionBlock:^(UIImage *image, NSString *error){
            if(!error){
                BSProductCell *productCell = (BSProductCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                [productCell.productImageView setImage:image animated:YES];
            }else{
//                NSLog(@"ERROR: %@", error);
            }
        }];
    }
}


#pragma mark - Callbacks

- (NSDictionary *)dataSourceDictionary
{
    if(!_dataSourceDictionary){
        _dataSourceDictionary = @{kBSBookedProducts : [@[] mutableCopy], kBSUnbookedProducts : [@[] mutableCopy]};
    }
    return _dataSourceDictionary;
}

@end
