//
//  BSProductStockPriceView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductStockPriceView.h"
#import "BSCoreDataAdapter.h"
#import "BSSessionManager.h"
#import "Vendor.h"

@interface BSProductStockPriceView()
@property (weak, nonatomic) IBOutlet UILabel *stockTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceCurrencyLabel;
@property (weak, nonatomic) IBOutlet UIView *priceContainerView;

@end
@implementation BSProductStockPriceView

+ (BSProductStockPriceView *)stockPriceViewForProduct:(Product *)product
{
    BSProductStockPriceView* stockPriceView = [[NSBundle mainBundle] loadNibNamed:@"BSProductStockPriceView" owner:self options:nil][0];
    stockPriceView.stockValueLabel.text = [NSString stringWithFormat:@"%@", product.productStock];
    stockPriceView.priceValueLabel.text = [NSString stringWithFormat:@"%@", product.productPrice];
    
    [stockPriceView configureLocalization];
    
    Vendor *vendor = [[BSCoreDataAdapter sharedInstance] vendorForIdentifier:product.vendorIdentifier];
    
    if([USER_INFO.preferredCurrency isEqualToString:vendor.vendorCurrency]){
        stockPriceView.priceValueLabel.text = [NSString stringWithFormat:@"%@", product.productPrice];
        stockPriceView.priceCurrencyLabel.text = vendor.vendorCurrency;
    }else{
        stockPriceView.priceValueLabel.text = [NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:[product.productPrice floatValue] withCurrency:vendor.vendorCurrency]];
        stockPriceView.priceCurrencyLabel.text = USER_INFO.preferredCurrency;
    }
    return stockPriceView;
}

- (void) configureLocalization
{
    self.stockTextLabel.text = LOCALIZED_STRING(@"product_details_stock_text");
    self.priceTextLabel.text = LOCALIZED_STRING(@"product_details_price_text");
}

- (void) configureForOrderedProduct{
    [self.stockTextLabel setHidden:YES];
    [self.stockValueLabel setHidden:YES];
    [_priceContainerView setCenter:self.center];
}

- (void) updateStock:(NSInteger)newStock
{
    self.stockValueLabel.text = [NSString stringWithFormat:@"%ld", (long)newStock];
}

@end
