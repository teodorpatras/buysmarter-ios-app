//
//  BSDebugManager.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSConstants.h"

@interface BSDebugManager : NSObject
/**
 *  Prints information in a useful way for debugging purposes
 *
 *  @param postData             the payload for the post request
 *  @param statusCode           the status code of the response
 *  @param dict                 the response dictionary
 *  @param error                error string
 *  @param operationType        the type of the networking operation
 */
+ (void)printPostData:(NSDictionary *)postData statusCode:(NSNumber *)statusCode responseDictionary:(NSDictionary *)dict andError:(NSString *)error forOperationType:(BSNetworkOperationType)operationType;

@end
