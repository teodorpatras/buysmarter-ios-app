//
//  BSRoundedCornersImageView.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSRoundedCornersImageView.h"
static const float BSCornerRadius = 15.0;

@implementation BSRoundedCornersImageView

#pragma mark - Initializer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
        [self setup];
    
    return self;
}

#pragma mark - Nib loading

- (void)awakeFromNib
{
    [self setup];
}

#pragma mark - Customization

- (void)setup
{
    [self.layer setCornerRadius:BSCornerRadius];
    self.layer.masksToBounds = YES;
}
@end
