//
//  BSOrderViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/23/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSBaseViewController.h"
#import "BSOrderView.h"

@interface BSOrdersHistoryViewController : BSBaseViewController<UIScrollViewDelegate, BSOrderViewDelegate>


@end
