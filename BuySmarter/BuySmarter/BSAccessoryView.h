//
//  BSAccessoryView.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSAccessoryView : UIView
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UILabel *label;

/**
 *  Factory method
 *
 *  @param text     the text to be displayed
 *
 *  @return         instance of the accessory view
 */
+ (BSAccessoryView *)accessoryViewWithText:(NSString *)text;

@end
