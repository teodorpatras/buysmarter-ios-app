//
//  Order.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/11/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "Order.h"


@implementation Order

@dynamic products;
@dynamic date;

@end
