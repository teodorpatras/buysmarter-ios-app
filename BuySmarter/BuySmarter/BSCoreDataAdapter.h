//
//  BSCoreDataAdapter.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/19/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSCoreDataManager.h"
#import "BSConstants.h"
#import "Vendor.h"
#import "UserInfo.h"

typedef NS_ENUM(NSUInteger, BSEntityType){
    BSEntityTypeProduct,
    BSEntityTypeVendor,
    BSEntityTypeUser,
    BSEntityTypeOrder
};

@interface BSCoreDataAdapter : BSCoreDataManager

/**
 *  Method for accesssing the singleton instance
 *
 *  @return                 singleton instance
 */
+ (BSCoreDataAdapter *)sharedInstance;
/**
 *  Parses the server response
 *
 *  @param response         the server response
 *  @param operationType    the type of the operation
 */
- (void)parseServerResponse:(NSDictionary *)response forOperationType:(BSNetworkOperationType)operationType;
/**
 *  Inserts the order along with it's products and date
 *
 *  @param productsArray    the products within the order
 *  @param creationString   the creation date as string
 */
- (void) insertOrderWithProducts:(NSArray *)productsArray andCreationDateString:(NSString *)creationString;
/**
 *  Stores the user email and access token
 *
 *  @param email            the user email
 *  @param serverResponse   the server response
 */
- (void) storeUserEmail:(NSString *)email andAccessTokenFromResponse:(NSDictionary *)serverResponse;
/**
 *  Fetches the products for the specified category
 *
 *  @param                  category the category of the products
 *
 *  @return                 an array with products
 */
- (NSArray *)fetchProductsForCategory:(BSProductCategory)category;
/**
 *  Fetches a vendor for the specified identifier
 *
 *  @param identifier       the vendor identifier
 *
 *  @return                 the vendor object
 */
- (Vendor *) vendorForIdentifier:(NSString *)identifier;
/**
 *  Stores the shipping details
 *
 *  @param                  shipping shipping details
 */
- (void) storeUserShippingDetails:(NSDictionary *)shipping;
/**
 *  Validates a product dictionary
 *
 *  @param dict             product dictionary
 *
 *  @return                 whether or not the dictionary is valid
 */
- (BOOL) isProductDictionaryValid:(NSDictionary *)dict;
/**
 *  Gets vendors array
 *
 *  @return the vendors array
 */
- (NSArray *)fetchedVendorsArray;
/**
 *  Gets the fetched orders array
 *
 *  @return                 the fetched orders array
 */
- (NSArray *)fetchedOrders;
/**
 *  Gets the user info
 *
 *  @return                 user info object
 */
- (UserInfo *) storedUserInfo;
/**
 *  Empties the local core data storage
 */
- (void) deleteAll;
@end
