//
//  BSCheckoutHeaderView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSCheckoutHeaderView.h"
#import "BSConstants.h"

@interface BSCheckoutHeaderView()
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end

@implementation BSCheckoutHeaderView

+ (BSCheckoutHeaderView *) headerViewForSuccess:(BOOL)success
{
    BSCheckoutHeaderView *headerView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
    if(success){
        headerView.textLabel.text = LOCALIZED_STRING(@"checkout_screen_success_header_text");
        headerView.backgroundColor = BSCOLOR;
    }else{
        headerView.textLabel.text = LOCALIZED_STRING(@"checkout_screen_fail_header_text");
        headerView.backgroundColor = [UIColor redColor];
    }
    return headerView;
}

@end
