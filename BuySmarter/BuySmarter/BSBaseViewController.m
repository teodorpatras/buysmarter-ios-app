//
//  BSBaseViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSBaseViewController.h"
#import "UIViewController+BSAuth.h"
#import "BSUIManager.h"

static const int kBSDefaultTag = 99;

@implementation BSBaseViewController

- (void)logOutUserWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_warning_title") message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    alertView.tag = kBSDefaultTag;
    [alertView show];
}

#pragma mark - UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0 && alertView.tag == kBSDefaultTag){
        // Ok
        [self logOut];
    }
}

@end
