//
//  BSSlidingmenuViewController.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/12/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSSlidingMenuViewController : UIViewController <UIGestureRecognizerDelegate>

- (void)setMenuViewController:(UIViewController *)controller;
- (void)setFrontViewController:(UINavigationController *)controller;
- (void) hideMenuWithCompletionBlock:(void(^)(void))block;

@end
/*
 * Segue for integrating this class with storyboards
 */
@interface BSSlidingMenuSegue : UIStoryboardSegue

@end
/*
 * Category for accessing the BSSlidingMenuViewController fron any view controller
 *
 */
@interface UIViewController (BSSlidingMenu)
/*!
 Category method for getting a hold of the sliding menu view controller
 \return The instance of the sliding menu
 */
- (BSSlidingMenuViewController*) slidingMenuViewController;
@end

