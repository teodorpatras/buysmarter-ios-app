//
//  BSProductStockPriceView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface BSProductStockPriceView : UIView
/**
 *  Factory method
 *
 *  @param product  the product for the view to be configured with
 *
 *  @return         the instance of the stock price view
 */
+ (BSProductStockPriceView *)stockPriceViewForProduct:(Product *)product;
/**
 *  Updates the stock for the view
 *
 *  @param newStock  the new stock value
 */
- (void) updateStock:(NSInteger)newStock;
/**
 *  Reconfigures the view
 */
- (void) configureForOrderedProduct;

@end
