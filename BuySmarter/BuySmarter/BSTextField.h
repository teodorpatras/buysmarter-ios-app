//
//  BSTextField.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSValidationManager.h"

@interface BSTextField : UITextField
@property (nonatomic) NSString *validationIdentifier;
@end
