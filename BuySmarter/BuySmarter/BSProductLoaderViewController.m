//
//  BSProductLoaderViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductLoaderViewController.h"
#import "BSCoreDataAdapter.h"
#import "BSNetworkingAdapter.h"
#import "BSPhotoGalleryView.h"
#import "BSConstants.h"
#import "BSUIManager.h"
#import "Product.h"
#import "BSSessionManager.h"

typedef NS_ENUM(NSUInteger, BSActionSheetButton) {
    BSActionSheetButtonShoppingCart,
    BSActionSheetButtonWishList,
    BSActionSheetButtonCancel
};

static const float BSInitialScale = 0.0f;
static const float BSOvershootScale = 1.1f;
static const float BSUnderShootScale = 0.9f;

static const float BSInitialTime = 0.0f;
static const float BSOvershootTime = 0.4f;
static const float BSCloneOvershootTime = 0.45f;
static const float BSUndershootTime = 0.75f;
static const float BSFinalTime = 1.0f;

static const float BSAnimationDuration = 0.5f;


@interface BSProductLoaderViewController ()
{
    BSPhotoGalleryView *galleryView;
    NSDictionary *productDictionary;
}
@property (weak, nonatomic) IBOutlet UILabel *outOfStockLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UIView *animatorView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *galleryContainer;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *availabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@end

@implementation BSProductLoaderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    [self configureLocalization];
}

#pragma mark - configurations


- (void) configureLocalization
{
    self.outOfStockLabel.text = LOCALIZED_STRING(@"out_of_stock");
    self.availabilityLabel.text = LOCALIZED_STRING(@"product_details_stock_text");
    [_saveButton setTitle:LOCALIZED_STRING(@"save_button_title") forState:UIControlStateNormal];
}

- (void) configureView
{
    [self.animatorView setHidden:YES];
    [self.indicatorView setColor:BSCOLOR];
    [BSUIManager configureViewWithBorderShadowAndRoundedCorners:self.containerView];
}

#pragma mark - callbacks

- (IBAction)closeAction:(id)sender {
    [self.delegate productLoaderDidFinishWithDictionary:nil authenticationError:NO];
    [[BSNetworkingAdapter sharedInstance] cancelOperation:BSNetworkOperationTypeGETProductDetails];
}
- (IBAction)saveAction {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:LOCALIZED_STRING(@"alert_select_category")
                                  delegate:self
                                  cancelButtonTitle:LOCALIZED_STRING(@"cancel_button_title")
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:LOCALIZED_STRING(@"shopping_cart_segment_title"), LOCALIZED_STRING(@"wish_list_segment_title"), nil];
    [actionSheet showInView:self.view];
}
#pragma mark - UIActionSheetDelegate methods

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [actionSheet.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            button.titleLabel.textColor = BSCOLOR;
        }else if([subview isKindOfClass:[UILabel class]]){
            [(UILabel *)subview setTextColor:BSCOLOR];
        }
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != BSActionSheetButtonCancel){
        NSMutableDictionary *dict = [@{kBSNVendorIdKey : productDictionary[kBSNVendorIdKey], kBSNProductIdentifier : productDictionary[kBSNProductIdentifier]} mutableCopy];;
        if(buttonIndex == BSActionSheetButtonShoppingCart){
            [dict setValue:kBSNProductCategoryShoppingCart forKeyPath:kBSNProductCategory];
        }else{
            [dict setValue:kBSNProductCategoryWishList forKeyPath:kBSNProductCategory];
        }
        [self.delegate productLoaderDidFinishWithDictionary:dict authenticationError:NO];
    }
}

#pragma mark - instance methods

- (void) populateViewWithDictionary:(NSDictionary *)dict{

    NSAssert(dict != nil, @"Fatal error. Product dictionary cannot be nil at this stage!");
    [self.indicatorView stopAnimating];
    if([[BSCoreDataAdapter sharedInstance] isProductDictionaryValid:dict]){
        galleryView = [BSPhotoGalleryView photoGalleryViewForFrame:(CGRect){.size = {CGRectGetWidth(self.galleryContainer.frame), CGRectGetHeight(self.galleryContainer.frame)}} withDataSourceArray:dict[kBSNProductImageURLs]];
        [self.galleryContainer addSubview:galleryView];
        _titleLabel.text = dict[kBSNProductTitle];
        _descriptionLabel.text = dict[kBSNProductDescription];
        
        Vendor *vendor = [[BSCoreDataAdapter sharedInstance] vendorForIdentifier:dict[kBSNVendorIdKey]];
        CGFloat productPrice = [dict[kBSNProductPrice] floatValue];
        
        if([USER_INFO.preferredCurrency isEqualToString:vendor.vendorCurrency]){
            _priceAmountLabel.text = [NSString stringWithFormat:@"%.2f", productPrice];
            _currencyLabel.text = vendor.vendorCurrency;
        }else{
            _priceAmountLabel.text = [NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:productPrice withCurrency:vendor.vendorCurrency]];
            _currencyLabel.text = USER_INFO.preferredCurrency;
        }
        
        _saveButton.enabled = [dict[kBSNProductStock] integerValue] > 0;
        if([dict[kBSNProductStock] integerValue] == 0){
            [_outOfStockLabel setHidden:NO];
            [_availabilityLabel setHidden:YES];
            [_stockLabel setHidden:YES];
        }else{
            _stockLabel.text = [NSString stringWithFormat:@"%@",dict[kBSNProductStock]];
        }
        [UIView animateWithDuration:0.3 animations:^{
            [self.contentView setAlpha:1];
        }];
    }
}

- (void)requestForProductWithDictionary:(NSDictionary *)dict
{
    [self showProductPopup];
    [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGETProductDetails withAdditionalData:dict completionBlock:^(NSNumber * statusCode, NSDictionary *responseDict, NSString *errorMessage){
        if(statusCode.integerValue == 200){
            // 200 OK
            productDictionary = dict;
            [self populateViewWithDictionary:responseDict];
        }else{
            if(statusCode.integerValue == 405){
                [self.delegate productLoaderDidFinishWithDictionary:nil authenticationError:YES];
            }else{
                [BSUIManager showInvalidProductAlert];
                [self.delegate productLoaderDidFinishWithDictionary:nil authenticationError:NO];
            }
        }
    }];
}

- (void)showProductPopup
{
    [self.animatorView setHidden:NO];
    [self.containerView.layer setCornerRadius:5];
    [self.containerView setAlpha:1];
    [self.closeButton setAlpha:1];
    [self.animatorView.layer addAnimation:[self bounceAnimation] forKey:@"Bounce"];
}

- (CAKeyframeAnimation *) bounceAnimation
{
    
    CALayer *layer = self.containerView.layer;
    
    CAKeyframeAnimation *boundsOvershootAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    
    CATransform3D startingScale = CATransform3DScale (layer.transform, BSInitialScale, BSInitialScale, 0);
    CATransform3D overshootScale = CATransform3DScale (layer.transform, BSOvershootScale, BSOvershootScale, 1.0);
    CATransform3D overshootScaleClone = CATransform3DScale (layer.transform, BSOvershootScale, BSOvershootScale, 1.0);
    CATransform3D undershootScale = CATransform3DScale (layer.transform, BSUnderShootScale, BSUnderShootScale, 1.0);
    CATransform3D endingScale = layer.transform;
    
    NSArray *boundsValues = @[[NSValue valueWithCATransform3D:startingScale],
                             [NSValue valueWithCATransform3D:overshootScale],
                             [NSValue valueWithCATransform3D:overshootScaleClone],
                             [NSValue valueWithCATransform3D:undershootScale],
                             [NSValue valueWithCATransform3D:endingScale]];
    [boundsOvershootAnimation setValues:boundsValues];
    
    NSArray *times = @[@(BSInitialTime), @(BSOvershootTime), @(BSCloneOvershootTime), @(BSUndershootTime), @(BSFinalTime)];
    [boundsOvershootAnimation setKeyTimes:times];
    
    
    NSArray *timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [boundsOvershootAnimation setTimingFunctions:timingFunctions];
    boundsOvershootAnimation.fillMode = kCAFillModeForwards;
    boundsOvershootAnimation.removedOnCompletion = NO;
    boundsOvershootAnimation.duration = BSAnimationDuration;
	return boundsOvershootAnimation;
}

@end
