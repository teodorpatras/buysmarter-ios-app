//
//  BSNotEmptyValidator.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSNotEmptyValidator.h"

@implementation BSNotEmptyValidator

- (BOOL)execute
{
    if(self.inputString.length > 0)
    {
        return YES;
    }else{
        return NO;
    }
}


@end
