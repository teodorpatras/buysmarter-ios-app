//
//  BSSessionManager.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSSessionManager.h"
#import "BSNetworkingAdapter.h"
#import "BSCoreDataAdapter.h"
#import "BSImageLoader.h"

@interface BSSessionManager()
@end

@implementation BSSessionManager

#pragma mark - Singleton

+ (BSSessionManager *)sharedInstance
{
    static BSSessionManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [BSSessionManager new];
    });
    return manager;
}

#pragma mark - Facade

- (BSSupportedCurrency)preferredUserCurrencyAsEnum
{
    NSString *currency = USER_INFO.preferredCurrency;
    if([currency isEqualToString:BSRONCurrency]){
        return BSSupportedCurrencyRON;
    }else{
        return BSSupportedCurrencyEUR;
    }
}

- (void) preferredCurrencyChanged:(BSSupportedCurrency)currency
{
    [[BSCoreDataAdapter sharedInstance] updateObject:USER_INFO withValuesAndKeys:@{BSPreferredCurrencyKey : currency == BSSupportedCurrencyRON ? BSRONCurrency : BSEURCurrency}];
    NSLog(@"NEW pref currency is : %@", USER_INFO.preferredCurrency);
}

- (void)logOut
{
    [[BSCoreDataAdapter sharedInstance] deleteAll];
    [[BSNetworkingAdapter sharedInstance] cancelAllConnections];
    [[BSImageLoader sharedInstance] cancelAllConnections];
    [[BSImageLoader sharedInstance] emptyCache];
    [BSLocalizationManager deallocate];
    [self postLanguageChangedNotification];
}

- (void) postHideKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BSHideKeyboardNotification object:nil userInfo:nil];
}

- (void) postWillEnterForeground{
    [[NSNotificationCenter defaultCenter] postNotificationName:BSWillEnterForegroundNotification object:nil userInfo:nil];
}

- (void) postLanguageChangedNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:BSLanguageChangedNotification object:nil userInfo:nil];
}

- (BOOL)shouldDownloadShippingDetails
{
    return USER_INFO.shippingRequested.boolValue;
}

- (CGFloat) convertedAmountFromTotalAmount:(CGFloat) amount withCurrency:(NSString *)currency{
    
    /**
     *  @{BSCurrencyRatesKey : currencies, BSCurrencyExpirationKey : [[NSDate date] midnight]}
     */
    
    NSAssert([currency isEqualToString:BSRONCurrency] || [currency isEqualToString:BSEURCurrency], @"Invalid currency!");
    CGFloat rate = [USER_INFO.currencyRatesDictionary[BSCurrencyRatesKey][BSEURCurrency] floatValue];
    if([currency isEqualToString:BSEURCurrency]){
        /**
         *  EUR TO RON
         */
        return amount * rate;
    }else {
        /**
         *  RON TO EUR
         */
        return amount / rate;
    }
}
@end
