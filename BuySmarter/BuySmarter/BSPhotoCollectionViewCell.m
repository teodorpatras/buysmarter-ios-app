//
//  BSPhotoCollectionViewCell.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/3/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSPhotoCollectionViewCell.h"
#import "UIImageView+BSAsync.h"
#import "BSConstants.h"

@interface BSPhotoCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@end

@implementation BSPhotoCollectionViewCell

#pragma mark - Initializer

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"BSPhotoCollectionViewCell" owner:self options:nil][0];
        _indicatorView.color = BSCOLOR;
    }
    return self;
}

#pragma mark - Public methods

- (void)setImage:(UIImage *)image animated:(BOOL)animated
{
    [self.imageView setImage:image animated:animated];
    if([_indicatorView isAnimating]){
        [_indicatorView stopAnimating];
    }
}

- (void)clearImageView
{
    [self.imageView setImage:nil];
    [_indicatorView startAnimating];
}

@end
