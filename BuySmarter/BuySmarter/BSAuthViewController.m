//
//  BSAuthViewController.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSSessionManager.h"
#import "BSAuthViewController.h"
#import "BSLoginViewController.h"
#import "BSUIManager.h"
#import "BSConnectionManager.h"
#import "BSValidationManager.h"
#import "BSEmailValidator.h"
#import "BSNetworkingAdapter.h"
#import "BSIndicatorView.h"
#import "BSShippingDetailsViewController.h"
#import "BSUIManager.h"
#import "BSCoreDataAdapter.h"
#import "BSSlidingMenuViewController.h"

static NSString * const BSEmailValidatorIdentifier = @"email";
static const float kBSKeyboardHeight = 80.0;
static const float kBSTransformPercentage = 0.8;

@interface BSAuthViewController ()
{
    NSString *forgotPasswordEmailInput;
    UITextField *alertTextField;
    CGRect logoInitialFrame;
    CGRect logoMinimisedFrame;
}
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic) BSLoginViewController *loginViewController;
@property (nonatomic) BSShippingDetailsViewController *shippingViewController;
@property (nonatomic) BSEmailValidator *emailValidator;
@end

@implementation BSAuthViewController

#pragma mark - viewcontroller lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureLogoFrames];
    [self configureView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureLocalization];
}

- (void) configureView
{
    [self.navigationController setNavigationBarHidden:YES];
    [BSUIManager customizeButton:self.saveButton withResizableImageNamed:@"GreenButton-Normal"];
}

- (void) configureLocalization
{
    [_forgotPasswordButton setTitle:LOCALIZED_STRING(@"forgot_password_button_title") forState:UIControlStateNormal];
    _infoLabel.text = LOCALIZED_STRING(@"auth_info_label_text");
}

- (void) configureLogoFrames
{
    logoInitialFrame = self.logoView.frame;
    
    logoMinimisedFrame.size.width = kBSTransformPercentage * logoInitialFrame.size.width;
    logoMinimisedFrame.size.height = kBSTransformPercentage * logoInitialFrame.size.height;
    logoMinimisedFrame.origin.x = (CGRectGetWidth(self.view.bounds) - logoMinimisedFrame.size.width)/2;
    logoMinimisedFrame.origin.y = 15;
}

- (void) resetToInitialState
{
    [self.forgotPasswordButton setAlpha:1];
    [self.infoView setAlpha:0];
    [self.saveButton setAlpha:0];
    [self.skipButton setAlpha:0];
    
    [self.forgotPasswordButton setEnabled:YES];
    [self.saveButton setEnabled:NO];
    [self.skipButton setEnabled:NO];
    
    for(UIView * v in self.containerView.subviews){
        [v removeFromSuperview];
    }
    [self performSegueWithIdentifier:@"embeddLogin" sender:self];
}
#pragma mark - callbacks
- (IBAction)hideKeyboard:(id)sender {
    [[BSSessionManager sharedInstance] postHideKeyboardNotification];
}

- (IBAction)forgotPasswordAction {
    
    if(INTERNET_REACHABLE){
        _emailValidator = [[BSEmailValidator alloc] init];
        forgotPasswordEmailInput = self.loginViewController.email;
        [_emailValidator setInputString:forgotPasswordEmailInput];
        
        if(![_emailValidator execute]){
            forgotPasswordEmailInput = nil;
            [_emailValidator setInputString:forgotPasswordEmailInput];
        }
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:LOCALIZED_STRING(@"alert_email")
                                  message:LOCALIZED_STRING(@"alert_please_enter_email")
                                  delegate:self
                                  cancelButtonTitle:LOCALIZED_STRING(@"cancel_button_title")
                                  otherButtonTitles:@"Ok", nil];
        [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        alertTextField = [alertView textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeEmailAddress;
        if(forgotPasswordEmailInput){
            alertTextField.text = forgotPasswordEmailInput;
        }
        [alertView show];
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }

}

- (IBAction)skipAction {
    [[BSCoreDataAdapter sharedInstance] updateObject:USER_INFO withValuesAndKeys:@{BSShippingRequestedKey: @(YES),
                                                                                  BSOrdersRequestedKey : @(YES),
                                                                                  BSProductsListsRequested : @(YES)}];
    [self performSegueWithIdentifier:@"showHomeScreen" sender:self];
}

- (IBAction)saveAction {
    if(INTERNET_REACHABLE){
        NSDictionary *dict = [self.shippingViewController getValidInputs];
        if(dict){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_processing")];
            [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeUpdateShippingDetails withAdditionalData:dict completionBlock:^(NSNumber *statusCode, NSDictionary *responseJSON, NSString *error){
                if(error){
                    NSLog(@"%@", error);
                    [BSUIManager showServerErrorAlert];
                }else{
                    if(statusCode.integerValue != 200){
                        [BSUIManager showServerErrorAlert];
                        NSLog(@"%@", responseJSON);
                    }else{
                        [[BSCoreDataAdapter sharedInstance] storeUserShippingDetails:dict];
                        [[BSCoreDataAdapter sharedInstance] updateObject:USER_INFO withValuesAndKeys:@{BSShippingRequestedKey: @(YES),
                                                                                                       BSOrdersRequestedKey : @(YES),
                                                                                                       BSProductsListsRequested : @(YES)}];
                        [self performSegueWithIdentifier:@"showHomeScreen" sender:self];
                    }
                }
                [BSIndicatorView hide];
            }];
        }
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embeddLogin"]){
        _loginViewController = segue.destinationViewController;
        _loginViewController.delegate = self;
    }
}

#pragma mark - BSAuthDelegate

- (void) commenceLoginWithEmail:(NSString *)email andPasword:(NSString *)password
{
    if(INTERNET_REACHABLE){
        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_logging_in")];
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeLogin withAdditionalData:@{kBSNEmail : email, kBSNPassword: password} completionBlock:^(NSNumber *statusCode, NSDictionary *responseJSON, NSString *error){
            if(error){
                [BSUIManager showServerErrorAlert];
            }else{
                if(statusCode.integerValue != 200)
                {
                    if(statusCode.integerValue == 405){
                        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(@"alert_invalid_username_password")];
                    }else{
                        [BSUIManager showServerErrorAlert];
                    }
                }else{
                    [[BSCoreDataAdapter sharedInstance] updateObject:USER_INFO withValuesAndKeys:@{BSShippingRequestedKey: @(NO),
                                                                                                   BSOrdersRequestedKey : @(NO),
                                                                                                   BSProductsListsRequested : @(NO)}];
                    [[BSCoreDataAdapter sharedInstance] storeUserEmail:email andAccessTokenFromResponse:responseJSON];
                    [self performSegueWithIdentifier:@"showHomeScreen" sender:self];
                }
            }
            [BSIndicatorView hide];
        }];
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}

- (void) commenceRegistrationWithEmail:(NSString*)email andPasword:(NSString *)password
{
    if(INTERNET_REACHABLE){
        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_registering")];
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeRegister withAdditionalData:@{kBSNEmail : email, kBSNPassword: password} completionBlock:^(NSNumber *statusCode, NSDictionary *responseJSON, NSString *error){
            if(error){
                [BSUIManager showServerErrorAlert];
            }else{
                if(statusCode.integerValue != 201)
                {
                    // 201 created
                    if(statusCode.integerValue == 405){
                        [BSUIManager showUserAlreadyExistsAlert];
                    }else{
                        [BSUIManager showServerErrorAlert];
                    }
                }else{
                    [[BSCoreDataAdapter sharedInstance] storeUserEmail:email andAccessTokenFromResponse:responseJSON];
                    [self showShippingSection];
                }
            }
            [BSIndicatorView hide];
        }];
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}

- (void) showShippingSection
{
    self.shippingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSShippingDetailsViewController"];
    self.shippingViewController.view.frame = (CGRect){.size = self.containerView.frame.size};
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    for(UIView *v in self.containerView.subviews){
        [v removeFromSuperview];
    }
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:self.containerView cache:YES];
    [self.containerView addSubview:self.shippingViewController.view];
    [UIView commitAnimations];
    
    [self.skipButton setEnabled:YES];
    [self.saveButton setEnabled:YES];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.forgotPasswordButton setAlpha:0];
        [self.skipButton setAlpha:1];
        [self.saveButton setAlpha:1];
        [self.infoView setAlpha:1];
    }completion:^(BOOL completed){
        if(completed){
            [self.forgotPasswordButton setEnabled:NO];
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

#pragma mark - Managing The Keyboard Display

- (void) keyboardWillShow
{
    [self handleKeyboardAppearance:YES];
}

- (void) keyboardWillHide
{
    [self handleKeyboardAppearance:NO];
}

-(void)handleKeyboardAppearance:(BOOL)isDisplayed
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.containerView.frame;
    if (isDisplayed)
    {
        rect.origin.y -= kBSKeyboardHeight;
        [self.logoView setFrame:logoMinimisedFrame];
    }
    else
    {
        rect.origin.y += kBSKeyboardHeight;
        [self.logoView setFrame:logoInitialFrame];
    }
    self.containerView.frame = rect;
    
    [UIView commitAnimations];
}

#pragma mark - UIAlertViewDelegate


- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    [_emailValidator setInputString:alertTextField.text];
    return ([_emailValidator execute]);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    _emailValidator = nil;
    forgotPasswordEmailInput = alertTextField.text;
    if(buttonIndex == 1){
        // ok button pressed, email is valid
        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeForgotPassword withAdditionalData:@{kBSNEmail : forgotPasswordEmailInput} completionBlock:^(NSNumber *statusCode, NSDictionary *responseJSON, NSString *errorMessage){
            [BSIndicatorView hide];
            if(errorMessage){
                [BSUIManager showServerErrorAlert];
            }else{
                if(statusCode.integerValue == 200){
                    //200 OK
                    [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_email_dispatched")];
                    forgotPasswordEmailInput = @"";
                }else{
                    [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:[NSString stringWithFormat: LOCALIZED_STRING(@"alert_username_not_found"), forgotPasswordEmailInput]];
                }
            }
        }];
    }else{
        forgotPasswordEmailInput = @"";
    }
}

@end
