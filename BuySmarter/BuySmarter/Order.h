//
//  Order.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/11/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Order : NSManagedObject

@property (nonatomic, retain) id products;
@property (nonatomic, retain) NSDate * date;

@end
