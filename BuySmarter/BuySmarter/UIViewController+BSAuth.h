//
//  UIViewController+BSAuth.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BSAuth)
/**
 *  Logs out of the account
 */
- (void)logOut;

@end
