//
//  BSNetworkingManager.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSNetworkingAdapter.h"
#import "BSCoreDataAdapter.h"
#import "Reachability.h"
#import "BSXMLParser.h"
#import "BSDebugManager.h"

static NSString * const BSBaseServerAddress = @"https://buysmarter.herokuapp.com";

@interface BSNetworkingAdapter()

@property (nonatomic) NSDictionary *routesDictionary;
@end

@implementation BSNetworkingAdapter

#pragma mark - Singleton

+ (BSNetworkingAdapter *)sharedInstance
{
    static BSNetworkingAdapter *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [BSNetworkingAdapter new];
    });
    return manager;
}

#pragma mark - accessor methods

- (NSDictionary *)routesDictionary
{
    if (!_routesDictionary){
        _routesDictionary = @{@(BSNetworkOperationTypeGETWishlist) : @"/account/wishlist",
                              @(BSNetworkOperationTypeGETShoppingCart) : @"/account/shoppingcart",
                              @(BSNetworkOperationTypeGETShippingDetails) : @"/account/shipping",
                              @(BSNetworkOperationTypeGETOrdersHistory) : @"/account/orders",
                              @(BSNetworkOperationTypeRegister) : @"/auth/register",
                              @(BSNetworkOperationTypeForgotPassword) : @"/auth/forgot",
                              @(BSNetworkOperationTypeLogin) : @"/auth/login",
                              @(BSNetworkOperationTypeUpdateShippingDetails) : @"/account/shipping",
                              @(BSNetworkOperationTypeUpdatePassword) : @"/account/password",
                              @(BSNetworkOperationTypeAddProduct) : @"/product/add",
                              @(BSNetworkOperationTypeDiscardProduct) : @"/product/discard",
                              @(BSNetworkOperationTypeSwitchProduct) : @"/product/switch",
                              @(BSNetworkOperationTypeCheckProductsAvailability) : @"/product/availability",
                              @(BSNetworkOperationTypeBookProducts) : @"/product/book",
                              @(BSNetworkOperationTypeCancelBooking) : @"/product/cancel",
                              @(BSNetworkOperationTypeProcessOrder) : @"/product/order",
                              @(BSNetworkOperationTypeGETProductDetails) : @"/product/details",
                              @(BSNetworkOperationTypeGETVendorsList) : @"/vendors/get",
                              @(BSNetworkOperationTypeGETOrdersHistoryPaginated) : @"/account/orders"
                              };
    }
    return _routesDictionary;
}

#pragma mark - Public methods

- (void) cancelOperation:(BSNetworkOperationType)operation
{
    NSString *operationPath = self.routesDictionary[@(operation)];
    NSAssert(operationPath, @"Invalid operation!");
    NSString *url = [NSString stringWithFormat:@"%@%@", BSBaseServerAddress, operationPath];
    [self cancelConnectionForURL:url];
}

- (void) performOperation:(BSNetworkOperationType)operationType  withAdditionalData:(NSDictionary *)data completionBlock:(BSOperationBlock)compBlock{

    BSStandardCompletionBlock completionBlock = [self adapterBlockForBlock:compBlock withOperation:operationType andAditionalData:data];
    
    switch (operationType) {
        case BSNetworkOperationTypeLogin: case BSNetworkOperationTypeRegister:
            [self performAuthenticationOperation:operationType withPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeGETWishlist: case BSNetworkOperationTypeGETShoppingCart:
            [self getUsersProductsListForOperationType:operationType withCompletionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeGETOrdersHistory:
            [self getUsersOrderHistoryWithCompletionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeAddProduct:
            [self addProductWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeForgotPassword:
            [self performForgotPasswordOperationWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeUpdateShippingDetails:
            [self updateUsersShippingDetailsWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeUpdatePassword:
            [self updateUsersPasswordWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeDiscardProduct: case BSNetworkOperationTypeSwitchProduct:
            [self performOperationForProductIdWithOperationType:operationType postData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeCheckProductsAvailability: case BSNetworkOperationTypeBookProducts:
            [self performOperationForProductIDSWithOperationType:operationType postData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeCancelBooking:
            [self cancelBookingWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeProcessOrder:
            [self processOrderWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeGETShippingDetails:
            [self getShippingDetailsWithCompletionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeGETProductDetails:
            [self getProductDetailsWithPostData:data completionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeGETVendorsList:
            [self getVendorsListWithCompletionBlock:completionBlock];
            break;
        case BSNetworkOperationTypeGetCurrencyRates:
            [self getCurrencyRatesWithCompletionBlock:compBlock];
            break;
        case BSNetworkOperationTypeGETOrdersHistoryPaginated:
            [self getOrdersHistoryPaginatedWithCompletionBlock:completionBlock];
            break;
        default:
            NSAssert(nil, @"Invalid operation type.");
            break;
    }
}

#pragma mark - Private methods

- (void) getOrdersHistoryPaginatedWithCompletionBlock:(BSStandardCompletionBlock) completionBlock
{
    int begin, end;
    begin =  USER_INFO.requestedHistoryCount.integerValue;
    end = begin + BSOrderHistoryIncrementalStep;
    NSDictionary *headers = [self headersWithAccessToken:YES];
    NSString *url = [NSString stringWithFormat:@"%@%@?%@=%d&%@=%d", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeGETOrdersHistoryPaginated)], kBSNBeginIndex, begin, kBSNEndIndex, end];
    [self newGETRequestForURL:url headers:headers completionBlock:completionBlock];
}

- (void) getCurrencyRatesWithCompletionBlock:(BSOperationBlock) completionBlock
{
    [self newGETRequestForURL:BSCurrencyRatesURL headers:nil completionBlock:^(NSString *url, NSNumber *statusCode, NSData *responseData, NSString *error){
        if(!error){
            NSDictionary *resultedDict = [BSXMLParser dictionaryForXMLData:responseData error:nil];
            NSMutableDictionary *outputDict = [@{} mutableCopy];
            if(resultedDict){
                NSArray *currencies = resultedDict[BSCurrencyDataSetKey][BSCurrencyBodyKey][BSCurrencyCubeKey][BSCurrencyRateKey];
                NSString *tempVal;
                for(NSDictionary *dict in currencies){
                    tempVal = dict[kBSNTextKey];
                    tempVal = [tempVal stringByReplacingOccurrencesOfString:@"\t" withString:@""];
                    tempVal = [tempVal stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    [outputDict setValue:@([tempVal floatValue])forKey:dict[kBSNCurrencyKey]];
                }
                completionBlock(statusCode, outputDict, nil);
            }else{
                completionBlock(statusCode, nil, @"Server error.");
            }
        }else{
            completionBlock(statusCode, nil, error);
        }
    }];
}

- (void) getVendorsListWithCompletionBlock:(BSStandardCompletionBlock) completionBlock
{
    NSDictionary *headers = [self headersWithAccessToken:YES];
    [self newGETRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeGETVendorsList)]] headers:headers completionBlock:completionBlock];
}

- (void) getShippingDetailsWithCompletionBlock:(BSStandardCompletionBlock) completionBlock
{
    NSDictionary *headers = [self headersWithAccessToken:YES];
    [self newGETRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeGETShippingDetails)]] headers:headers completionBlock:completionBlock];
}

- (void) processOrderWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeProcessOrder fromDataDictionary:dataDict]){
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData *data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeProcessOrder)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) cancelBookingWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeCancelBooking fromDataDictionary:dataDict]){
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData * data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeCancelBooking)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) performOperationForProductIDSWithOperationType:(BSNetworkOperationType)type postData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    NSAssert((type == BSNetworkOperationTypeBookProducts) ||(type == BSNetworkOperationTypeCheckProductsAvailability), @"Invalid operation type!");
    if([self isValidPostDataForOperationType:type fromDataDictionary:dataDict]){
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData *data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(type)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) updateUsersPasswordWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeUpdatePassword fromDataDictionary:dataDict]){
        NSData *data = [self dataFromDictionary:dataDict];
        NSDictionary *headers =[self headersWithAccessToken:YES];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeUpdatePassword)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) updateUsersShippingDetailsWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeUpdateShippingDetails fromDataDictionary:dataDict]){
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData *data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeUpdateShippingDetails)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) performForgotPasswordOperationWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeForgotPassword fromDataDictionary:dataDict]){
        NSData *data = [self dataFromDictionary:dataDict];
        NSDictionary *headers = [self headersWithAccessToken:NO];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeForgotPassword)]] headers:headers postData:data completionBlock:completionBlock];
    }
}
- (void) performOperationForProductIdWithOperationType:(BSNetworkOperationType)type postData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    NSAssert((type == BSNetworkOperationTypeDiscardProduct) || (type == BSNetworkOperationTypeSwitchProduct), @"Invalid operation type!");
    if([self isValidPostDataForOperationType:type fromDataDictionary:dataDict]){
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData *data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(type)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) getProductDetailsWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeGETProductDetails fromDataDictionary:dataDict]){
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData * data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeGETProductDetails)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) addProductWithPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock
{
    if([self isValidPostDataForOperationType:BSNetworkOperationTypeAddProduct fromDataDictionary:dataDict]){
        NSAssert(([dataDict[kBSNProductCategory] isEqualToString:kBSNProductCategoryShoppingCart] || [dataDict[kBSNProductCategory] isEqualToString:kBSNProductCategoryWishList]), @"Invalid product category");
        NSDictionary *headers = [self headersWithAccessToken:YES];
        NSData * data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeAddProduct)]] headers:headers postData:data completionBlock:completionBlock];
    }
}

- (void) getUsersOrderHistoryWithCompletionBlock:(BSStandardCompletionBlock) completionBlock
{
    NSDictionary *headers = [self headersWithAccessToken:YES];
    [self newGETRequestForURL:[NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(BSNetworkOperationTypeGETOrdersHistory)]] headers:headers completionBlock:completionBlock];
}

- (void) getUsersProductsListForOperationType:(BSNetworkOperationType)type withCompletionBlock:(BSStandardCompletionBlock) completionBlock
{
    NSAssert((type == BSNetworkOperationTypeGETWishlist) || (type == BSNetworkOperationTypeGETShoppingCart), @"Invalid operation type.");
    NSDictionary *headers = [self headersWithAccessToken:YES];
    NSString *url = [NSString stringWithFormat:@"%@%@",BSBaseServerAddress, self.routesDictionary[@(type)]];
    [self newGETRequestForURL:url headers:headers completionBlock:completionBlock];
}

- (void) performAuthenticationOperation:(BSNetworkOperationType)operationType withPostData:(NSDictionary *)dataDict completionBlock:(BSStandardCompletionBlock) completionBlock {
    
    NSAssert((operationType == BSNetworkOperationTypeRegister || operationType == BSNetworkOperationTypeLogin), @"Invalid operation type!!!");
    if([self isValidPostDataForOperationType:operationType fromDataDictionary:dataDict]){
        NSString *urlString = [NSString stringWithFormat:@"%@%@", BSBaseServerAddress, self.routesDictionary[@(operationType)]];
        NSData * data = [self dataFromDictionary:dataDict];
        [self newPOSTRequestForURL:urlString headers:[self headersWithAccessToken:NO] postData:data completionBlock:completionBlock];
    }
}

#pragma mark - helper methods

- (BOOL) isValidPostDataForOperationType:(BSNetworkOperationType)operationType fromDataDictionary:(NSDictionary *)dataDict
{
    BOOL result = YES;
    switch (operationType) {
        case BSNetworkOperationTypeLogin: case BSNetworkOperationTypeRegister:
            result = (dataDict[kBSNPassword] != nil) &&(dataDict[kBSNEmail] != nil);
            break;
        case BSNetworkOperationTypeAddProduct:
            result = (dataDict[kBSNProductIdentifier] != nil) && (dataDict[kBSNVendorIdKey] != nil) && (dataDict[kBSNProductCategory] != nil);
            break;
        case BSNetworkOperationTypeGETProductDetails:
            result = (dataDict[kBSNProductIdentifier] != nil) && (dataDict[kBSNVendorIdKey] != nil);
            break;
        case BSNetworkOperationTypeForgotPassword:
            result = dataDict[kBSNEmail] != nil;
            break;
        case BSNetworkOperationTypeUpdateShippingDetails:
            result = (dataDict[kBSNAddressLine] != nil) && (dataDict[kBSNPostalCode] != nil) && (dataDict[kBSNCity] != nil) && (dataDict[kBSNPhoneNumber] != nil) &&
            (dataDict[kBSNFirstName] != nil) && (dataDict[kBSNLastName] != nil);
            break;
        case BSNetworkOperationTypeUpdatePassword:
            result = (dataDict[kBSNOldPassword] != nil) && (dataDict[kBSNNewPassword] != nil);
            break;
        case BSNetworkOperationTypeDiscardProduct: case BSNetworkOperationTypeSwitchProduct:
            result = dataDict[kBSNIDKey] != nil;
            break;
        case BSNetworkOperationTypeCheckProductsAvailability: case BSNetworkOperationTypeBookProducts:
            result = (dataDict[kBSNProductsKey] != nil) && (dataDict[kBSNVendorIdKey] != nil);
            break;
        case BSNetworkOperationTypeCancelBooking:
            result = dataDict[kBSNVendorIdKey] != nil;
            break;
        case BSNetworkOperationTypeProcessOrder:
            result = (dataDict[kBSNVendorIdKey] != nil) && (dataDict[kBSNConfirmationKey] != nil);
            break;
        default:
            break;
    }
    NSAssert(result, @"Invalid arguments for operation type!");
    return  result;
}

- (NSMutableDictionary *)headersWithAccessToken:(BOOL)withAccessToken
{
    NSMutableDictionary *dictionary = [@{@"Content-Type" : @"application/json"} mutableCopy];
    if(withAccessToken){
        NSAssert(USER_INFO.accessToken != nil, @"Access token is nil!");
        [dictionary setObject:USER_INFO.accessToken forKey:kBSNAccessTokenKey];
    }
    return dictionary;
}

- (NSData *)dataFromDictionary:(NSDictionary *)dict
{
    NSError *error;
    NSData * data = [NSJSONSerialization dataWithJSONObject:dict
                                                    options:kNilOptions
                                                      error:&error];
    if(error){
        NSAssert(nil, @"Error occured while serializing < %@ > : %@", dict, error.description);
        return nil;
    }else{
        return data;
    }
}

#pragma mark - utility methods

-  (BSStandardCompletionBlock) adapterBlockForBlock:(BSOperationBlock)block withOperation:(BSNetworkOperationType)operation andAditionalData:(NSDictionary *)postData
{
    NSAssert(block != nil, @"Invalid completion block!!!");
    return ^(NSString * url, NSNumber *statusCode, NSData *receivedData, NSString *errorMessage){
        NSDictionary *json = [self jsonDictionaryFromData:receivedData error:nil];
        [BSDebugManager printPostData:postData statusCode:statusCode responseDictionary:json andError:errorMessage forOperationType:operation];
        block(statusCode, json, errorMessage);
    };
}

- (NSDictionary *) jsonDictionaryFromData:(NSData *)receivedData error:(NSError **)error
{
    if(!receivedData){
        return nil;
    }
    NSError *err;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&err];
    if(err){
        if(error){
            *error = err;
        }
        return nil;
    }else{
        error = nil;
        return json;
    }
    
}

@end
