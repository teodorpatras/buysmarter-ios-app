//
//  BSLoginViewController.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSLoginViewController.h"
#import "BSRegisterViewController.h"
#import "BSUIManager.h"
#import "BSValidationManager.h"
#import "BSEmailValidator.h"
#import "BSPasswordValidator.h"
#import "BSConstants.h"
#import "BSTextField.h"
#import "BSUIManager.h"

@interface BSLoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (nonatomic) BSRegisterViewController *registerViewController;
@property (nonatomic) BSValidationManager *validationManager;
@property (weak, nonatomic) IBOutlet BSTextField *emailTextField;
@property (weak, nonatomic) IBOutlet BSTextField *passwordTextField;
@end

@implementation BSLoginViewController

#pragma mark - viewcontroller lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    [self configureLocalization];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configureLocalization) name:BSLanguageChangedNotification object:nil];
}

- (void) configureLocalization
{
    [self.logInButton setTitle:LOCALIZED_STRING(@"login_button_title") forState:UIControlStateNormal];
    [self.registerButton setTitle:LOCALIZED_STRING(@"register_button_title") forState:UIControlStateNormal];
    self.emailTextField.placeholder = LOCALIZED_STRING(@"email_field_placeholder");
    self.passwordTextField.placeholder = LOCALIZED_STRING(@"password_field_placeholder");
    
}

- (void) configureView
{
    [BSUIManager customizeButton:self.logInButton withResizableImageNamed:@"OrangeButton-Normal"];
    [_emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_passwordTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_emailTextField setReturnKeyType:UIReturnKeyNext];
    [_emailTextField setValidationIdentifier:kvBSEmailIdentifier];
    [_passwordTextField setValidationIdentifier:kvBSPasswordIdentifier];
}

#pragma mark - accessor methods

- (BSRegisterViewController *)registerViewController
{
    if(!_registerViewController){
        _registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BSRegisterViewController"];
        _registerViewController.view.frame = (CGRect){.size = {self.view.frame.size.width, self.view.frame.size.height}};
        _registerViewController.delegate = self.delegate;
    }
    return _registerViewController;
}
- (BSValidationManager *)validationManager
{
    if(!_validationManager){
        _validationManager = [BSValidationManager new];
        [_validationManager registerClass:[BSEmailValidator class] withIdentifier:kvBSEmailIdentifier andErrorMessage:@"alert_invalid_email"];
        [_validationManager registerClass:[BSPasswordValidator class] withIdentifier:kvBSPasswordIdentifier andErrorMessage:@"alert_password_invalid"];
    }
    return _validationManager;
}
#pragma mark - callbacks

- (void) hideKeyboard
{
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
}
- (IBAction)loginAction {

    
    [self.validationManager registerInput:_emailTextField.text withValidatorIdentifier:_emailTextField.validationIdentifier];
    [self.validationManager registerInput:_passwordTextField.text withValidatorIdentifier:_passwordTextField.validationIdentifier];
    NSString *error;
    [self.validationManager validateInputs:&error];
    if(error){
        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(error)];
    }else{
        [self hideKeyboard];
        [self.delegate commenceLoginWithEmail:_emailTextField.text andPasword:_passwordTextField.text];
    }
}

- (IBAction)registerAction {
    [self hideKeyboard];
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:self.view cache:YES];
    [self.view addSubview:self.registerViewController.view];
    
    [UIView commitAnimations];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(BSTextField *)textField
{
    NSString *error;
    if(textField.text.length > 0){
        [self.validationManager validateInput:textField.text forIdentifier:textField.validationIdentifier error:&error];
        if(error){
            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_warning_title") andText:LOCALIZED_STRING(error)];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_emailTextField]){
        [_passwordTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - Public Methods

- (NSString *)email
{
    return self.emailTextField.text;
}

@end
