//
//  BSBaseInputViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSBaseInputViewController.h"
#import "BSConstants.h"

@implementation BSBaseInputViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideKeyboard)
                                                 name:BSHideKeyboardNotification
                                               object:nil];
}

- (void) hideKeyboard
{
    [self.view.subviews enumerateObjectsUsingBlock:^(id view, NSUInteger index, BOOL *stop){
        if([view isKindOfClass:[UITextField class]]){
            [view resignFirstResponder];
        }
    }];
}

@end
