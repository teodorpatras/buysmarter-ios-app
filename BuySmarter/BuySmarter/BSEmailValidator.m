//
//  BSEmailValidator.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSEmailValidator.h"

@implementation BSEmailValidator

- (BOOL) execute{
    
    if(!self.inputString){
        return NO;
    }
    NSString *expression = @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
    NSError *error = NULL;
    BOOL result;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:self.inputString options:0 range:NSMakeRange(0, [self.inputString length])];
    if (match){
        result = YES;
    }else{
        result = NO;
    }
    return result;
}

@end
