//
//  BSProductDetailsViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductDetailsViewController.h"
#import "BSPhotoGalleryView.h"
#import "BSConstants.h"
#import "BSConnectionManager.h"
#import "BSUIManager.h"
#import "BSProductDetailViewsFactory.h"
#import "BSNetworkingAdapter.h"
#import "BSCoreDataAdapter.h"
#import "BSIndicatorView.h"

static const CGFloat BSGalleryViewHeight = 200.0;

@interface BSProductDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic) BSProductTitleView *titleView;
@property (nonatomic) BSPhotoGalleryView *galleryView;
@property (nonatomic) BSProductDescriptionView *descriptionView;
@property (nonatomic) BSProductStockPriceView *stockPriceView;
@end

@implementation BSProductDetailsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.product.productTitle;
    [self configureScrollView];
    
}

- (void) configureScrollView
{
    self.titleView = [BSProductDetailViewsFactory productDetailViewOfType:BSProductDetailViewTypeTitleView withProduct:self.product];
    self.galleryView = [BSPhotoGalleryView photoGalleryViewForFrame:(CGRect){.size = {SCREEN_WIDTH, BSGalleryViewHeight}} withDataSourceArray:self.product.imageURLsArray];
    self.descriptionView = [BSProductDetailViewsFactory productDetailViewOfType:BSProductDetailViewTypeDescriptionView withProduct:self.product];
    self.stockPriceView = [BSProductDetailViewsFactory productDetailViewOfType:BSProductDetailViewTypeStockPriceView withProduct:self.product];;
    
    if(_shouldConfigureForOrderProduct){
        [self.scrollView setFrame:self.view.frame];
        [self.toolBar removeFromSuperview];
        [self.stockPriceView configureForOrderedProduct];
    }
    
    NSArray *views = @[self.titleView, self.galleryView, self.stockPriceView, self.descriptionView];
    
    CGSize contentSize;
    contentSize.width = SCREEN_WIDTH;
    contentSize.height = CGRectGetHeight(self.titleView.frame) + CGRectGetHeight(self.galleryView.frame) + CGRectGetHeight(self.descriptionView.frame) + CGRectGetHeight(self.stockPriceView.frame) + BSSidePadding * views.count;
    if(!_shouldConfigureForOrderProduct){
        contentSize.height += CGRectGetHeight(self.toolBar.frame);
    }
    
    self.scrollView.contentSize = contentSize;
    
    CGRect frame = self.titleView.frame;
    frame.origin = (CGPoint){0, _shouldConfigureForOrderProduct? 0 : BSTopBarsHeight};
    frame.size = self.titleView.frame.size;
    [self.titleView setFrame:frame];
    [self.scrollView addSubview:self.titleView];
    
    for(int i = 1; i < views.count; i++){
        frame = [views[i] frame];
        frame.origin = (CGPoint){0, CGRectGetMaxY([views[i - 1] frame]) + BSSidePadding};
        [views[i] setFrame:frame];
        //NSLog(@"Frame : %@", NSStringFromCGRect(frame));
        [self.scrollView addSubview:views[i]];
    }
}

#pragma mark - Callbacks

- (IBAction)action
{
    if(INTERNET_REACHABLE){
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:LOCALIZED_STRING(@"please_select_an_action")
                                      delegate:self
                                      cancelButtonTitle:LOCALIZED_STRING(@"cancel_button_title")
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:(self.product.productCategory.integerValue == BSProductCategoryWishList? LOCALIZED_STRING(@"product_details_move_to_shopping_cart") : LOCALIZED_STRING(@"product_details_move_to_wishlist")), LOCALIZED_STRING(@"product_details_refresh_stock"), nil];
        [actionSheet showInView:self.view];
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}

- (IBAction) deleteAction
{
    if(INTERNET_REACHABLE){
         [[[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_warning_title") message:LOCALIZED_STRING(@"alert_are_you_sure_discard") delegate:self cancelButtonTitle:LOCALIZED_STRING(@"cancel_button_title") otherButtonTitles:LOCALIZED_STRING(@"yes_answer"), nil] show];
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}

#pragma mark - UIActionSheetDelegate methods

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [actionSheet.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            button.titleLabel.textColor = BSCOLOR;
        }else if([subview isKindOfClass:[UILabel class]]){
            [(UILabel *)subview setTextColor:BSCOLOR];
        }
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        //switch
        if(INTERNET_REACHABLE){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
            [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeSwitchProduct withAdditionalData:@{kBSNIDKey : self.product.databaseId} completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
                [BSIndicatorView hide];
                if(!error){
                    if(statusCode.integerValue == 200){
                        NSDictionary *valuesDict = @{BSProductCategoryKey : (self.product.productCategory.integerValue == BSProductCategoryWishList?@(BSProductCategoryShoppingCart) : @(BSProductCategoryWishList))};
                        [[BSCoreDataAdapter sharedInstance]updateObject:self.product withValuesAndKeys:valuesDict];
                        [self.delegate refreshDataSource];
                        [self.delegate changeToCategory:self.product.productCategory.integerValue];
                        [self.navigationController popToRootViewControllerAnimated:YES];
//                        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_operation_finished_successfully")];
                    }else{
                        if(statusCode.integerValue == 405){
                            [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                        }else{
                            [BSUIManager showServerErrorAlert];
                        }
                    }
                }else{
                    [BSUIManager showServerErrorAlert];
                }
            }];
        }else{
            [BSUIManager showInternetNotReachableAlert];
        }
    }else if(buttonIndex == 1){
        // refresh
        if(INTERNET_REACHABLE){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
            [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeCheckProductsAvailability withAdditionalData:@{kBSNVendorIdKey : self.product.vendorIdentifier, kBSNProductsKey : @[self.product.productIdentifier]} completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
                [BSIndicatorView hide];
                if(!error){
                    if(statusCode.integerValue == 200){
                        NSDictionary *valuesDict = @{BSProductStockKey : responseDict[kBSNProductsKey][0][kBSNProductStock]};
                        [[BSCoreDataAdapter sharedInstance] updateObject:self.product withValuesAndKeys:valuesDict];
                        [self.delegate refreshDataSource];
                        [_stockPriceView updateStock:self.product.productStock.integerValue];
                    }else{
                        if(statusCode.integerValue == 405){
                            [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                        }else{
                            [BSUIManager showServerErrorAlert];
                        }
                    }
                }else{
                    [BSUIManager showServerErrorAlert];
                }
            }];
        }else{
            [BSUIManager showInternetNotReachableAlert];
        }
    }
}

#pragma mark - UIAlertViewDelegate methods

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [super alertView:alertView clickedButtonAtIndex:buttonIndex];
    if(buttonIndex == 1){
        // YES
        if(INTERNET_REACHABLE){
            [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeDiscardProduct withAdditionalData:@{kBSNIDKey : self.product.databaseId} completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
                if(!error){
                    if(statusCode.integerValue == 200){
                        [[BSCoreDataAdapter sharedInstance] deleteObject:self.product];
                        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_product_successfully_discarded")];
                        [self.delegate refreshDataSource];
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }else{
                        if(statusCode.integerValue == 405){
                            [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                        }else{
                            [BSUIManager showServerErrorAlert];
                        }
                    }
                }else{
                    [BSUIManager showServerErrorAlert];
                }
            }];
        }else{
            [BSUIManager showInternetNotReachableAlert];
        }
    }
}

@end
