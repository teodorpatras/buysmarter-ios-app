//
//  BSIndicatorView.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSAccessoryView.h"

@interface BSIndicatorView : UIAlertView

/**
 *  Displays the indicator view with the specified text
 *
 *  @param text     text to be displayed within the view
 */
+ (void) showWithText:(NSString *)text;
/**
 *  Hides the view
 */
+ (void) hide;

@end
