//
//  BSScanViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/1/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarReaderView.h"
#import "BSProductLoaderViewController.h"
#import "BSBaseViewController.h"

@protocol BSScannerDelegate <NSObject>
/**
 *  Informs the delegate that it finished scanning
 *
 *  @param dictionary   dictionary representation of the product
 *  @param authErr      whether or not authentication error occured
 */
- (void) scannerViewControllerDidFinishWithDictionary:(NSDictionary *)dictionary authenticationError:(BOOL)authErr;
@end

@interface BSScanViewController : BSBaseViewController<ZBarReaderViewDelegate, BSProductLoaderDelegate>
@property (nonatomic, weak) id<BSScannerDelegate> delegate;
@end
