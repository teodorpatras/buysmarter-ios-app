//
//  BSLocalizationManager.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSLocalizationManager.h"
#import "BSCoreDataAdapter.h"

NSString * const SupportedLanguages[] = {@"en",@"ro"};
static NSBundle *bundle = nil;

@implementation BSLocalizationManager

+ (NSString *)getLanguageForType:(BSSupportedLanguageType)type
{
    NSAssert(type == BSSupportedLanguageTypeEN || type == BSSupportedLanguageTypeRO, @"Invalid type!!!");
    return SupportedLanguages[type];
}

+ (BSSupportedLanguageType)getTypeForLanguage:(NSString *)language
{
    NSAssert([language isEqualToString:@"en"] || [language isEqualToString:@"ro"], @"Invalid language!!!");
    return [language isEqualToString:@"en"]?BSSupportedLanguageTypeEN : BSSupportedLanguageTypeRO;
}

+ (void)setLocalizedLanguage:(NSString *)language
{
    NSString *path = [[ NSBundle mainBundle ] pathForResource:language ofType:@"lproj" ];
    bundle = [NSBundle bundleWithPath:path];
}

+ (NSString *)localizedStringForKey:(NSString *)key comment:(NSString *)comment
{
    if(!bundle){
        if(![BSCoreDataAdapter sharedInstance].storedUserInfo.preferredLanguage){
            [self setLocalizedLanguage:@"en"];
        }else{
            [self setLocalizedLanguage:[BSCoreDataAdapter sharedInstance].storedUserInfo.preferredLanguage];
        }
    }
    NSString *string = NSLocalizedStringFromTableInBundle(key, nil, bundle, comment);
    return string;
}

+ (void) deallocate{
    bundle = nil;
}

@end
