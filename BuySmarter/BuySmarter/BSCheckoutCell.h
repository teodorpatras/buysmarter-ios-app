//
//  BSCheckoutCell.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vendor.h"

@interface BSCheckoutCell : UITableViewCell

/**
 *  Configuration method
 *
 *  @param vendor           vendor for the cell to be configured with
 *  @param productsArray    the products array for the cell to be configured with
 */
- (void) configureWithVendor:(Vendor *)vendor andProductsArray:(NSArray *)productsArray;

@end
