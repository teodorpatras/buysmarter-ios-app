//
//  BSPhotoCollectionView.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/3/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSPhotoGalleryView.h"
#import "BSPhotoCollectionViewCell.h"
#import "UIImageView+BSAsync.h"
#import "BSImageLoader.h"
#import "BSConstants.h"

@interface BSPhotoGalleryView(){
    NSInteger previousPage;
}
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSArray *dataSourceArray;
@end

@implementation BSPhotoGalleryView

#pragma mark - Class Methods

+ (BSPhotoGalleryView *) photoGalleryViewForFrame:(CGRect)frame withDataSourceArray:(NSArray *)imageUrls
{
    BSPhotoGalleryView* photoGalleryView = [[NSBundle mainBundle] loadNibNamed:@"BSPhotoGalleryView" owner:self options:nil][0];
    [photoGalleryView setFrame:frame];
    [photoGalleryView setDataSourceArray:imageUrls];
    return photoGalleryView;
}

#pragma mark - Private methods

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    NSAssert(self.dataSourceArray != nil, @"Datasource array must be populated with URL's before adding the photo collection view to it's superview.");
    [self setupCollectionView];
}

-(void)setupCollectionView {
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    [self.collectionView registerClass:[BSPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
    if(self.dataSourceArray.count <= 1){
        [self.rightImageView setAlpha:0];
    }
}


- (void) startImageDownloadForIndexPath:(NSIndexPath *)indexPath
{
    [[BSImageLoader sharedInstance] getImageWithURL:self.dataSourceArray[indexPath.row] withCompletionBlock:^(UIImage *image, NSString *error){
        if(!error){
            BSPhotoCollectionViewCell *photoCell = (BSPhotoCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [photoCell setImage:image animated:YES];
        }else{
            //NSLog(@"ERROR: %@", error);
        }
    }];
}

#pragma mark - Public methods

- (void) reloadData{
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewData

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        previousPage = page;
        //NSLog(@"Scrolled to %li", (long)page);
        [UIView animateWithDuration:0.3 animations:^{
            if(page == 0 && self.dataSourceArray.count > 1){
                [_leftImageView setAlpha:0];
                [_rightImageView setAlpha:1];
            }else if(page == self.dataSourceArray.count - 1 && self.dataSourceArray.count > 1){
                [_rightImageView setAlpha:0];
                [_leftImageView setAlpha:1];
            }else{
                [_leftImageView setAlpha:0.5];
                [_rightImageView setAlpha:0.5];
            }
        }];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataSourceArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSAssert([_dataSourceArray[indexPath.row] isKindOfClass:[UIImage class]] || [_dataSourceArray[indexPath.row] isKindOfClass:[NSString class]], @"Invalid data type in data source array.");
    BSPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    if([_dataSourceArray[indexPath.row] isKindOfClass:[UIImage class]]){
        [cell setImage:_dataSourceArray[indexPath.row] animated:NO];
    }else{
        UIImage *img = [[BSImageLoader sharedInstance] cachedImageFromURL:self.dataSourceArray[indexPath.row]];
        if(img){
            [cell setImage:img animated:NO];
        }else{
            [cell clearImageView];
            [self startImageDownloadForIndexPath:indexPath];
        }
    }
    return cell;
}

@end
