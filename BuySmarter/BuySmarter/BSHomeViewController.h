//
//  BSHomeViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSScanViewController.h"
#import "BSProductDetailsViewController.h"
#import "BSBaseViewController.h"
#import "BSCheckoutViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface BSHomeViewController : BSBaseViewController<UITableViewDelegate, UITableViewDataSource, BSScannerDelegate, BSProductDetailsViewControllerDelegate, BSCheckoutViewControllerDelegate, MFMailComposeViewControllerDelegate>

@end
