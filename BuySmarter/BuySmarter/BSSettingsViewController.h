//
//  BSSettingsViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/19/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSBaseViewController.h"

@interface BSSettingsViewController : BSBaseViewController

@end
