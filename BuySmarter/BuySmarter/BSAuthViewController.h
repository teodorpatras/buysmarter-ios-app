//
//  BSAuthViewController.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSAuthDelegate.h"

@interface BSAuthViewController : UIViewController<BSAuthDelegate, UIAlertViewDelegate>

/**
 *  Resets to the initial state
 */
- (void) resetToInitialState;

@end
