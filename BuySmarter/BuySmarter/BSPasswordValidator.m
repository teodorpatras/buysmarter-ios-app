//
//  BSPasswordValidator.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSPasswordValidator.h"

@implementation BSPasswordValidator

- (BOOL)execute
{
    if(!self.inputString){
        return NO;
    }
    NSString *expression = @"^.*(?=.{6,})(?=.*[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*$";
    NSError *error = NULL;
    BOOL result;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:self.inputString options:0 range:NSMakeRange(0, [self.inputString length])];
    if (match){
        result = YES;
    }else{
        result = NO;
    }
    return result;
}

@end
