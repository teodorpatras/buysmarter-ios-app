//
//  BSProductHeaderView.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vendor.h"

static const CGFloat BSVendorHeaderHeight = 50.0f;

@interface BSVendorHeaderView : UIView
/**
 *  Factory method
 *
 *  @param vendor   the vendor object for the view to be configured
 *
 *  @return         the instance of the header view
 */
+ (BSVendorHeaderView *)headerViewForVendor:(Vendor *)vendor;

@end
