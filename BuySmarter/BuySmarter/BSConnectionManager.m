//
//  BSConnectionManager.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSConnectionManager.h"
#import "Reachability.h"

static NSString * const WWHostName = @"www.google.com";

@interface BSConnectionManager()
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) NetworkStatus networkStatus;
@end
@implementation BSConnectionManager
#pragma mark - Singleton

+ (instancetype) sharedInstance
{
    static BSConnectionManager *sharedManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedManager = [BSConnectionManager new];
    });
    return sharedManager;
}

#pragma mark - Public methods

- (void) monitorConectivityStatus
{
    if(!self.hostReachability){
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        self.hostReachability = [Reachability reachabilityWithHostName:WWHostName];
        [self.hostReachability startNotifier];
    }
}

- (BOOL) internetConnectionAvailable
{
    return _networkStatus != NotReachable;
}

#pragma mark - Notification method

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    _networkStatus = [curReach currentReachabilityStatus];
    if(_networkStatus == NotReachable){
        NSLog(@"Internet not reachable!");
    }else if(_networkStatus == ReachableViaWWAN){
        NSLog(@"Internet reachable via WWAN!");
    }else{
        NSLog(@"Internet reachable via WiFi!");
    }
}

#pragma mark - Object lifecycle

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
