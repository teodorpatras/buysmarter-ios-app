//
//  BSAccessoryView.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSAccessoryView.h"

@implementation BSAccessoryView

+ (BSAccessoryView *)accessoryViewWithText:(NSString *)text
{
    BSAccessoryView * accessoryView = [[NSBundle mainBundle] loadNibNamed:@"BSAccessoryView" owner:self options:nil][0];
//    CGSize expectedLabelSize = [text sizeWithFont:[accessoryView.label font]
//                                       constrainedToSize:CGSizeMake(CGRectGetWidth(accessoryView.label.frame), CGRectGetHeight(accessoryView.frame))
//                                           lineBreakMode:[accessoryView.label lineBreakMode]];
    CGSize expectedLabelSize = [text sizeWithAttributes:@{NSFontAttributeName : [accessoryView.label font]}];
    CGRect newFrame = accessoryView.label.frame;
    newFrame.size.height = expectedLabelSize.height;
    newFrame.size.width = expectedLabelSize.width;
    accessoryView.label.frame = newFrame;
    accessoryView.label.text = text;
    
    newFrame = accessoryView.frame;
    newFrame.size.width = CGRectGetMaxX(accessoryView.label.frame);
    [accessoryView setFrame:newFrame];
    
    [accessoryView.indicatorView setColor:[UIColor blackColor]];
    
    return accessoryView;
}
@end
