//
//  Product.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/12/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "Product.h"


@implementation Product

@dynamic imageURLsArray;
@dynamic productDescription;
@dynamic productIdentifier;
@dynamic productPrice;
@dynamic productStock;
@dynamic productTitle;
@dynamic productURL;
@dynamic vendorIdentifier;
@dynamic productCategory;
@dynamic databaseId;

@end
