//
//  BSTextField.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSTextField.h"
static const float BSSideOffset = 5.0;
static const float BSTopOffset = -2.0;
static const float BSBottomOffset = 3;

@interface BSTextField ()
@property (nonatomic) UIEdgeInsets edgeInsets;
@end

@implementation BSTextField

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        _edgeInsets = UIEdgeInsetsMake(BSTopOffset, BSSideOffset, BSBottomOffset, BSSideOffset);
        [self customize];
    }
    return self;
}

- (void) customize
{
    self.leftViewMode = UITextFieldViewModeAlways;
    [self setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self setBorderStyle:UITextBorderStyleNone];
    // font, text color
    self.textColor = [UIColor blackColor];
    [self setBackground:[[UIImage imageNamed:@"TextFieldBackground"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, BSSideOffset, BSSideOffset, BSSideOffset)]];
    [self setBackgroundColor:[UIColor clearColor]];
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

@end
