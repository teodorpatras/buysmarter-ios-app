//
//  BSDefaultBackgroundedView.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSDefaultBackgroundedView : UIView

@end
