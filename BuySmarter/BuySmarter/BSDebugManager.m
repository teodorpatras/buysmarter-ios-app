//
//  BSDebugManager.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSDebugManager.h"

@implementation BSDebugManager

+ (void)printPostData:(NSDictionary *)postData statusCode:(NSNumber *)statusCode responseDictionary:(NSDictionary *)dict andError:(NSString *)error forOperationType:(BSNetworkOperationType)operationType
{
    NSString *outerSeparator = @"**************************";
    NSString *innerSeparator = @"--------------------";
    NSString *operation = nil;
    switch (operationType) {
        case BSNetworkOperationTypeGETShippingDetails:
            operation = @"GET SHIPPING DETAILS";
            break;
        case BSNetworkOperationTypeAddProduct:
            operation = @"ADD PRODUCT";
            break;
        case BSNetworkOperationTypeBookProducts:
            operation = @"BOOK PRODUCTS";
            break;
        case BSNetworkOperationTypeCancelBooking:
            operation = @"CANCEL BOOKING";
            break;
        case BSNetworkOperationTypeCheckProductsAvailability:
            operation = @"CHECK AVAILABILITY";
            break;
        case BSNetworkOperationTypeDiscardProduct:
            operation = @"DISCARD PRODUCT";
            break;
        case BSNetworkOperationTypeForgotPassword:
            operation = @"FORGOT PASSWORD";
            break;
        case BSNetworkOperationTypeGetCurrencyRates:
            operation = @"GET CURRENCY RATES";
            break;
        case BSNetworkOperationTypeGETOrdersHistory:
            operation = @"GET ORDER HISTORY";
            break;
        case BSNetworkOperationTypeGETProductDetails:
            operation = @"GET PRODUCT DETAILS";
            break;
        case BSNetworkOperationTypeGETShoppingCart:
            operation = @"GET SHOPPING CART";
            break;
        case BSNetworkOperationTypeGETVendorsList:
            operation = @"GET VENDORS LIST";
            break;
        case BSNetworkOperationTypeGETWishlist:
            operation = @"GET WISH LIST";
            break;
        case BSNetworkOperationTypeLogin:
            operation = @"LOGIN";
            break;
        case BSNetworkOperationTypeProcessOrder:
            operation = @"PROCESS ORDER";
            break;
        case BSNetworkOperationTypeRegister:
            operation = @"REGISTER";
            break;
        case BSNetworkOperationTypeSwitchProduct:
            operation = @"SWTICH PRODUCT";
            break;
        case BSNetworkOperationTypeUpdatePassword:
            operation = @"UPDATE PASSWORD";
            break;
        case BSNetworkOperationTypeUpdateShippingDetails:
            operation = @"UPDATE SHIPPING";
            break;
        default:
            break;
    }
    NSString *start = [NSString stringWithFormat:@"%@ <[%@]> %@", outerSeparator, operation, outerSeparator];
    NSString *postString;
    if(postData){
        postString = [NSString stringWithFormat:@"%@ POST DATA %@\n\n%@\n\n%@ END POST DATA %@", innerSeparator, innerSeparator, postData, innerSeparator, innerSeparator];
    }else{
        postString = @"";
    }
    NSString *statusCodeString = [NSString stringWithFormat:@"\n[STATUS CODE]:\t\t%@", statusCode];
    NSString *errorString = [NSString stringWithFormat:@"\n[ERROR]:\t\t\t%@", error];
    NSString *responseString = [NSString stringWithFormat:@"\n%@ RESPONSE %@\n\n%@\n\n%@ END RESPONSE %@", innerSeparator, innerSeparator, dict, innerSeparator, innerSeparator];
    NSString *endString = [NSString stringWithFormat:@"%@ END %@ %@", outerSeparator, operation, outerSeparator];
    
    NSLog(@"\n%@\n%@\n%@\n%@\n%@\n%@", start, postString, statusCodeString, errorString, responseString, endString);
}

@end
