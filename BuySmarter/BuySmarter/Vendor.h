//
//  Vendor.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/13/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

static NSString * const BSVendorCurrencyKey = @"vendorCurrency";
static NSString * const BSVendorIdentifierKey = @"vendorIdentifier";
static NSString * const BSVendorNameKey = @"vendorName";
static NSString * const BSVendorLogoURLKey = @"vendorLogoURL";
static NSString * const BSVendorApiKey = @"vendorApiKey";
static NSString * const BSVendorEmailKey = @"vendorEmail";

@interface Vendor : NSManagedObject

@property (nonatomic, retain) NSString * vendorCurrency;
@property (nonatomic, retain) NSString * vendorIdentifier;
@property (nonatomic, retain) NSString * vendorName;
@property (nonatomic, retain) NSString * vendorLogoURL;
@property (nonatomic, retain) NSString * vendorApiKey;
@property (nonatomic, retain) NSString * vendorEmail;

@end
