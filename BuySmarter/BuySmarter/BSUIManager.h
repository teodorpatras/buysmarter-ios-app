//
//  BSUIManager.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSUIManager : NSObject

+ (void) customizeButton:(UIButton *) button withResizableImageNamed:(NSString *)imageName;
+ (void) configureViewWithBorderShadowAndRoundedCorners:(UIView *)view;
+ (void) roundCornersForView:(UIView *)view;

+ (void) showInformativeAlertViewWithTitle:(NSString *)title andText:(NSString *)text;
+ (void) showInternetNotReachableAlert;
+ (void) showInvalidProductAlert;
+ (void) showServerErrorAlert;
+ (void) showMissingShippingAlert;
+ (void) showUserAlreadyExistsAlert;

@end
