//
//  BSNumericValidator.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSNumericValidator.h"

@implementation BSNumericValidator

- (BOOL)execute
{
    if(!self.inputString || !self.inputString.length){
        return NO;
    }
    //This is the string that is going to be compared to the input string
    NSString *testString = [NSString string];
    
    NSScanner *scanner = [NSScanner scannerWithString:self.inputString];
    
    //This is the character set containing all digits. It is used to filter the input string
    NSCharacterSet *skips = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    
    //This goes through the input string and puts all the
    //characters that are digits into the new string
    [scanner scanCharactersFromSet:skips intoString:&testString];
    
    return ([self.inputString length] == [testString length]);
}

@end
