//
//  BSTransitionDelegate.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSTransitionDelegate.h"
#import "BSAnimatedTransitioning.h"

@implementation BSTransitionDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    BSAnimatedTransitioning *controller = [BSAnimatedTransitioning new];
    return controller;
}

@end
