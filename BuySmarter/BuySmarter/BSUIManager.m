//
//  BSUIManager.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSUIManager.h"
#import "BSConstants.h"

static const float BSSideInset = 5.0;
static const float BSCornerRadius = 7.0;
static const float BSBorderWidth = 2.0;

@implementation BSUIManager

#pragma mark - configurations

+ (void) customizeButton:(UIButton *) button withResizableImageNamed:(NSString *)imageName
{
    UIImage *img = [[UIImage imageNamed:imageName] resizableImageWithCapInsets:UIEdgeInsetsMake(0, BSSideInset, 0, BSSideInset)];
    [button setBackgroundImage:img forState:UIControlStateNormal];
}

+ (void) roundCornersForView:(UIView *)view
{
    [view.layer setCornerRadius:BSCornerRadius];
    [view.layer setMasksToBounds:YES];
}

+ (void) configureViewWithBorderShadowAndRoundedCorners:(UIView *)view
{
    view.layer.borderColor = BSCOLOR.CGColor;
    view.layer.borderWidth = BSBorderWidth;
    [view.layer setCornerRadius:BSCornerRadius];
    [view.layer setMasksToBounds:YES];
	[view.layer setShadowColor:[UIColor blackColor].CGColor];
	[view.layer setShadowOpacity:BSShadowOpacity];
	[view.layer setShadowOffset:CGSizeMake(BSShadowOffset, BSShadowOffset)];
}

#pragma mark - alerts

+ (void) showInformativeAlertViewWithTitle:(NSString *)title andText:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert show];
}

+ (void) showInternetNotReachableAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_error_title") message:LOCALIZED_STRING(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert show];
}

+ (void) showInvalidProductAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_error_title")  message:LOCALIZED_STRING(@"alert_invalid_qr_code") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert show];
}

+ (void) showServerErrorAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_error_title") message:LOCALIZED_STRING(@"alert_server_error") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert show];
}

+ (void) showMissingShippingAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_error_title") message:LOCALIZED_STRING(@"alert_no_shipping_details") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert show];
}

+ (void) showUserAlreadyExistsAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZED_STRING(@"alert_error_title") message:LOCALIZED_STRING(@"alert_user_already_exists") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert show];
}

@end
