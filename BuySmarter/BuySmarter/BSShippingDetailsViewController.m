//
//  BSShippingDetailsViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSShippingDetailsViewController.h"
#import "BSTextField.h"
#import "BSValidationManager.h"
#import "BSNumericValidator.h"
#import "BSPhoneValidator.h"
#import "BSAddressValidator.h"
#import "BSAlphaValidator.h"
#import "BSConstants.h"
#import "BSUIManager.h"

@interface BSShippingDetailsViewController ()
@property (weak, nonatomic) IBOutlet BSTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet BSTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet BSTextField *addressLineTextField;
@property (weak, nonatomic) IBOutlet BSTextField *postalCodeTextField;
@property (weak, nonatomic) IBOutlet BSTextField *cityTextField;
@property (weak, nonatomic) IBOutlet BSTextField *phonenumberTextField;
@property (nonatomic) BSValidationManager *validationManager;
@end

@implementation BSShippingDetailsViewController

#pragma mark - ViewController lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    [self configureLocalization];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configureLocalization) name:BSLanguageChangedNotification object:nil];
}

- (void) configureView
{
    _firstNameTextField.validationIdentifier = kvBSFirstNameIdentifier;
    _lastNameTextField.validationIdentifier = kvBSLastNameIdentifier;
    _phonenumberTextField.validationIdentifier = kvBSPhoneNumberIdentifier;
    _addressLineTextField.validationIdentifier = kvBSAddressLineIdentifier;
    _postalCodeTextField.validationIdentifier = kvBSPostalCodeIdentifier;
    _cityTextField.validationIdentifier = kvBSCityIdentifier;
}

- (void) configureLocalization
{
    _firstNameTextField.placeholder = LOCALIZED_STRING(@"first_name_field_placeholder");
    _lastNameTextField.placeholder = LOCALIZED_STRING(@"last_name_field_placeholder");
    _phonenumberTextField.placeholder = LOCALIZED_STRING(@"phone_number_field_placeholder");
    _addressLineTextField.placeholder = LOCALIZED_STRING(@"address_line_field_placeholder");
    _postalCodeTextField.placeholder = LOCALIZED_STRING(@"postal_code_field_placeholder");
    _cityTextField.placeholder = LOCALIZED_STRING(@"city_field_placeholder");
}

#pragma mark - accessor methods

- (BSValidationManager *)validationManager
{
    if(!_validationManager){
        _validationManager = [BSValidationManager new];
        [_validationManager registerClass:[BSAlphaValidator class] withIdentifier:kvBSFirstNameIdentifier andErrorMessage:LOCALIZED_STRING(@"alert_invalid_first_name")];
        [_validationManager registerClass:[BSAlphaValidator class] withIdentifier:kvBSLastNameIdentifier andErrorMessage:LOCALIZED_STRING(@"alert_invalid_last_name")];
        [_validationManager registerClass:[BSNumericValidator class] withIdentifier:kvBSPhoneNumberIdentifier andErrorMessage:@"alert_invalid_phone_number"];
        [_validationManager registerClass:[BSAddressValidator class] withIdentifier:kvBSAddressLineIdentifier andErrorMessage:@"alert_invalid_address_line"];
        [_validationManager registerClass:[BSNumericValidator class] withIdentifier:kvBSPostalCodeIdentifier andErrorMessage:@"alert_invalid_postal_code"];
        [_validationManager registerClass:[BSAlphaValidator class] withIdentifier:kvBSCityIdentifier andErrorMessage:@"alert_invalid_city"];
    }
    return _validationManager;
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.firstNameTextField]){
        [self.lastNameTextField becomeFirstResponder];
    }else if([textField isEqual:self.lastNameTextField]){
        [self.phonenumberTextField becomeFirstResponder];
    }else if([textField isEqual:self.addressLineTextField]){
        [self.postalCodeTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(BSTextField *)textField
{
    NSString *error;
    if(textField.text.length > 0){
        [self.validationManager validateInput:textField.text forIdentifier:textField.validationIdentifier error:&error];
        if(error){
            [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_warning_title") andText:LOCALIZED_STRING(error)];
        }
    }
    return YES;
}

#pragma mark - validation

- (void) hideKeyboard
{
    [_firstNameTextField resignFirstResponder];
    [_lastNameTextField resignFirstResponder];
    [_phonenumberTextField resignFirstResponder];
    [_postalCodeTextField resignFirstResponder];
    [_addressLineTextField resignFirstResponder];
    [_cityTextField resignFirstResponder];
}

- (NSDictionary *)getValidInputs
{
    [self.validationManager registerInput:self.firstNameTextField.text withValidatorIdentifier:self.firstNameTextField.validationIdentifier];
    [self.validationManager registerInput:self.lastNameTextField.text withValidatorIdentifier:self.lastNameTextField.validationIdentifier];
    [self.validationManager registerInput:self.phonenumberTextField.text withValidatorIdentifier:self.phonenumberTextField.validationIdentifier];
    [self.validationManager registerInput:self.addressLineTextField.text withValidatorIdentifier:self.addressLineTextField.validationIdentifier];
    [self.validationManager registerInput:self.postalCodeTextField.text withValidatorIdentifier:self.postalCodeTextField.validationIdentifier];
    [self.validationManager registerInput:self.cityTextField.text withValidatorIdentifier:self.cityTextField.validationIdentifier];
    
    NSString *error;
    [self.validationManager validateInputs:&error];
    if(error){
        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:LOCALIZED_STRING(error)];
        return nil;
    }else{
        return @{kBSNFirstName : self.firstNameTextField.text, kBSNLastName : self.lastNameTextField.text, kBSNPhoneNumber : self.phonenumberTextField.text, kBSNAddressLine: self.addressLineTextField.text, kBSNPostalCode: self.postalCodeTextField.text, kBSNCity : self.cityTextField.text};
    }
}

- (void)setEditable:(BOOL)editable
{
    [self.view setUserInteractionEnabled:editable];
}

#pragma mark - Configuration

- (void)populateFieldsWithUser:(UserInfo *)user
{
    self.firstNameTextField.text = user.firstName;
    self.lastNameTextField.text = user.lastName;
    self.phonenumberTextField.text = user.phoneNumber;
    self.addressLineTextField.text = user.addressLine;
    self.postalCodeTextField.text = user.postalCode;
    self.cityTextField.text = user.city;
}

@end
