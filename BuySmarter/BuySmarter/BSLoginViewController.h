//
//  BSLoginViewController.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/21/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSAuthDelegate.h"
#import "BSBaseInputViewController.h"

@interface BSLoginViewController : BSBaseInputViewController <UITextFieldDelegate>
@property (nonatomic, weak) id<BSAuthDelegate> delegate;
/**
 *  Gets the input email
 *
 *  @return     input email
 */
- (NSString *)email;

@end
