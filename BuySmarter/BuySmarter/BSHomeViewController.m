//
//  BSHomeViewController.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/22/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSHomeViewController.h"
#import "UIImageView+BSAsync.h"
#import "BSImageLoader.h"
#import "BSProductCell.h"
#import "BSConnectionManager.h"
#import "BSTransitionDelegate.h"
#import "BSNetworkingAdapter.h"
#import "BSCoreDataAdapter.h"
#import "BSUIManager.h"
#import "BSConstants.h"
#import "Product.h"
#import "BSIndicatorView.h"
#import "Vendor.h"
#import "BSProductDetailsViewController.h"
#import "BSSessionManager.h"
#import "BSCheckoutCell.h"
#import "BSVendorHeaderView.h"
#import "BSCheckoutViewController.h"
#import "UIViewController+BSAuth.h"
#import "BSLocalizationManager.h"

static NSString * const kPaypalResponse = @"response";
static NSString * const kPaypalCreateTime = @"create_time";

@interface BSHomeViewController (){
    NSIndexPath *_latestSelectedIndex;
    NSMutableArray *_availabilityCheckArray;
    BOOL _isRefreshing;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray *shoppingCartArray;
@property (nonatomic) NSMutableArray *wishListArray;
@property (nonatomic) NSMutableArray *objectsToRemove;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) NSTimer *vendorsFetchTimer;
@property (nonatomic) NSDictionary *dataSourceDictionary;
@end

@implementation BSHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView addSubview:self.refreshControl];
    [self configureLocalization];
    [self refreshAndFetchDataIfNeeded];
 }

- (void) configureLocalization
{
    self.title = LOCALIZED_STRING(@"home_screen_title");
    [self.segmentedControl setTitle:LOCALIZED_STRING(@"shopping_cart_segment_title") forSegmentAtIndex:0];
    [self.segmentedControl setTitle:LOCALIZED_STRING(@"wish_list_segment_title") forSegmentAtIndex:1];
}

- (void) reloadData
{
    [self reloadDataSource];
    [_tableView reloadData];
}

-  (void)refreshAndFetchDataIfNeeded{
    if(![[BSCoreDataAdapter sharedInstance] fetchedVendorsArray].count || !USER_INFO.productsListsRequested.boolValue){
        if(INTERNET_REACHABLE){
            [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_loading")];
            if(![[BSCoreDataAdapter sharedInstance] fetchedVendorsArray].count){
                [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGETVendorsList withAdditionalData:nil completionBlock:^(NSNumber * responseCode, NSDictionary *responseDict, NSString *error){
                    if(error){
                        [BSIndicatorView hide];
                        [self startVendorsFetchTimer];
                        [BSUIManager showServerErrorAlert];
                    }else if(responseCode.integerValue != 200){
                        // !OK
                        [BSIndicatorView hide];
                        if(responseCode.integerValue == 405){
                            [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                        }else{
                            [BSUIManager showServerErrorAlert];
                        }
                    }else{
                        // 200 OK
                        [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDict forOperationType:BSNetworkOperationTypeGETVendorsList];
                        if(!USER_INFO.productsListsRequested.boolValue){
                            [self fetchAndRefreshListsAndCurrenciesIfNeeded];
                        }else{
                            if([self shouldRequestCurrencies]){
                                [self requestCurrenciesWithCompletionBlock:^(NSString * errr){
                                    [BSIndicatorView hide];
                                    if(errr){
                                        [BSUIManager showServerErrorAlert];
                                    }else{
                                        [self refreshDataSource];
                                        [self reloadData];
                                    }
                                }];
                            }else{
                                [BSIndicatorView hide];
                                [self refreshDataSource];
                                [self reloadData];
                            }
                        }
                    }
                }];
            }else{
                if(!USER_INFO.productsListsRequested.boolValue)
                {
                    [self fetchAndRefreshListsAndCurrenciesIfNeeded];
                }else if([self shouldRequestCurrencies]){
                    [self requestCurrenciesWithCompletionBlock:^(NSString * errr){
                        [BSIndicatorView hide];
                        if(errr){
                            [BSUIManager showServerErrorAlert];
                        }else{
                            [self refreshDataSource];
                            [self reloadData];
                        }
                    }];
                }
            }
        }else{
            [BSUIManager showInternetNotReachableAlert];
            [self startVendorsFetchTimer];
        }
    }else{
        if([self shouldRequestCurrencies]){
            if(INTERNET_REACHABLE){
                [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_updating_currency")];
                [self requestCurrenciesWithCompletionBlock:^(NSString *err){
                    [BSIndicatorView hide];
                    if(err){
                        [BSUIManager showServerErrorAlert];
                    }
                    [self refreshDataSource];
                    [self reloadData];
                }];
            }else{
                [BSUIManager showInternetNotReachableAlert];
            }
        }else{
            [self refreshDataSource];
            [self reloadData];
        }
    }
}

- (void) fetchAndRefreshListsAndCurrenciesIfNeeded
{
    [self requestAllProductsWithCompletionBlock:^(NSString *error){
        if(!error){
            [self refreshDataSource];
            if([self shouldRequestCurrencies]){
                [self requestCurrenciesWithCompletionBlock:^(NSString *error){
                    [BSIndicatorView hide];
                    if(!error){
                        [self reloadData];
                    }else{
                        [BSUIManager showServerErrorAlert];
                    }
                }];
            }
        }else{
            [BSIndicatorView hide];
            [BSUIManager showServerErrorAlert];
        }
    }];
    
}
#pragma mark - Currency Rates Update

- (BOOL) shouldRequestCurrencies{
    NSDate *expirationDate = USER_INFO.currencyRatesDictionary[BSCurrencyExpirationKey];
    if([expirationDate timeIntervalSinceNow] <= 0){
        NSLog(@"Should reload the currency");
        return YES;
    }else{
        return NO;
    }
}

- (void) requestCurrenciesWithCompletionBlock:(void(^)(NSString *))completionBlock
{
    if(INTERNET_REACHABLE){
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGetCurrencyRates withAdditionalData:nil completionBlock:^(NSNumber *statusCode, NSDictionary *dict, NSString *err){
            if(!err){
                if(statusCode.integerValue == 200){
                    [[BSCoreDataAdapter sharedInstance] parseServerResponse:dict forOperationType:BSNetworkOperationTypeGetCurrencyRates];
                    completionBlock(nil);
                }else{
                    completionBlock(@"Server error");
                }
            }else{
                completionBlock(err);
            }
        }];
    }else{
        completionBlock(@"No internet connection!");
    }
}


#pragma mark - Vendors fetch

- (void) startVendorsFetchTimer
{
    self.vendorsFetchTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(vendorsTimerTick) userInfo:nil repeats:YES];
}
- (void) vendorsTimerTick
{
    if(INTERNET_REACHABLE){
        [self.vendorsFetchTimer invalidate];
        self.vendorsFetchTimer = nil;
        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGETVendorsList withAdditionalData:nil completionBlock:^(NSNumber * responseCode, NSDictionary *responseDict, NSString *error){
            if(responseCode.integerValue == 200){
                [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDict forOperationType:BSNetworkOperationTypeGETVendorsList];
            }else{
                [self startVendorsFetchTimer];
            }
        }];
    }
}

#pragma mark - Products fetch

- (void) requestAllProductsWithCompletionBlock:(void(^)(NSString *))completionBlock{
    [self getProductsForCategory:BSProductCategoryShoppingCart completionBlock:^(NSArray *shoppingCart, NSString *error){
        if(error){
            completionBlock(error);
        }else{
            [self getProductsForCategory:BSProductCategoryWishList completionBlock:^(NSArray *wishList, NSString *err){
                if(error){
                    completionBlock(error);
                }else{
                    completionBlock(nil);
                }
            }];
        }
    }];
}

- (void) getProductsForCategory:(BSProductCategory)category completionBlock:(void (^) (NSArray *, NSString *))completionBlock
{
    BSNetworkOperationType operationType = category == BSProductCategoryShoppingCart? BSNetworkOperationTypeGETShoppingCart : BSNetworkOperationTypeGETWishlist;
    
    [[BSNetworkingAdapter sharedInstance] performOperation:operationType withAdditionalData:nil completionBlock:^(NSNumber *responseCode, NSDictionary *responseDict, NSString *error){
        if(responseCode.integerValue == 200){
            if(responseDict){
                // 200 OK
                [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDict forOperationType:operationType];
                completionBlock ([[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:category], nil);
            }else{
                NSAssert(nil, @"How can the dictionary be nil!?");
            }
        }else{
            if(responseCode.integerValue == 405){
                [BSIndicatorView hide];
                [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
            }else{
                completionBlock(nil, @"Error while fetching products. Please try again later.");
            }
        }
    }];
}

#pragma mark - Callbacks

- (IBAction)scanAction {
    if([[BSConnectionManager sharedInstance] internetConnectionAvailable]){
        [self performSegueWithIdentifier:@"showProductScanner" sender:self];
    }else{
        [BSUIManager showInternetNotReachableAlert];
    }
}

- (IBAction)segmentValueChanged:(UISegmentedControl *)sender {
    [self reloadData];
}

#pragma mark - Refresh products' stocks

- (void) configureArrayForAvailabilityCheck
{
    _availabilityCheckArray = [@[] mutableCopy];
    NSMutableArray *productsArray;
    NSDictionary *dataSourceDictionary = [self dataSourceDictionary];
    for(int i = 0; i < [dataSourceDictionary[kBSNVendorsKey] count]; i++)
    {
        productsArray = [@[] mutableCopy];
        for(int j = 0; j < [dataSourceDictionary[kBSNProductsKey][i] count]; j++){
            [productsArray addObject:[dataSourceDictionary[kBSNProductsKey][i][j] productIdentifier]];
        }
        [_availabilityCheckArray addObject:@{kBSNVendorIdKey : [dataSourceDictionary[kBSNVendorsKey][i] vendorIdentifier], kBSNProductsKey : productsArray}];
    }
}

- (void) refreshProductsWithDictionary:(NSDictionary *)dict
{
    [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeCheckProductsAvailability withAdditionalData:dict completionBlock:^(NSNumber *statusCode, NSDictionary *responseDictionary, NSString *error){
        [_availabilityCheckArray removeObject:dict];
        if(error){
            [BSUIManager showServerErrorAlert];
            [self.refreshControl endRefreshing];
        }else if(statusCode.integerValue == 200){
            // 200 OK
            _isRefreshing = NO;
            [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDictionary forOperationType:BSNetworkOperationTypeCheckProductsAvailability];
            if(_availabilityCheckArray.count == 0){
                // all request have finished
                [self refreshDataSource];
                [self.refreshControl endRefreshing];
            }else{
                [self refreshProductsWithDictionary:_availabilityCheckArray.lastObject];
            }
        }else{
            if(statusCode.integerValue == 405){
                [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
            }else{
                [BSUIManager showServerErrorAlert];
                [self.refreshControl endRefreshing];
            }
        }
    }];
}

- (void) refreshAction
{
    if(INTERNET_REACHABLE){
        [self configureArrayForAvailabilityCheck];
        _isRefreshing = YES;
        if(_availabilityCheckArray.count){
            [self refreshProductsWithDictionary:_availabilityCheckArray.lastObject];
        }else{
            [self requestAllProductsWithCompletionBlock:^(NSString *error){
                _isRefreshing = NO;
                if(!error){
                    [self refreshDataSource];
                    [self reloadData];
                }else{
                    [BSUIManager showServerErrorAlert];
                }
                [self.refreshControl endRefreshing];
            }];
        }
    }else{
        [BSUIManager showInternetNotReachableAlert];
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - accessor methods

- (NSDictionary *)dataSourceDictionary
{
    if(!_dataSourceDictionary){
        _dataSourceDictionary = @{kBSNVendorsKey : [@[] mutableCopy], kBSNProductsKey : [@[] mutableCopy]};
    }
    return _dataSourceDictionary;
}

- (UIRefreshControl *)refreshControl
{
    if(!_refreshControl){
        _refreshControl = [UIRefreshControl new];
        [_refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
    }
    return _refreshControl;
}

- (NSMutableArray *)objectsToRemove
{
    if(!_objectsToRemove){
        _objectsToRemove = [@[] mutableCopy];
    }
    return _objectsToRemove;
}
- (NSMutableArray *)shoppingCartArray
{
    if(!_shoppingCartArray){
        _shoppingCartArray = [@[] mutableCopy];
    }
    return _shoppingCartArray;
}

- (NSMutableArray *)wishListArray
{
    if(!_wishListArray){
        _wishListArray = [@[] mutableCopy];
    }
    return _wishListArray;
}

- (void)reloadDataSource
{
    BOOL found;
    _dataSourceDictionary =  @{kBSNVendorsKey : [@[] mutableCopy], kBSNProductsKey : [@[] mutableCopy]};
    for(Product *p in self.segmentedControl.selectedSegmentIndex == 0 ? self.shoppingCartArray : self.wishListArray){
        found = NO;
        for(int i = 0; i < [(NSArray *)_dataSourceDictionary[kBSNVendorsKey] count]; i ++){
            if([[_dataSourceDictionary[kBSNVendorsKey][i] vendorIdentifier] isEqual:p.vendorIdentifier]){
                [_dataSourceDictionary[kBSNProductsKey][i] addObject:p];
                found = YES;
                break;
            }
        }
        if(!found){
            [_dataSourceDictionary[kBSNVendorsKey] addObject:[[BSCoreDataAdapter sharedInstance] vendorForIdentifier:p.vendorIdentifier]];
            [_dataSourceDictionary[kBSNProductsKey] addObject:[@[p] mutableCopy]];
        }
    }
}
#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}


#pragma mark - Helper methods

- (void)loadImagesForOnscreenRows
{
    if ([(NSArray *)self.dataSourceDictionary[kBSNProductsKey] count] > 0)
    {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if(indexPath.row < [self.dataSourceDictionary[kBSNProductsKey][indexPath.section] count]){
                if (![[BSImageLoader sharedInstance] cachedImageFromURL:[self.dataSourceDictionary[kBSNProductsKey][indexPath.section][indexPath.row] imageURLsArray][0]]){
                    [self startIconDownloadForIndexPath:indexPath andCategory:_segmentedControl.selectedSegmentIndex + 1];
                }
            }
        }
    }
}

- (void) startIconDownloadForIndexPath:(NSIndexPath *)indexPath andCategory:(BSProductCategory)category
{
    Product *product = self.dataSourceDictionary[kBSNProductsKey][indexPath.section][indexPath.row];
    if([(NSArray *)product.imageURLsArray count] > 0){
        [[BSImageLoader sharedInstance] getImageWithURL:product.imageURLsArray[0] withCompletionBlock:^(UIImage *image, NSString *error){
            if(!error && _segmentedControl.selectedSegmentIndex == category - 1){
                BSProductCell *productCell = (BSProductCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                [productCell.productImageView setImage:image animated:YES];
            }else{
                // NSLog(@"ERROR: %@", error);
            }
        }];
    }
}


#pragma mark - UITableView methods


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return BSVendorHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [BSVendorHeaderView headerViewForVendor:self.dataSourceDictionary[kBSNVendorsKey][section]];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.dataSourceDictionary[kBSNVendorsKey][section] vendorName];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.segmentedControl.selectedSegmentIndex == BSProductCategoryShoppingCart - 1){
        if(indexPath.row < [self.dataSourceDictionary[kBSNProductsKey][indexPath.section] count]){
            return kBSProductCellHeight;
        }else{
            return kBSCheckoutCellHeight;
        }
    }else{
        return kBSProductCellHeight;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [(NSArray *)self.dataSourceDictionary[kBSNVendorsKey] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.segmentedControl.selectedSegmentIndex == BSProductCategoryShoppingCart - 1){
        __block NSMutableArray *sectionsArray = [@[] mutableCopy];
        __block BOOL outOfStock = YES;
        [self.dataSourceDictionary[kBSNProductsKey] enumerateObjectsUsingBlock:^(id array, NSUInteger index, BOOL *stop){
            outOfStock = YES;
            for(Product *p in array){
                if(p.productStock.integerValue > 0){
                    outOfStock = NO;
                    break;
                }
            }
            if(outOfStock){
                [sectionsArray addObject:@(index)];
            }
        }];
        if([sectionsArray containsObject:@(section)]){
            return [(NSArray *)self.dataSourceDictionary[kBSNProductsKey][section] count];
        }else{
            return [(NSArray *)self.dataSourceDictionary[kBSNProductsKey][section] count] + 1;
        }
    }else{
        return [(NSArray *)self.dataSourceDictionary[kBSNProductsKey][section] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [self.dataSourceDictionary[kBSNProductsKey][indexPath.section] count]){
        BSProductCell *cell = [tableView dequeueReusableCellWithIdentifier:BSProductCellIdentifier];
        
        Product *product = self.dataSourceDictionary[kBSNProductsKey][indexPath.section][indexPath.row];
        
        if(![[BSImageLoader sharedInstance] cachedImageFromURL:product.imageURLsArray[0]]){
            cell.productImageView.image = [UIImage imageNamed:@"Placeholder"];
            if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
            {
                [self startIconDownloadForIndexPath:indexPath andCategory:_segmentedControl.selectedSegmentIndex + 1];
            }
        }else{
            cell.productImageView.image = [[BSImageLoader sharedInstance] cachedImageFromURL:product.imageURLsArray[0]];
        }
        [cell configureWithProduct:product andVendor:self.dataSourceDictionary[kBSNVendorsKey][indexPath.section]];
        return cell;
    }else{
        BSCheckoutCell *cell = [tableView dequeueReusableCellWithIdentifier:BSCheckoutCellIdentifier];
        [cell configureWithVendor:self.dataSourceDictionary[kBSNVendorsKey][indexPath.section] andProductsArray:self.dataSourceDictionary[kBSNProductsKey][indexPath.section]];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _latestSelectedIndex = indexPath;
    if(indexPath.row < [self.dataSourceDictionary[kBSNProductsKey][indexPath.section] count]){
        [self performSegueWithIdentifier:@"showProductDetails" sender:self];
    }else{
        if(USER_INFO.addressLine){
            [self performSegueWithIdentifier:@"showCheckout" sender:self];
        }else{
            if(!USER_INFO.shippingRequested.boolValue){
                if(INTERNET_REACHABLE){
                    [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_updating_shipping")];
                    [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeGETShippingDetails withAdditionalData:nil completionBlock:^(NSNumber *statusCode, NSDictionary *responseData, NSString *error){
                        [BSIndicatorView hide];
                        if(error){
                            [BSUIManager showServerErrorAlert];
                        }else{
                            if(statusCode.integerValue == 200){
                                [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseData forOperationType:BSNetworkOperationTypeGETShippingDetails];
                                if(USER_INFO.addressLine){
                                    if([self shouldRequestCurrencies]){
                                        [BSIndicatorView hide];
                                        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_updating_currency")];
                                        [self requestCurrenciesWithCompletionBlock:^(NSString *err){
                                            if(!err){
                                                [BSIndicatorView hide];
                                                [self performSegueWithIdentifier:@"showCheckout" sender:self];
                                            }else{
                                                [BSUIManager showServerErrorAlert];
                                            }
                                        }];
                                    }else{
                                        [BSIndicatorView hide];
                                        [self performSegueWithIdentifier:@"showCheckout" sender:self];
                                    }
                                }else{
                                    [BSUIManager showMissingShippingAlert];
                                }
                            }else if(statusCode.integerValue == 405){
                                [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                            }else{
                                [BSUIManager showServerErrorAlert];
                            }
                        }
                    }];
                }else{
                    [BSUIManager showInternetNotReachableAlert];
                }
            }else{
                [BSUIManager showMissingShippingAlert];
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete && INTERNET_REACHABLE && !_isRefreshing) {
        Product *prod = self.dataSourceDictionary[kBSNProductsKey][indexPath.section][indexPath.row];
        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_processing")];
        [self discardProduct:prod withCompletionBlock:^(NSString *error, BOOL authError){
            [BSIndicatorView hide];
            if(authError){
                [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
            }else if(error){
                [BSUIManager showServerErrorAlert];
            }else{
                /**
                 *  1 Product row + 1 Checkout row
                 */
                
                if(self.segmentedControl.selectedSegmentIndex == 0){
                    //shopping cart
                    [self.shoppingCartArray removeObject:prod];
                }else{
                    //wish list
                    [self.wishListArray removeObject:prod];
                }
                
                [[BSCoreDataAdapter sharedInstance] deleteObject:prod];
                [self reloadData];
            }
        }];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return LOCALIZED_STRING(@"cell_delete_button_title");
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [self.dataSourceDictionary[kBSNProductsKey][indexPath.section] count] && INTERNET_REACHABLE){
        return UITableViewCellEditingStyleDelete;
    }else{
        return UITableViewCellEditingStyleNone;
    }
}

- (void) discardProduct:(Product*) product withCompletionBlock:(void(^)(NSString *error, BOOL authError))completionBlock
{
    [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeDiscardProduct withAdditionalData:@{kBSNIDKey : product.databaseId} completionBlock:^(NSNumber *statusCode, NSDictionary *responseDict, NSString *error){
        if(!error){
            if(statusCode.integerValue == 200){
                completionBlock(nil, NO);
            }else{
                if(statusCode.integerValue == 405){
                    completionBlock(nil, YES);
                }else{
                    completionBlock(@"Server error", NO);
                }
            }
        }else{
            completionBlock(error, NO);
        }
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showProductScanner"]){
        BSScanViewController *viewController = [segue.destinationViewController childViewControllers][0];
        viewController.delegate = self;
    }else if([segue.identifier isEqualToString:@"showProductDetails"]){
        BSProductDetailsViewController *controller = segue.destinationViewController;
        controller.product = self.dataSourceDictionary[kBSNProductsKey][_latestSelectedIndex.section][_latestSelectedIndex.row];
        controller.delegate = self;
    }else if([segue.identifier isEqualToString:@"showCheckout"]){
        BSCheckoutViewController *checkoutViewController = segue.destinationViewController;
        checkoutViewController.delegate = self;
        Vendor *vendor = self.dataSourceDictionary[kBSNVendorsKey][_latestSelectedIndex.section];
        checkoutViewController.vendor = vendor;
        checkoutViewController.productsArray = self.dataSourceDictionary[kBSNProductsKey][_latestSelectedIndex.section];
    }
}

#pragma mark - BSScannerDelegate methods

- (void)scannerViewControllerDidFinishWithDictionary:(NSDictionary *)dictionary authenticationError:(BOOL)authErr
{
    [self dismissViewControllerAnimated:YES completion:^{
        if(authErr){
            [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
        }else if(dictionary){
            
            if([self shouldAddProductWithDictionary:dictionary]){
                if(INTERNET_REACHABLE){
                    BSProductCategory category = [dictionary[kBSNProductCategory] isEqualToString:kBSNProductCategoryShoppingCart] ? BSProductCategoryShoppingCart : BSProductCategoryWishList;
                    // -1 because 0 -> BSProductCategoryNone
                    self.segmentedControl.selectedSegmentIndex = category - 1;
                    [self reloadData];
                    BSProductCategory existingCategory = [self categoryForAlreadyStoredProduct:dictionary];
                    if(existingCategory != BSProductCategoryNone){
                        [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_info_title") andText:[NSString stringWithFormat:LOCALIZED_STRING(@"alert_product_already_stored_error"), existingCategory == BSProductCategoryShoppingCart?LOCALIZED_STRING(@"your_shopping_cart"):LOCALIZED_STRING(@"your_wish_list")]];
                    }else{
                        [BSIndicatorView showWithText:LOCALIZED_STRING(@"indicator_view_adding_product")];
                        [[BSNetworkingAdapter sharedInstance] performOperation:BSNetworkOperationTypeAddProduct withAdditionalData:dictionary completionBlock:^(NSNumber *responseCode, NSDictionary *responseDict, NSString *error){
                            if(error){
                                [BSIndicatorView hide];
                                [BSUIManager showServerErrorAlert];
                            }else{
                                if(responseCode.integerValue == 200){
                                    // 200 OK
                                    [[BSCoreDataAdapter sharedInstance] parseServerResponse:responseDict forOperationType:BSNetworkOperationTypeAddProduct];
                                    NSArray *products =  [[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:category];
                                    if(category == BSProductCategoryShoppingCart){
                                        _shoppingCartArray = [products mutableCopy];
                                    }else{
                                        _wishListArray = [products mutableCopy];
                                    }
                                    [self reloadData];
                                    [BSIndicatorView hide];
                                }else{
                                    [BSIndicatorView hide];
                                    if(responseCode.integerValue == 405){
                                        [self logOutUserWithMessage:LOCALIZED_STRING(@"alert_logout_message")];
                                    }else{
                                        [BSUIManager showServerErrorAlert];
                                    }
                                }
                            }
                        }];
                    }
                }else{
                    [BSUIManager showInternetNotReachableAlert];
                }
            }else{
                [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_error_title") andText:[NSString stringWithFormat:LOCALIZED_STRING(@"alert_too_many_products"), @(BSListProductLimit)]];
            }
        }
    }];
}

#pragma mark - Helper methods

- (BSProductCategory) categoryForAlreadyStoredProduct:(NSDictionary *)dictionary
{
    for(Product *p in _wishListArray){
        if([p.productIdentifier isEqual:dictionary[kBSNProductIdentifier]] && [p.vendorIdentifier isEqual:dictionary[kBSNVendorIdKey]]){
            return BSProductCategoryWishList;
        }
    }
    for(Product *p in _shoppingCartArray){
        if([p.productIdentifier isEqual:dictionary[kBSNProductIdentifier]] && [p.vendorIdentifier isEqual:dictionary[kBSNVendorIdKey]]){
            return BSProductCategoryShoppingCart;
        }
    }
    return BSProductCategoryNone;
}

- (BOOL) shouldAddProductWithDictionary:(NSDictionary *)dictionary{
    NSString *vendorId = dictionary[kBSNVendorIdKey];
    NSInteger count = 0;
    NSString *category = dictionary[kBSNProductCategory];
    
    id array = [category isEqualToString:kBSNProductCategoryShoppingCart] ? _shoppingCartArray : _wishListArray;
    
    for(Product *p in array){
        if([p.vendorIdentifier isEqualToString:vendorId]){
            count ++;
        }
    }
    
    if(count < BSListProductLimit){
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - BSProductDetailsViewControllerDelegate method

- (void)refreshDataSource
{
    self.shoppingCartArray = [[[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:BSProductCategoryShoppingCart] mutableCopy];
    self.wishListArray = [[[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:BSProductCategoryWishList] mutableCopy];
    [self reloadData];
}

- (void)changeToCategory:(BSProductCategory)category
{
    NSAssert(category != BSProductCategoryNone, @"Invalid category");
    self.segmentedControl.selectedSegmentIndex = category - 1;
    [self reloadData];
}

#pragma mark - BSCheckoutViewControllerDelegate methods

-(void)checkoutViewControllerDidAlterDataSource
{
    [self refreshDataSource];
    [self reloadData];
}

- (void)checkoutViewControllerDidFinishWithDictionary:(NSDictionary *)dictionary
{
    BOOL errorOccured = [dictionary[kBSCheckoutErrorKey] boolValue];
    NSArray *reservedProducts = dictionary[kBSCheckoutReservedProductsKey];
    NSDictionary *paymentConfirmation = dictionary[kBSCheckoutConfirmationKey];
    
    if(errorOccured){
        if(paymentConfirmation){
            /**
             *   Send an email to the vendor
             */
            [self.navigationController popToRootViewControllerAnimated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                Vendor *v = [[BSCoreDataAdapter sharedInstance] vendorForIdentifier:[reservedProducts[0] vendorIdentifier]];
                MFMailComposeViewController *composer = [MFMailComposeViewController new];
                composer.navigationBar.tintColor = BSCOLOR;
                [composer setMailComposeDelegate:self];
                if([MFMailComposeViewController canSendMail]){
                    [composer setToRecipients:@[v.vendorEmail]];
                    [composer setSubject:@"Payment issue"];
                    [composer setMessageBody:[NSString stringWithFormat:@"%@", paymentConfirmation] isHTML:NO];
                    [self presentViewController:composer animated:YES completion:nil];
                }
            });
        }else{
            /**
             *  Some server error occured, do nothing
             */
        }
    }else{
        /**
         *  Success, remove the ordered products from the list
         */
        [[BSCoreDataAdapter sharedInstance] insertOrderWithProducts:reservedProducts andCreationDateString:paymentConfirmation[kPaypalResponse][kPaypalCreateTime]];
        
        _shoppingCartArray = [[[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:BSProductCategoryShoppingCart] mutableCopy];
        _wishListArray = [[[BSCoreDataAdapter sharedInstance] fetchProductsForCategory:BSProductCategoryWishList] mutableCopy];
        [self reloadData];
        // pop to root after the tableview has loaded it's data
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{
        if(error){
            [BSUIManager showServerErrorAlert];
        }else{
            if(result == MFMailComposeResultSent){
                [BSUIManager showInformativeAlertViewWithTitle:LOCALIZED_STRING(@"alert_success_title") andText:LOCALIZED_STRING(@"alert_successfully_sent_email")];
            }
        }
    }];
}

@end
