//
//  BSConstants.h
//  BuySmarter
//
//  Created by Teodor Patraș on 3/19/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#ifndef BuySmarter_BSConstants_h
#define BuySmarter_BSConstants_h

/**User Info**/

#define USER_INFO [BSCoreDataAdapter sharedInstance].storedUserInfo

/**Visual effects*/

static const float BSShadowOpacity = 0.4;
static const float BSShadowOffset = -1.5;
#define BSCOLOR [UIColor colorWithRed:29.0/255.0 green:143.0/255.0 blue:78.0/255.0 alpha:1]

/**Custom types*/

typedef void (^BSStandardCompletionBlock) (NSString *, NSNumber *, NSData *, NSString*);
typedef void (^BSOperationBlock) (NSNumber*, NSDictionary*, NSString*);

/**Validation identifiers*/

static NSString * const kvBSCurrentPasswordIdentifier = @"current_password";
static NSString * const kvBSEmailIdentifier = @"email";
static NSString * const kvBSPasswordIdentifier = @"password";
static NSString * const kvBSRepeatPasswordIdentifier = @"repeatPassword";
static NSString * const kvBSFirstNameIdentifier = @"firstName";
static NSString * const kvBSLastNameIdentifier = @"lastName";
static NSString * const kvBSPhoneNumberIdentifier = @"phoneNumber";
static NSString * const kvBSAddressLineIdentifier = @"addressLine";
static NSString * const kvBSPostalCodeIdentifier = @"postalCode";
static NSString * const kvBSCityIdentifier = @"city";

/**Supported networking operation types*/

typedef NS_ENUM(NSUInteger, BSNetworkOperationType) {
    BSNetworkOperationTypeGETProductDetails,
    BSNetworkOperationTypeGETWishlist,
    BSNetworkOperationTypeGETShoppingCart,
    BSNetworkOperationTypeGETShippingDetails,
    BSNetworkOperationTypeGETOrdersHistory,
    BSNetworkOperationTypeGETVendorsList,
    BSNetworkOperationTypeRegister,
    BSNetworkOperationTypeForgotPassword,
    BSNetworkOperationTypeLogin,
    BSNetworkOperationTypeUpdateShippingDetails,
    BSNetworkOperationTypeUpdatePassword,
    BSNetworkOperationTypeAddProduct,
    BSNetworkOperationTypeDiscardProduct,
    BSNetworkOperationTypeSwitchProduct,
    BSNetworkOperationTypeCheckProductsAvailability,
    BSNetworkOperationTypeBookProducts,
    BSNetworkOperationTypeCancelBooking,
    BSNetworkOperationTypeProcessOrder,
    BSNetworkOperationTypeGetCurrencyRates,
    BSNetworkOperationTypeGETOrdersHistoryPaginated
};

/** Object keys from server */

static NSString * const kBSNPassword = @"password";
static NSString * const kBSNEmail = @"email";
static NSString * const kBSNProductIdentifier = @"identifier";
static NSString * const kBSNVendorIdKey = @"vendorId";
static NSString * const kBSNproductKey = @"product";
static NSString * const kBSNProductCategory = @"category";
static NSString * const kBSNProductCategoryShoppingCart = @"shoppingCart";
static NSString * const kBSNProductCategoryWishList = @"wishList";
static NSString * const kBSNAddressLine = @"addressLine";
static NSString * const kBSNPostalCode = @"postalCode";
static NSString * const kBSNCity = @"city";
static NSString * const kBSNPhoneNumber = @"phoneNumber";
static NSString * const kBSNFirstName = @"firstName";
static NSString * const kBSNLastName = @"lastName";
static NSString * const kBSNOldPassword = @"old-password";
static NSString * const kBSNNewPassword = @"new-password";
static NSString * const kBSNIDKey = @"_id";
static NSString * const kBSNProductsKey = @"products";
static NSString * const kBSNConfirmationKey = @"confirmation";
static NSString * const kBSNMessageKey = @"message";
static NSString * const kBSNProductDescription = @"description";
static NSString * const kBSNProductImageURLs = @"imageUrls";
static NSString * const kBSNProductPageURL = @"pageUrl";
static NSString * const kBSNProductPrice = @"price";
static NSString * const kBSNProductStock = @"stock";
static NSString * const kBSNProductTitle = @"title";
static NSString * const kBSNVendorTitle = @"title";
static NSString * const kBSNVendorIdentifier = @"identifier";
static NSString * const kBSNVendorCurrency = @"currency";
static NSString * const kBSNVendorLogoURL = @"logoURL";
static NSString * const kBSNVendorApiKey = @"apiKey";
static NSString * const kBSNVendorEmail = @"email";
static NSString * const kBSNVendorsKey = @"vendors";
static NSString * const kBSNAccessTokenKey = @"access-token";
static NSString * const kBSNShippingKey = @"shipping";
static NSString * const kBSNCurrencyKey = @"currency";
static NSString * const kBSNTextKey = @"text";
static NSString * const kBSNBookedProductsKey = @"bookedProducts";
static NSString * const kBSNProofOfPaymentKey = @"proof_of_payment";
static NSString * const kBSNAdaptivePayment = @"adaptive_payment";
static NSString * const kBSNOrdersKey = @"orders";
static NSString * const kBSNDateKey = @"date";
static NSString * const kBSNBeginIndex = @"beginIndex";
static NSString * const kBSNEndIndex = @"endIndex";
static NSString * const kBSNTotalCount = @"totalCount";

/* Product Categories */

typedef NS_ENUM(NSUInteger, BSProductCategory){
    BSProductCategoryNone = 0, //for use with order
    BSProductCategoryShoppingCart,
    BSProductCategoryWishList
};

/*Order related */

static NSString * const BSOrderKey = @"order";
static NSString * const BSOrderVendorKey = @"vendor";
static NSString * const BSOrderProductsKey = @"products";

/* Currency related */

static NSString * const BSCurrencyRatesURL = @"http://www.bnr.ro/nbrfxrates.xml";
static NSString * const BSCurrencyDataSetKey = @"DataSet";
static NSString * const BSCurrencyBodyKey = @"Body";
static NSString * const BSCurrencyCubeKey = @"Cube";
static NSString * const BSCurrencyRateKey = @"Rate";

static NSString * const BSEURCurrency = @"EUR";
static NSString * const BSRONCurrency = @"RON";

static NSString * const BSCurrencyExpirationKey = @"expiration";
static NSString * const BSCurrencyRatesKey = @"rates";

/* Misc */

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width


static const NSInteger BSListProductLimit = 5;
static const NSInteger BSOrderHistoryIncrementalStep = 5;

static const CGFloat BSSidePadding = 20.0;
static const CGFloat BSTopBarsHeight = 64.0;

static NSString * const BSSandboxReceiverEmail = @"vendor@buysmarter.com";
static NSString * const BSHideKeyboardNotification = @"hide-keyboard";
static NSString * const BSWillEnterForegroundNotification = @"will-enter-foreground";
static NSString * const BSLanguageChangedNotification = @"languageChanged";

/* Table View Cell related */

static NSString * const BSProductCellIdentifier = @"productCell";
static NSString * const BSCheckoutCellIdentifier = @"checkoutCell";
static NSString * const BSPayPalCellIdentifier = @"payPalCell";

static const CGFloat kBSProductCellHeight = 124.0f;
static const CGFloat kBSCheckoutCellHeight = 62.0f;
static const CGFloat kBSPayPalCellHeight = 90.0f;

#endif
