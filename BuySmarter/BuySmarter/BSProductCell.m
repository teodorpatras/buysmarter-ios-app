//
//  BSProductViewCell.m
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/24/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSProductCell.h"
#import "BSSessionManager.h"
#import "BSCoreDataAdapter.h"

@interface BSProductCell()
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *outOfStockLabel;
@end

@implementation BSProductCell

- (void) configureWithProduct:(Product *)product andVendor:(Vendor *)vendor
{
    [self configureLocalization];
    BOOL outOfStock = NO;
    self.productTitleLabel.text = product.productTitle;
    self.productDescriptionLabel.text = product.productDescription;
    outOfStock = product.productStock.integerValue == 0;
    
    if([USER_INFO.preferredCurrency isEqualToString:vendor.vendorCurrency]){
        self.priceLabel.text = [NSString stringWithFormat:@"%@", product.productPrice];
        self.currencyLabel.text = vendor.vendorCurrency;
    }else{
        self.priceLabel.text = [NSString stringWithFormat:@"%.2f", [[BSSessionManager sharedInstance] convertedAmountFromTotalAmount:[product.productPrice floatValue] withCurrency:vendor.vendorCurrency]];
        self.currencyLabel.text = USER_INFO.preferredCurrency;
    }
    
    [self.outOfStockLabel setHidden:!outOfStock];
    [self.stockLabel setHidden:outOfStock];
    [self.stockValueLabel setHidden:outOfStock];
    
    if(!outOfStock){
        self.stockValueLabel.text = [NSString stringWithFormat:@"%@", product.productStock];
        [self.contentView setAlpha:1];
    }else{
        [self.contentView setAlpha:0.8];
    }
    
}

- (void) configureLocalization
{
    _stockLabel.text = LOCALIZED_STRING(@"stock_label_text");
    _outOfStockLabel.text = LOCALIZED_STRING(@"out_of_stock");
}

- (void) forceOutOfStock
{
    [self.outOfStockLabel setHidden:NO];
    [self.stockLabel setHidden:YES];
    [self.stockValueLabel setHidden:YES];
    
    CGFloat yOrigin = (CGRectGetHeight(self.frame) - CGRectGetHeight(self.currencyLabel.frame))/2;
    CGRect frame = self.currencyLabel.frame;
    frame.origin.y = yOrigin;
    self.currencyLabel.frame = frame;
    frame = self.priceLabel.frame;
    frame.origin.y = yOrigin;
    self.priceLabel.frame = frame;
}

@end
