//
//  BSSlidingmenuViewController.m
//  BuySmarter
//
//  Created by Teodor Patraș on 3/12/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import "BSSlidingMenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BSConstants.h"
#import "BSHomeViewController.h"

static NSString * const BSSlidingMenuBackSegue = @"init_menu_segue";
static NSString * const BSSlidingMenuFrontSegue = @"init_front_segue";
static NSString * const BSSlidingMenuIcon = @"MenuIcon";

static const float BSMenuToFrontViewPercentage = 0.82;
static const float BSAnimationDuration = 0.2;
static const float BSFrontViewCornerRadius = 5.0;


@interface BSSlidingMenuViewController ()
{
    CGPoint _panVelocity;
    CGPoint _panTranslatedPoint;
    CGRect _frontViewDestinationFrame;
    CGRect _frontViewStartingFrame;
    BOOL _isMenuVisible;
    UIViewController *_menuViewController;
    UINavigationController *_frontViewController;
}
@property (nonatomic) UITapGestureRecognizer *tapGesture;
@property (nonatomic) UIPanGestureRecognizer *panGesture;

@end

@implementation BSSlidingMenuViewController

#pragma mark - view controller lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _frontViewStartingFrame = [[UIScreen mainScreen] bounds];
    _frontViewDestinationFrame = _frontViewStartingFrame;
    _frontViewDestinationFrame.origin.x = BSMenuToFrontViewPercentage * _frontViewDestinationFrame.size.width;
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    /*
     *  There must be two segues from this view controller (one for
     *  each of the two chid view controllers: the menu view controller
     *  and the front view controller) with their appropriate identifiers
     */
    [self performSegueWithIdentifier:BSSlidingMenuBackSegue sender:self];
    [self performSegueWithIdentifier:BSSlidingMenuFrontSegue sender:self];
    
}

#pragma mark - setting the view controllers

-(void)setMenuViewController:(UIViewController *)controller
{
    [self addMenuViewController:controller];
}

- (void)setFrontViewController:(UINavigationController *)controller
{
    if(!_frontViewController){
        [self addFrontViewController:controller];
    }else{
        NSLog(@"%@ <> %@", [controller.childViewControllers[0] class], [_frontViewController.childViewControllers[0] class]);
        if(![controller.childViewControllers[0] isKindOfClass:[_frontViewController.childViewControllers[0] class]]){
            [self cycleFromViewController:_frontViewController toViewController:controller];
        }else{
            [self hideMenu];
        }
    }
}

- (void) addMenuViewController:(UIViewController *)controller
{
    _menuViewController = controller;
    [self addChildViewController:_menuViewController];
    CGRect frame = CGRectMake(_frontViewStartingFrame.origin.x, _frontViewStartingFrame.origin.y, _frontViewStartingFrame.size.width, _frontViewStartingFrame.size.height);
    [_menuViewController.view setFrame:frame];
    [self.view addSubview:_menuViewController.view];
    [self.view sendSubviewToBack:_menuViewController.view];
    [_menuViewController didMoveToParentViewController:self];
}

- (void) configureMenuButton
{
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:BSSlidingMenuIcon] style:UIBarButtonItemStyleBordered target:self action:@selector(toggle)];
    [[_frontViewController.childViewControllers[0] navigationItem] setLeftBarButtonItem:barButton];
}

- (void) addFrontViewController:(UINavigationController *)controller
{
    _frontViewController = controller;
    [self addChildViewController:_frontViewController];
    [self configureMenuButton];
    _frontViewController.view.frame = _frontViewStartingFrame;
    [self.view addSubview:_frontViewController.view];
    [self.view bringSubviewToFront:_frontViewController.view];
    [self drawShadowOnFrontView];
    [controller.view addGestureRecognizer:self.panGesture];
    [controller.view addGestureRecognizer:self.tapGesture];
    [_frontViewController didMoveToParentViewController:self];
}

- (void) drawShadowOnFrontView
{
    [_frontViewController.view.layer setMasksToBounds:NO];
    [_frontViewController.view.layer setCornerRadius:BSFrontViewCornerRadius];
	[_frontViewController.view.layer setShadowColor:[UIColor blackColor].CGColor];
	[_frontViewController.view.layer setShadowOpacity:BSShadowOpacity];
	[_frontViewController.view.layer setShadowOffset:CGSizeMake(BSShadowOffset, BSShadowOffset)];
}

- (void) cycleFromViewController: (UINavigationController*) oldC toViewController:(UINavigationController*) newC
{
    [oldC willMoveToParentViewController:nil];
    [self addChildViewController:newC];
    
    _frontViewController = newC;
    [_frontViewController.view addGestureRecognizer:self.panGesture];
    [_frontViewController.view addGestureRecognizer:self.tapGesture];
    
    [self configureMenuButton];
    
    newC.view.frame = _frontViewDestinationFrame;
    [self drawShadowOnFrontView];
    
    [self transitionFromViewController: oldC toViewController: newC
                              duration: 0 options:0
                            animations:^{}
                            completion:^(BOOL finished) {
                                [oldC removeFromParentViewController];
                                [newC didMoveToParentViewController:self];
                                [self hideMenu];

                            }];
    
}


#pragma mark - accessor methods

- (UITapGestureRecognizer *)tapGesture
{
    if(!_tapGesture){
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [_tapGesture setDelegate:self];
    }
    return _tapGesture;
}
- (UIPanGestureRecognizer *)panGesture
{
    if(!_panGesture){
        _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        _panGesture.maximumNumberOfTouches = _panGesture.minimumNumberOfTouches = 1;
        _panGesture.delegate = self;
    }
    return _panGesture;
}

#pragma mark - callbacks

- (void) animateToFrame:(CGRect)frame completionBlock:(void (^) (void))block
{
    [UIView animateWithDuration: BSAnimationDuration delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [_frontViewController.view setFrame:frame];
                     } completion:^(BOOL finished){
                         if(finished){
                             _isMenuVisible = (frame.origin.x == _frontViewDestinationFrame.origin.x);
                             if(block) block();
                         }
                     }];
}

- (void) toggle
{
     if(_isMenuVisible){
        [self hideMenu];
    }else{
        [self showMenu];
    }
}

#pragma mark - handling gesture recognizers

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view isKindOfClass:[NSClassFromString(@"UITableViewCellContentView") class]] || ([gestureRecognizer isEqual:self.tapGesture] && !_isMenuVisible)){
        return NO;
    }else{
        return YES;
    }
}

- (void) handleTap:(UITapGestureRecognizer *)recognizer
{
    if(_isMenuVisible){
        [self hideMenu];
    }
}

- (void) handlePan:(UIPanGestureRecognizer *)recognizer
{
    _panTranslatedPoint = [recognizer translationInView:self.view];
    _panVelocity = [recognizer velocityInView:[recognizer view]];

    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            [self handleGestureRecognizerStateBegan:recognizer];
            break;
        case UIGestureRecognizerStateChanged:
            [self handleGestureRecognizerStateChanged:recognizer];
            break;
        case UIGestureRecognizerStateEnded:
            [self handleGestureRecognizerStateEnded:recognizer];
            break;
        default:
            break;
    }
}
- (void) handleGestureRecognizerStateBegan:(UIPanGestureRecognizer*)recognizer
{
    [[_frontViewController.viewControllers[0] view] setUserInteractionEnabled:NO];
    [_menuViewController.view setUserInteractionEnabled:NO];
    [_menuViewController.view setHidden:NO];
    [self.view sendSubviewToBack:_menuViewController.view];
    [[recognizer view] bringSubviewToFront:[recognizer view]];
}
- (void) handleGestureRecognizerStateEnded:(UIPanGestureRecognizer*)recognizer
{
    CGPoint point = _frontViewController.view.frame.origin;
    if(_panVelocity.x > 0){
        // showing the menu
        if(point.x > _frontViewDestinationFrame.origin.x / 2){
            // show menu
            [self showMenu];
        }else{
            [self hideMenu];
        }
    }else{
        // hiding the menu
        if(point.x < _frontViewDestinationFrame.origin.x){
            [self hideMenu];
            
        }else{
            [self showMenu];
        }
    }
}
- (void) hideMenu
{
    [_menuViewController.view setUserInteractionEnabled:NO];
    [self animateToFrame:_frontViewStartingFrame completionBlock:^{
        [_menuViewController.view setHidden:YES];
        [[_frontViewController.viewControllers[0] view] setUserInteractionEnabled:YES];
        
    }];
}
- (void) hideMenuWithCompletionBlock:(void (^)(void))block
{
    [_menuViewController.view setUserInteractionEnabled:NO];
    [self animateToFrame:_frontViewStartingFrame completionBlock:^{
        [_menuViewController.view setHidden:YES];
        [[_frontViewController.viewControllers[0] view] setUserInteractionEnabled:YES];
        block();
        
    }];
}
- (void) showMenu
{
    [_menuViewController.view setHidden:NO];
    [self animateToFrame:_frontViewDestinationFrame completionBlock:^{
        [_menuViewController.view setUserInteractionEnabled:YES];
        [[_frontViewController.viewControllers[0] view] setUserInteractionEnabled:NO];
    }];
}
- (void) handleGestureRecognizerStateChanged:(UIPanGestureRecognizer*)recognizer
{
    float xOrigin = _frontViewController.view.frame.origin.x + _panTranslatedPoint.x;
    if(xOrigin >= 0 && xOrigin <= CGRectGetWidth(_frontViewDestinationFrame)){
        [recognizer view].center = CGPointMake([recognizer view].center.x + _panTranslatedPoint.x, [recognizer view].center.y);
        [recognizer setTranslation:CGPointMake(0,0) inView:self.view];
    }
}

@end

@implementation BSSlidingMenuSegue

- (void) perform
{
    NSAssert([self.sourceViewController isKindOfClass:[BSSlidingMenuViewController class]], @"Source view controller must be of type BSSlidingMenuViewController.");
    BSSlidingMenuViewController *slidingController = (BSSlidingMenuViewController*)self.sourceViewController;
    
    if([self.identifier isEqualToString:BSSlidingMenuBackSegue]){
        [slidingController setMenuViewController:self.destinationViewController];
    }else {
        NSAssert([self.destinationViewController isKindOfClass:[UINavigationController class]], @"Destination view controller must be an instance of UINavigationController.");
        [slidingController setFrontViewController:self.destinationViewController];
    }
    
}

@end

@implementation UIViewController (BSSlidingMenu)
- (BSSlidingMenuViewController*) slidingMenuViewController
{
    id result = self;
    while([result parentViewController] && ![result isKindOfClass:[BSSlidingMenuViewController class]]){
        result = [result parentViewController];
    }
    
    if([result isKindOfClass:[BSSlidingMenuViewController class]]){
        return result;
    }else{
        return nil;
    }
}
@end


