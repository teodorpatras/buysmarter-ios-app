//
//  BSTitleViewsFactory.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 4/20/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSProductStockPriceView.h"
#import "BSProductTitleView.h"
#import "BSProductDescriptionView.h"

typedef NS_ENUM(NSUInteger, BSProductDetailViewType) {
    BSProductDetailViewTypeStockPriceView,
    BSProductDetailViewTypeDescriptionView,
    BSProductDetailViewTypeTitleView
};

@interface BSProductDetailViewsFactory : NSObject
/**
 *  Initialized
 *
 *  @param type  the type of the detail view
 *  @param p     the product for the view to be configured with
 *
 *  @return      the instance of the configured view
 */
+ (id) productDetailViewOfType:(BSProductDetailViewType)type withProduct:(Product *)p;

@end
