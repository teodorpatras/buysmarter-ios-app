//
//  BSProductLoaderViewController.h
//  Buy Smarter
//
//  Created by Teodor Patraș on 3/27/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSBaseViewController.h"

@protocol BSProductLoaderDelegate <NSObject>
/**
 *  Tells it's deegate that the view controller has loaded the product
 *
 *  @param dictionary   the dictionary representation of the product
 *  @param authError    whether or not the authentication error occured
 */
- (void) productLoaderDidFinishWithDictionary:(NSDictionary *)dictionary authenticationError:(BOOL)authError;
@end

@interface BSProductLoaderViewController : BSBaseViewController <UIActionSheetDelegate>

@property (nonatomic, weak) id<BSProductLoaderDelegate> delegate;
/**
 *  Initiates a new request for the product
 *
 *  @param dict     the payload dictionary
 */
- (void) requestForProductWithDictionary:(NSDictionary *)dict;

@end
