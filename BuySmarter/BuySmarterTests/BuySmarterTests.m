//
//  BuySmarterTests.m
//  BuySmarterTests
//
//  Created by Teodor Patraș on 3/11/14.
//  Copyright (c) 2014 Teodor Patraș. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BSValidationManager.h"
#import "BSAlphaValidator.h"
#import "BSNumericValidator.h"
#import "BSEmailValidator.h"
#import "BSNotEmptyValidator.h"
#import "BSAddressValidator.h"
#import "BSPhoneValidator.h"
#import "BSPasswordValidator.h"

@interface BuySmarterTests : XCTestCase
@property (nonatomic, strong) BSValidationManager *manager;
@end

@implementation BuySmarterTests

- (BSValidationManager *)manager
{
    if(!_manager){
        _manager = [BSValidationManager new];
        [_manager registerClass:[BSAddressValidator class] withIdentifier:NSStringFromClass([BSAddressValidator class]) andErrorMessage:@""];
        [_manager registerClass:[BSPhoneValidator class] withIdentifier:NSStringFromClass([BSPhoneValidator class]) andErrorMessage:@""];
        [_manager registerClass:[BSPasswordValidator class] withIdentifier:NSStringFromClass([BSPasswordValidator class]) andErrorMessage:@""];
        [_manager registerClass:[BSNotEmptyValidator class] withIdentifier:NSStringFromClass([BSNotEmptyValidator class]) andErrorMessage:@""];
        [_manager registerClass:[BSEmailValidator class]  withIdentifier:NSStringFromClass([BSEmailValidator class]) andErrorMessage:@""];
        [_manager registerClass:[BSNumericValidator class] withIdentifier:NSStringFromClass([BSNumericValidator class]) andErrorMessage:@""];
        [_manager registerClass:[BSAlphaValidator class] withIdentifier:NSStringFromClass([BSAlphaValidator class]) andErrorMessage:@""];
    }
    return _manager;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Positives

- (void) testSuccessAlpha
{
    NSString *error;
    [self.manager validateInput:@"alpha"
                  forIdentifier:NSStringFromClass([BSAlphaValidator class])
                          error:&error];
    XCTAssertNil(error, @"Invalid alpha text");
}

- (void) testSuccessNumeric
{
    NSString *error;
    [self.manager validateInput:@"12345"
                  forIdentifier:NSStringFromClass([BSNumericValidator class])
                          error:&error];
    XCTAssertNil(error, @"Invalid numeric text");
}

- (void) testSuccessAddress
{
    NSString *error;
    [self.manager validateInput:@"Str. Mistrețului Nr. 20" forIdentifier:NSStringFromClass([BSAddressValidator class]) error:&error];
    
    XCTAssertNil(error, @"Invalid address text");
}

- (void) testSuccessPhone
{
    NSString *error;
    [self.manager validateInput:@"0741056533" forIdentifier:NSStringFromClass([BSAddressValidator class]) error:&error];
    
    XCTAssertNil(error, @"Invalid phone text");
}

- (void) testSuccessPassword
{
    NSString *error;
    [self.manager validateInput:@"Parola123" forIdentifier:NSStringFromClass([BSPasswordValidator class]) error:&error];
    
    XCTAssertNil(error, @"Invalid password text");
}

- (void) testSuccessEmail
{
    NSString *error;
    [self.manager validateInput:@"john@doe.com" forIdentifier:NSStringFromClass([BSEmailValidator class]) error:&error];
    
    XCTAssertNil(error, @"Invalid email text");
}

#pragma mark - Negatives

- (void) testFailAlpha
{
    NSString *error;
    [self.manager validateInput:@"alp2ha"
                  forIdentifier:NSStringFromClass([BSAlphaValidator class])
                          error:&error];
    XCTAssertNotNil(error, @"Invalid alpha text");
}
// exemplu de caz de succes în cazul validării numerice
- (void) testFailNumeric
{
    NSString *error;
    [self.manager validateInput:@"12s345" forIdentifier:NSStringFromClass([BSNumericValidator class]) error:&error];
    
    XCTAssertNotNil(error, @"Invalid numeric text");
}

- (void) testFailAddress
{
    NSString *error;
    [self.manager validateInput:@"Str. ~@#      Mistrețului Nr. 20    " forIdentifier:NSStringFromClass([BSAddressValidator class]) error:&error];
    
    XCTAssertNotNil(error, @"Invalid address text");
}

- (void) testFailPhone
{
    NSString *error;
    [self.manager validateInput:@"07410~sd56533" forIdentifier:NSStringFromClass([BSPhoneValidator class]) error:&error];
    
    XCTAssertNotNil(error, @"Invalid phone text");
}

- (void) testFailPassword
{
    NSString *error;
    [self.manager validateInput:@"@@arola123" forIdentifier:NSStringFromClass([BSPasswordValidator class]) error:&error];
    
    XCTAssertNotNil(error, @"Invalid password text");
}

- (void) testFailEmail
{
    NSString *error;
    [self.manager validateInput:@"john@doecom" forIdentifier:NSStringFromClass([BSEmailValidator class]) error:&error];
    
    XCTAssertNotNil(error, @"Invalid email text");
}

@end
